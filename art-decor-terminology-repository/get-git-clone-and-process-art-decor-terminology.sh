#!/bin/bash
# Routine for updating packages for terminology.art-decor.org

# link to remote git for the ART-DECOR terminology packages
# use our terminology repo at art-decor.cloud
REPOSITORY=git@git.art-decor.cloud:art-decor-terminology-packages.git
# link to local git working copy
BRANCH=master

# OUR identifiy for GIT commits
GITUSERNAME="GIT art-decor.cloud"
GITEMAIL="git@art-decor.email"
git config --global user.name "${GITUSERNAME}"
git config --global user.email ${GITEMAIL} 

# local files and dirs
SCRIPTFOLDER="/opt/art-decor-terminology-repository"
TARGETURL=/var/www/vhosts/art-decor.org/docs.art-decor.org
GIT_LOCAL_WORKING_COPY_DIR=${SCRIPTFOLDER}/${BRANCH}

# order file (in branch) and log
ORDERFILE="aaa-rebuild-orders.txt"
LASTORDERLOGFILE="aaa-order-run-log.txt"

# tmp file
TMP=__log_$$

# go home
cd $SCRIPTFOLDER || exit 0

# create directory for local repo if needed
mkdir -p ${GIT_LOCAL_WORKING_COPY_DIR}
cd ${GIT_LOCAL_WORKING_COPY_DIR}

# php processing and check
PHP=php
PHPCMD="${PHP} art-decor-terminology-package-management.php"
# test it works
if ! type "$PHP" > /dev/null; then
  echo "+++ ${PHP} not working, exiting"
  exit
fi
# test ant TODO
ANT=ant
if ! type "$ANT" > /dev/null; then
  echo "+++ ${ANT} not working"
  exit
fi
# mail processing and check
MAILCMD=mailx
if ! type "$MAILCMD" > /dev/null; then
  echo "+++ ${MAILCMD} not working"
  # unset MAILCMD so it is not executed later
  MAILCMD=
fi

echo "### -------------------------------------------------------------------" > ${TMP}
echo "### This is the log of the last terminology repository continuous build" >> ${TMP}
echo "###" `date` >> ${TMP}

echo "*** PHASE I: git action"
echo "*** PHASE I: git action" >> ${TMP}

# check if directory is empty
# if no: was already cloned, that is Ok for later runs, just continue
if [ "$(ls -A ${GIT_LOCAL_WORKING_COPY_DIR})" ]; then
  echo Did not create local working copy because folder already exists
  # ... but erase former rebuild-orders.txt file before getting from git
  rm -f ${GIT_LOCAL_WORKING_COPY_DIR}/${ORDERFILE}
else
  echo Created local working copy, doing initial clone
  git clone $REPOSITORY ${GIT_LOCAL_WORKING_COPY_DIR}
fi

# git actions
# override local changes
git reset --hard origin/${BRANCH}
git pull origin ${BRANCH}
git checkout ${BRANCH}
# override local changes
# git reset --hard origin/${BRANCH}
# git pull

# store what our last head was
cd $SCRIPTFOLDER
LASTHEADFILE=last.head
LASTHEAD=`cat ${LASTHEADFILE}`
# build and deploy
# version info
cd $GIT_LOCAL_WORKING_COPY_DIR
VERSIONSIGN=`git rev-list HEAD --count --all`
cd $SCRIPTFOLDER
if [ -n "$LASTHEAD" ]
then
  if [ "$LASTHEAD" = "$VERSIONSIGN" ]
  then
    echo "No git update needed, revision ${VERSIONSIGN}"
  else
    echo Updating git, revisions: last=$LASTHEAD, current=$VERSIONSIGN
  fi
else
  echo Updating git completely because of no last head
  rm -rf $GIT_LOCAL_WORKING_COPY_DIR
  mkdir $GIT_LOCAL_WORKING_COPY_DIR || exit 0
  git clone $REPOSITORY ${GIT_LOCAL_WORKING_COPY_DIR}
fi

cd $GIT_LOCAL_WORKING_COPY_DIR

# override local changes
git reset --hard origin/${BRANCH}
# git action
git fetch origin
git rebase origin ${BRANCH}

# finished, update head
cd $SCRIPTFOLDER
echo ${VERSIONSIGN} > ${LASTHEADFILE}

echo "*** PHASE II: build new repositories upon request(s)"
echo "*** PHASE II: build new repositories upon request(s)" >> ${TMP}

# action to build new repositories based on requests in "rebuild-orders" file

# ok, get started
cd $GIT_LOCAL_WORKING_COPY_DIR

COUNT=0
ERRORS=0
if [ -f ${ORDERFILE} ]; then
  IFS=$' \t\n'
  for word in $(grep -v '^#' < "$ORDERFILE")
  do
    echo "*** Processing order" $word >> ${TMP}
    ( # try
      set -e
      # pwd
      #echo ${PHPCMD} --process ${word}
      ${PHPCMD} --process ${word} >> ${TMP}
    ) || ( # catch
      echo "+++ Errors detected while processing"
      echo "+++ Errors detected while processing" $word >> ${TMP}
      ERRORS=$(( ERRORS + 1 ))
    )
    COUNT=$(( COUNT + 1 ))
  done
  echo ${COUNT} orders processed... processing completed!
  echo ${COUNT} orders processed... processing completed! >> ${TMP}
else
  echo No order file
  echo No order file >> ${TMP}
fi


# list all created ANTs (XML ant input files)
# for later processing through command: ant --buildfile (ANT)
if [ $COUNT -gt 0 ]
then
  
  echo "*** PHASE III: post-processing"
  echo "*** PHASE III: post-processing" >> ${TMP}
  
  for entry in "ant-input-*"
  do
    echo "*** Processing ANT input file => " $entry >> ${TMP}
    echo >> ${TMP}
    echo ant --buildfile $entry
    echo ant --buildfile $entry >> ${TMP}
  done
fi
cd $SCRIPTFOLDER


# final git action: re-infuse the empty ORDERFILE if $COUNT > 0
if [ $COUNT -gt 0 ]
then

  echo "*** PHASE IV: cleanup" 
  echo "*** PHASE IV: cleanup" >> ${TMP}

  # reset the order file, call convert php
  echo "# " > ${ORDERFILE}
  ${PHPCMD} --print-empty-order-file > ${ORDERFILE}
  # email the last temp log to GITEMAIL if configured
  if [ "$MAILCMD" = "" ]
  then
    echo "Sending mail skipped..."
  else
    # prepare a preamble with a summary (for email)
    if [ $ERRORS -gt 0 ]
    then
      SUBJECT="Result of CI run for Terminology Packages, $COUNT processed, $ERRORS errors."
    else
      SUBJECT="Result of CI run for Terminology Packages, $COUNT processed, no errors."
    fi
    FROM="${GITUSERNAME} <reply.not.possible@art-decor.email>"
    TO=${GITEMAIL}
    cat ${TMP} | mail -r "${FROM}" -s "${SUBJECT}" "${TO}"
  fi
  # cat the temp log to the log file, last one first
  touch ${LASTORDERLOGFILE}
  cat ${LASTORDERLOGFILE} >> ${TMP}
  rm -f ${LASTORDERLOGFILE}
  mv ${TMP} ${LASTORDERLOGFILE}
  # git settings
  git config --global user.name "${GITUSERNAME}"
  git config --global user.email ${GITEMAIL}
  # add to git and commit
  git add ${ORDERFILE}
  git add ${LASTORDERLOGFILE}
  git add vuepress-terminology/docs/src/codesystems
  git commit -m "${ORDERFILE} processed, emptied docs updated"
  git push

else
  # remove temporary log file, it is empty anyway
  rm -f ${TMP}
fi

echo "*** KONEC"