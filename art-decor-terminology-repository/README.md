# ART-DECOR® Terminology Repository #

## Superior Shell Scripts and Configs

The PHP scripts residing in this directorx actually are located in the Terminology Repository Working Directors at `/opt/art-decor-terminology-repository` and not at master or any other subdirectory.

The script `get-git-clone-and-process-art-decor-terminology.sh` is doing the following tasks

* PHASE I: git actions
  * if the `master` branch working subdirectory does not exist, create it and clone the remote repository, otherwise check for updates of the remote repository and get updates if needed
  * Update the `last.head` file to reflect HEAD
* PHASE II: build new repositories upon request
  * inspect "rebuild-orders" file `aaa-rebuild-orders.txt` and build new repositories, one by one, based on the request(s) in that file
  * record counts of repositories processed and errors
  * Let update the eXist database public repository `development3` or `stable3`
  * write to a log file

If no new repositories where "ordered"/processed the script ends immediately with `*** KONEC`, otherwise

* PHASE III: post-processing
  * send email with latest log contents
  * re-infuse repositories with processing results and updates READMEs for code systems

The script ends with `*** KONEC` (czech for "The End")

## Package Upload to Repositories

After the script `get-git-clone-and-process-art-decor-terminology.sh` has completed all builds, the newly created terminology packages need to be uploaded to the corresponding repositories. The script `make_repo_upload_ant.php` compiles all information from the previous runs and emits ant files to be processed for uploads to the repository.

To be continued and refined...

## Terminology Documentation

The build script for the documentation that is based on Vuepress and is reachable at https://terminology.art-decor.org is run separately and refreshes the documentation upon changes.

```sh
# run new vuepress documentation
HOME=/opt/art-decor-terminology-repository
VUESRC=$HOME/master/vuepress-terminology/docs
VUETARGET=/var/www/vhosts/art-decor.org/terminology.art-decor.org
NODE_OPTIONS=--openssl-legacy-provider && vuepress build src
cd $VUESRC
npm run build
cd $VUESRC/src/.vuepress/dist
rm -rf $VUETARGET/*
cp -pr * $VUETARGET/
```

