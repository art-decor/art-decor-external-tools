<?php

# target credentials: username and password
$TARGETUSER="...";
$TARGETPASSWORD="...";

# eXist db absolute file system path, e.g. /usr/local/exist_repo
$EXISTDIR="/usr/local/exist_repo";
# eXist db uri to target repository, e.g. http://localhost:8877/exist/apps/(BRANCH)
# PLEASE NOTE: must NOT end on /, the corresponding branch is added while processing
$EXISTURI="http://localhost:8877/exist/apps";

# the branches to process: name and style
# old fashioned repos use style "put-package" as "command" uri, newer "publish" 
$BRANCHES = [
    [
        "branch" => "branchname1",
        "style"  => "publish" 
    ],
    [
        "branch" => "branchname2",
        "style"  => "put-package" 
    ],
    [
        "branch" => "branchname3",
        "style"  => "put-package" 
    ]
];