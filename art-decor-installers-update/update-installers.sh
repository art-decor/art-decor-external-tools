#!/bin/bash
# Routine for updating ther installer ZIPs at repository.art-decor.org
# URL https://repository.art-decor.org/installers

# link to remote git for artdecor and a short name for it
REPOSITORY=https://art-decor@bitbucket.org/art-decor/art-decor-external-tools.git
NAME="'External Tools'"
# link to local git working copy
BRANCH=master

# local files and dirs
SCRIPTFOLDER="/opt/art-decor-installers-update"
TMPDIR=${SCRIPTFOLDER}/tmp
TARGETURL=/var/www/vhosts/art-decor.org/repository.art-decor.org/installers

# go home
cd $SCRIPTFOLDER || exit 0
# dirs
if [ -d "$BRANCH" ]; then
  echo Directory $BRANCH already exist, fantastic!
  # git pull action, assume it is already checked out
  cd $BRANCH

  CHANGED=0
  git remote update && git status -uno | grep -q 'Your branch is behind' && CHANGED=1
  if [ $CHANGED = 1 ]; then
    git pull
    echo Repository $NAME updated
  else
    echo Repository $NAME is already up-to-date, no changes, no post-processing necessary
    echo konec
    exit 0
  fi
else
  mkdir $BRANCH || exit 0
  echo Directory $BRANCH created
  # git clone action
  if (git clone $REPOSITORY --branch $BRANCH --single-branch $BRANCH) then
    echo Repository $NAME cloned
  else
    echo +++Repository cloning failed
    exit 1
  fi
  cd $BRANCH || exit 0
fi

# in BRANCH now, make the installer ZIPs
for EI in art-decor-vue art-decor-exist-db-installer
do
  TAR=../$EI.tar.gz
  tar cfz $TAR $EI
  # all preprocessing finished, copy it to destination
  cp -prf $TAR $TARGETURL
  echo Installer $EI packed and copied to $TARGETURL
done

# bye
# rm -rf $BRANCH
echo "konec..."