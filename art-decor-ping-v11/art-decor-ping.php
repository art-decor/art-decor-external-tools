<?php
/*
 *
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
 *  https://docs.art-decor.org/copyright/
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the
 *  GNU Lesser General Public License as published by the Free Software Foundation; either version
 *  2.1 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
 *
 *  - - - - - - - - - - -
 *  Main script for the ART-DECOR® PING service (ADPING)
 *  Also offers and published summary of uptime statistics of the server
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools 2013-2021
 */

/* ----Overide default execution vars---- */
date_default_timezone_set('Europe/Berlin');

/* ----PRESETS---- */
/*  name and version of this script */
$thisscriptname = "ADPING";
$thisscriptversion = "v11";

// other presets
$statusfilename = "status";
$servicestatusfilename = "servicestatus.raw";
$servicesummaryfilename = "servicestatus-summary";
$debug = 0;

// preset variables, will be overridden by the config
$url1 = "";        // the URL to the main page of the art-decor server to ping
$recipients = "";  // recipients to inform on status changes, separated by comma

// include config
require "config.php";

// data empty
$data = array();

// create new cURL-Handle
$ch = curl_init();

// setze die URL und andere Optionen
curl_setopt($ch, CURLOPT_URL, $url1);
//curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

// action
$reachable = false;
if ($result = curl_exec($ch) ) {
	if (
		// if these items are present the database is up Release 3
		// TODO: API call to API version to prove API availability AND an extra text in the rendered html
		(
                  ( strpos($result, "<title>ART-DECOR®</title>") !== false ) and
		  ( strpos($result, "®<") !== false )
                ) or (
                  // if these items are present the database is up Release 2
                  ( strpos($result, ">Advanced Requirements Tooling<") !== false ) and
                  ( strpos($result, "<a href=\"http://art-decor.org\">") !== false )
                )
	)
	{
		if ($debug > 0) echo "Alive\n";
		$reachable = true;
	} else {
		if ($debug > 0) echo "Unreachable\n";
	}
};
// var_dump($result);exit;

// close cURL-Handle
curl_close($ch);

// act on (un)reachable
$msg = "";
$service = "up";
if ($reachable == false) {
	$msg = " +++unreachable+++";
	$service = "down";
} else {
	$msg = " is up again";
}

// get last status
$status = file_get_contents ($statusfilename);

if ($status != $reachable) {
	date_default_timezone_set('UTC');
	$date = date("Y-m-d H:i:s");
	$to = $recipients;
	$subject = "ART-DECOR Server " . $msg;
	$message = "ART-DECOR Server " . $url1 . $msg . " at " . $date . " UTC (" . $thisscriptname . " " . $thisscriptversion . ")";
	if (isset($existlog)) {
        if (is_file($existlog)) {
            $message .= "\n\nLast 100 lines of exist log\n";
            $message .= `tail -100 ${existlog}`;
        } 
    }
    $from = "From: ART-DECOR PING Service <support@art-decor.org>";
	mail ($to,$subject,$message,$from);
	echo "Email sent to $to: $message\n";
}

// write the recent status
file_put_contents ($statusfilename, $reachable);

// add the status to the log service status raw file
$nu = gmdate('Y-m-d H:i:s', time());
$line = $nu . " " . $service . "\n";
file_put_contents ($servicestatusfilename, $line, FILE_APPEND);

// process raw data and cretae the summary service status
$srv1 = file_get_contents ($servicestatusfilename);
$srvxml = make_uptime ($srv1);
$srv2 = simplexml_load_string($srvxml);
$srvjson = json_encode($srv2, JSON_NUMERIC_CHECK);
// write the summary files
file_put_contents ($servicesummaryfilename . ".xml", $srvxml);
file_put_contents ($servicesummaryfilename . ".json", $srvjson);

exit;



function make_uptime ($records) {
    date_default_timezone_set('UTC');
    $line = explode ("\n", $records);
    $now = new DateTime();
    $lastTimePoll = "-1";
    foreach ($line as $l) {
        $d = substr($l, 0, 19);
        // echo "-$d\n";
        if (strlen($d) > 10) $lastTimePoll = $d;
    }
    $lastTimePoll = strtotime($lastTimePoll);
    $lastTimePoll = $lastTimePoll * 1000;
    // echo $lastTimePoll->format('Y-m-d H:i:s');echo "-$d\n";

    /* $lastTimePoll = $lastTimePoll->format('Y-m-d H:i:s') . $now->format('Y-m-d H:i:s');
    $lastTimePoll = $lastTimePoll->diff($now);
    $lastTimePoll =
        ($lastTimePoll->y * 365 * 24 * 60) +
        ($lastTimePoll->m * 30 * 24 * 60) +
        ($lastTimePoll->d * 24 * 60) +
        ($lastTimePoll->h * 60) +
        ($lastTimePoll->i);
    */

    $today = new DateTime();
    $today->add(new DateInterval('P1D'));

    $xml = "";
    $sumpct = 0;
    $pctarray = array();

    // get up and downtime records per day
    for ($day = 0; $day <= 29; $day++) {
        $data = array();
        $startTime = "";
        $endTime = "";
        $offset = -$day;
        $cnt = 0;
        $subjectday = $today;
        $subjectday->sub(new DateInterval('P1D'));
        $subjectdaystring = $subjectday->format('Y-m-d');
        $subjectdayTimeStampPHP = strtotime($subjectdaystring);
        $subjectdayTimeStampJS = $subjectdayTimeStampPHP * 1000;
        // echo $subjectdaystring . "\n"; continue;
        foreach ($line as $l) {
            $r = explode(" ", $l);
            // echo "$r[0] - " . $subjectdaystring . "\n";
            if ($r[0] != $subjectdaystring) continue;
            $cnt = $cnt + 1;
            if ($cnt == 1) $startTime = strtotime($r[0] . " " . $r[1]);
            $endTime = strtotime($r[0] . " " . $r[1]);
            // echo $r[0] . "\n";
            $ts = $r[0] . " " . $r[1];
            if ($r[2] == "up") {
                $up = 1;
            } else {
                $up = 0;
            }
            $data[$ts] = $up;

            // Total number of seconds your website was down: 600 seconds.
            // Total number of seconds your website was monitored: 86,400.
            // We divide 600 by 86,400, which is 0.0069.
            // In percentages, this is 0.69%. This is the downtime percentage.
            // The uptime percentage for this website would be: 100% minus 0.69% is 99.31%.
        }

        $totalUptime = 0;
        $lastUptime = 0;
        $lastWorking = 0;

        if (count($data)>0) {
            foreach ($data as $dateString => $isWorking) {
                $timestamp = strtotime($dateString);

                if ($lastWorking && $lastTimestamp) {
                    $allowedStart = $lastTimestamp;
                    if ($allowedStart < $startTime)
                        $allowedStart = $startTime;

                    $allowedEnd = $timestamp;
                    if ($allowedEnd > $endTime)
                        $allowedEnd = $endTime;

                    $diff = $allowedEnd - $allowedStart;

                    if ($diff > 0) {
                        $totalUptime += $diff;
                    }
                }

                if ($timestamp >= $endTime) {
                    break;
                }

                $lastTimestamp = $timestamp;
                $lastWorking = $isWorking;
            }
            //echo "Date:         " . $subjectdaystring . "\n";
            //echo "Total uptime: " . $totalUptime . " seconds\n";
            $totaltime = $endTime - $startTime;
            //echo "Total time:   " .  $totaltime . " seconds\n";
            $uptimepct = number_format(($totalUptime / $totaltime) * 100, 2);
            $sumpct += $uptimepct;
            $pctarray[] = $uptimepct;
            //echo "Uptime%: " , $uptimepct . "%\n";
            //$xml .= "<uptime><date>$subjectdaystring</date><offset>$offset</offset><total>$totaltime</total><up>$uptimepct</up></uptime>\n";
            $xml .= "<data><x>$subjectdayTimeStampJS</x><y>$uptimepct</y></data>";
        } else {
            //$xml .= "<uptime><date>$subjectdaystring</date><offset>$offset</offset><total>86400</total><up>$uptimepct</up></uptime>\n";
            $xml .= "<data><x>$subjectdayTimeStampJS</x><y>$uptimepct</y></data>";
        }
    }

    $uptimepct30average = $sumpct / 30;
    $uptimepct30median  = median($pctarray);

    $xml = "<uptimes>" .
        "<uptimepct30average>$uptimepct30average</uptimepct30average>" .
        "<uptimepct30median>$uptimepct30median</uptimepct30median>" .
        "<lastPoll>$lastTimePoll</lastPoll>" .
        $xml . "</uptimes>";

    return $xml;
}

function median($arr){
    if($arr){
        $count = count($arr);
        sort($arr);
        $mid = floor(($count-1)/2);
        return ($arr[$mid]+$arr[$mid+1-$count%2])/2;
    }
    return 0;
}


?>
