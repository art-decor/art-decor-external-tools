#!/bin/bash
# simple exist-db install script for ART-DECOR
# usage: run this script with sudo permissions: sudo ./<script>
# to install to a different path: sudo ./<script> --path /usr/local/exist_repo
# usage: run this script to override default package with: sudo ./<script> -f <package location in ../packages> so:  sudo ./install_exist.sh -f eXist-db-setup-3.1.0.jar
# info: http://exist-db.org/exist/apps/doc/advanced-installation#headless

# version
# 2012 08 22: initial version
# 2013 02 25: changed to more general setup
# 2013 09 28: changed to exist-db 2.1
# 2014 04 24: existdb does not work with shell nologin, so bash
# 2014 11 22: modified for exist 2.2 LTS
# 2015 10 27: remove old symlink in usr/local
# 2016 04 07: overwrite option for the location_symlink
# 2017 02 14: modified for exist 3.0
# 2017 03 16: modified for exist 3.1.0
# 2017 04 19: modified for exist 3.1.1
# 2017 04 22: added exist for automated installation
# 2017 08 03: Introduced logic: set contextpath to default, start/stop, set contextpath to /
# 2017 08 24: yum_prerequisites: install expect
# 2018 02 23: fix for system_installer
# 2018 02 23: fix for sanity check
# 2018 03 02: support non-default ports for the repo eXist-db
# 2021 01 13: modified for exist 5
# 2021 07 11: modified for exist 5 ports
# 2022 08 13: move system_installer to settings. Removed --comment from adduser for ubuntu. Removed --skip-broken for system_installer for ubuntu.
# 2022 08 29: simplified the paths for exist_file_full_path
# 2024 01 31: removed option 12 during install

# get common parameters from settings script
. ./settings
# provides input for the following variables:
# java_location
# exist_password=test123
# exist_maxmem=8192
# exist_cachemem=1024
# installer_location -> to find the exist-db installer package
# exist_file
# exist_version
# jetty.http.port
# jetty.ssl.port
# system_installer=yum

# set the user to run the process as
application_user=existdb
# the location where application will be symlinked
location_symlink=/usr/local/exist_atp
# set the full path to the eXist-db installer package
exist_file_full_path="$( cd ${installer_location} && pwd )"
exist_file_full_path=${exist_file_full_path}/${exist_file}

function file_present ()
{
   file=$1
   # function to check if a file is present or exit
   # test if file is present and readable
   [ -f "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT FOUND: ${1}" ; usage ;}
   [ -r "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT READABLE: ${1}" ; usage ;}
}

function yum_prerequisites ()
{
        # for automating the eXist-db installation, we use expect
        ${system_installer} -y install expect

# end of function yum_prerequisites
}

function sanity_check ()
{
       # sanity check
       # these parameters needs to be set:

       echo $0 Installing eXist-db with the following settings:
       for var in java_location exist_file_full_path installer_location exist_maxmem exist_cachemem exist_file exist_version system_installer; do
         if [ -z "${!var}" ]
           then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings' or see --usage"
           usage
         else echo "    ${var}=${!var}"
         fi
       done

       var=exist_password
         if [ -z "${!var}" ]; then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings' or see --usage"; usage; else echo "    ${var}=not showing ..." ; fi

       # display this setting
           echo "    location_symlink = ${location_symlink}"

       # test if settings file is present and readable
       file_present ${exist_file_full_path}

# end of sanity_check
}

function setup_config ()
{
        # the location where application will be installed
        location_application=${location_symlink}_${exist_version}_$(date '+%Y%m%d')

        export JAVA_HOME=${java_location}

# end of setup_config_ports
}

function install_application ()
{
        # create directory for exist
        if [ ! -d ${location_application} ]; then
          mkdir $location_application
          else
            echo $0 ERROR: folder ${location_application} already exists
            echo ""
            usage
        fi
        cd $location_application
        # install application
        echo Starting exist-db installer.
        # automated installation needs 3 parameters, could be input from file settings, or passed to script
        # Examples:
        # exist_password=test123
        # exist_maxmem=8192
        # exist_cachemem=1024
        # to run manually: java -jar ${exist_file_full_path} -console
        expect -c "
          set timeout -1
          spawn java -jar ${exist_file_full_path} -console
          expect \"?redisplay\r\"
            send \"1\r\"
          expect \"?elect the installation path:*?\r\"
            send \"${location_application}\r\"
          expect \"Press 1 to continue*?\r\"
            send \"1\r\"
          expect \"Data dir:*?\r\"
            send \"\r\"
          expect \"Press 1 to continue*?\r\"
            send \"1\r\"
          expect \"Enter password:*?\r\"
            send \"${exist_password}\r\"
          expect \"Retype password:*?\r\"
            send \"${exist_password}\r\"
          expect \"Press 1 to continue*?\r\"
            send \"1\r\"
          expect \"Please select which packs you want to install.\r\"
            send \"10\r\"
          expect \"Please select which packs you want to install.\r\"
            send \"11\r\"
          expect \"Please select which packs you want to install.\r\"
            send \"0\r\"
          expect \"Press 1 to continue*?\r\"
            send \"1\r\"
          expect \"Enter Y for Yes*?\r\"
            send \"N\r\"
          expect \"Enter Y for Yes*?\r\"
            send \"N\r\"
          expect \"Enter Y for Yes*?\r\"
            send \"N\r\"
          expect \"Press 1 to continue*?\r\"
            send \"1\r\"
          expect eof"
# end of install_application
}

function symlink_application ()
{
        # remove old symlink
        rm ${location_symlink}
        # set up symlink
        ln -s ${location_application} ${location_symlink}
        echo ln -s ${location_application}/ ${location_symlink}

# end of function symlink_application
}

function setup_permissions ()
{
        # set up the user and permissions
        # adduser to run the application as
        # adduser --shell /bin/bash ${application_user}
        adduser --system --shell /sbin/nologin ${application_user}

        # set up permissions
        chown -h ${application_user}:${application_users} ${location_symlink}
        chown -R ${application_user}:${application_users} ${location_application}
}

function application_systemd ()
{
        FILE=/etc/systemd/system/eXist-db.service
        cat > ${FILE} << _EOF_
[Unit]
Description=eXist-db ${exist_version} Server
Documentation=
After=syslog.target

[Service]
Type=simple
User=existdb
Group=existdb
ExecStart=${location_symlink}/bin/startup.sh

[Install]
WantedBy=multi-user.target
_EOF_

# end of function application_systemd ()
}

function application_config ()
{
    # (optional) set our own ports instead of default ports
        if [ ! -z "${jetty_http_port}" ]; then
          echo Setting jetty.http.port to ${jetty_http_port}
          # <SystemProperty name="jetty.port" default="8877"/>
          sed -i '/jetty.port/s/8080/'${jetty_http_port}'/g' ${location_symlink}/etc/jetty/jetty-http.xml
          # port might already be set to our port
          sed -i '/jetty.port/s/8877/'${jetty_http_port}'/g' ${location_symlink}/etc/jetty/jetty-http.xml
    fi
        if [ ! -z "${jetty_ssl_port}" ]; then
          echo Setting jetty.ssl.port to ${jetty_ssl_port}
          sed -i '/jetty.ssl.port/s/8443/'${jetty_ssl_port}'/g' ${location_symlink}/etc/jetty/jetty-ssl.xml
          # port might already be set to our port
          sed -i '/jetty.ssl.port/s/8477/'${jetty_ssl_port}'/g' ${location_symlink}/etc/jetty/jetty-ssl.xml
        fi

# end of function application_config
}

function usage ()
{
        echo "Usage: $0 --exist_file <jarfile to install from installer_location>"
        echo "
        -p | --path )           Option to overwrite location_symlink: this provides an alternative name for the symlink we create. Example: --path /usr/local/exist_31_atp
        -f | --exist_file )     [required] which eXist-db .jar package to install
        --password )            [required] provide an admin password for eXist-db
        -m | --exist_maxmem )   [required] the maxmemory for eXist-db
        -c | --exist_cachemem ) [required] the cachemem for eXist-db
        --exist_version )       [required] provides the name for eXist-db in the path where we install the database
                --port_http )           [optional] set your own http port instead of default. For instance for the repo
                --port_ssl )            [optional] set your own ssl port instead of default. For instance for the repo
        -h | --help )           Display this usage function

        Please note that you can also set variables in the file 'settings'
        "
        exit 1
# end of function usage
}

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -p | --path )           shift
                                MYPATHSYMLINK=$1
                                ;;
        -f | --exist_file )     shift
                                exist_file=$1
                                ;;
        --password )            shift
                                exist_password=$1
                                ;;
        -m | --exist_maxmem )   shift
                                exist_maxmem=$1
                                ;;
        -c | --exist_cachemem ) shift
                                exist_cachemem=$1
                                ;;
        --exist_version )       shift
                                exist_version=$1
                                ;;
        --port_http )           shift
                                jetty_http_port=$1
                                ;;
        --port_ssl )            shift
                                jetty_ssl_port=$1
                                ;;
        -h | --help )           usage
                                ;;
        * )                     usage
    esac
    shift
  done

# option to overwrite location_symlink
if [ -n "${MYPATHSYMLINK}" ]; then
   location_symlink=${MYPATHSYMLINK}
fi

yum_prerequisites
sanity_check
setup_config
install_application
symlink_application
setup_permissions
application_systemd
application_config

echo ""
echo "Installation finished. You can start it with:"
echo "   sudo systemctl start eXist-db"
echo "For Centos 6:"
echo "   sudo service ${location_symlink##*/} start"
echo Check out the status with:
echo "   sudo service ${location_symlink##*/} status"
echo "   sudo netstat -anp|grep :8"
echo "   sudo ps aux|grep java"

# EOF

