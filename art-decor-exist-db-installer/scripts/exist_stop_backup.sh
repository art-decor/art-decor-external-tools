#!/bin/bash
# script to stop exist-db, create copy or data directory, start exist-db again
# usage: run this script: sudo ./<script>

# version
# 2023 12 06: initial version
# 2024 03 21: cleaned up the output with head
# 2024 03 26: added BackupLabel

# variables
# what is the eXist-db data directory
OrgFolder=/usr/local/exist_atp/data
# which folder to save the copy of the data directory to
TargetFolder=/usr/local/exist_atp/

function ask_user_abort () {
   echo $1
   echo Do you want to continue anyway?
   read -p "Continue (y/n)? " choice
   case "$choice" in
     y|Y ) echo Continuing;;
     n|N ) abort_installation;;
     * ) abort_installation;;
   esac
}

function abort_installation ()
{
    echo Cancelling script
    exit 0

# end of function abort_installation
}

check_variable_has_content ()
{
        # check whether variable has content
        # echo variable to check = $1
        if [ "$1" != "" ]; then
                        # if variable has content return 1
                        return 1
                else
                        return 0
        fi

# end of function check_variable_has_content
}

function sanity ()
{
        check_variable_has_content ${OrgFolder} && ask_user_abort "WARNING: OrgFolder is not set."
        check_variable_has_content ${TargetFolder} && ask_user_abort "WARNING: TargetFolder is not set."
        check_variable_has_content ${BackupFolderDate} && ask_user_abort "WARNING: BackupFolderDate is not set."

        [ ! -d ${OrgFolder} ] && ask_user_abort "WARNING: OrgFolder DOES NOT exist."
        [ ! -d ${TargetFolder} ] && ask_user_abort "WARNING: TargetFolder DOES NOT exist."
        # we don't check for ${BackupFolderDate} since that is created by cp in the script
}

function usage ()
{
        echo "Usage: $0"
        echo "
        -b | --BackupLabel )    Option to add a label for the user to clarify which backup this was. 
		                        Example: --BackupLabel emptyDatabase
        -h | --help )           Display this usage function

        "
        exit 1
# end of function usage
}

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -b | --BackupLabel )    shift
                                BackupLabel=_$1
                                ;;
        -h | --help )           usage
                                ;;
        * )                     usage
    esac
    shift
  done

# actual folder that is created containing backup of data directory
BackupFolderDate=${TargetFolder}/data_backup_$(date '+%Y%m%d%H%M')${BackupLabel}

# check sanity
sanity

echo CAUTION: this script will stop eXist-db and create a backup and store that to ${BackupFolderDate}
read -p "Continue (y/n)? " choice
case "$choice" in
  y|Y ) echo Continuing ..;;
  n|N ) abort_installation;;
  * ) abort_installation;;
esac

# Check current status of eXist-db
echo systemctl status eXist-db.service:
systemctl status eXist-db.service | head -n 3
# Stop eXist-db
echo Stopping eXist-db ...
echo systemctl stop eXist-db.service
systemctl stop eXist-db.service | head -n 3

# wait till last command is executed
wait
# sleep for number of seconds
sleep 8
echo systemctl status eXist-db.service
systemctl status eXist-db.service | head -n 3

# Create backup of the data directory
echo Creating backup of ${OrgFolder} to ${BackupFolderDate}
cp -rp ${OrgFolder} ${BackupFolderDate}
wait

# Start eXist-db
echo Starting eXist-db ...
echo systemctl start eXist-db.service
systemctl start eXist-db.service | head -n 3
# wait till last command is executed
wait
# sleep for number of seconds
sleep 8
echo systemctl status eXist-db.service
systemctl status eXist-db.service | head -n 3

# Report backup
echo Done. eXist-db started. Backup is saved as: ${BackupFolderDate}

# EOF
