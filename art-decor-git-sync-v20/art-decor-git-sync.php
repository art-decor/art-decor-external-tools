<?php
/*
	ADGITSY art-decor github synchronizer

    Copyright (C) 2019-2024 ART-DECOR Expert Group art-decor.org
    Copyright (C) 2019-2024 ART-DECOR Open Tools art-decor-open-tools.net
    
    Author: Kai U. Heitmann
*/

// name and version of this script
$thisscriptname = "ADGITSY";
$thisscriptversion = "v2.0";

date_default_timezone_set('Europe/Berlin');

/* some inits */
$projects2process = array();

/* get configs */
require("config.php");
// var_dump($supportedGitServers);exit;

if (!is_array($supportedGitServers)) {
	echo "+++ERROR No git servers defined in config file config.php\n";
	return;
}

require_once('lib/Git.php');

echo "[" . $thisscriptname . " " . $thisscriptversion . "]\n";

foreach ($projects2process as $r) {
	if (is_file($r . "-config.php")) {
		$nu = gmdate('Y-m-d H:i:s', time());   /* readable format */
		$startstamp = strtotime('now');
		echo "Processing $r at $nu \n";
		processGitHub ($r);
		$nu = gmdate('Y-m-d H:i:s', time());   /* readable format */
        $endstamp = strtotime('now');
		$diff = $endstamp-$startstamp;
        $seconds_total = $diff % 60;
		$minutes_total =floor($diff / 60);
		echo "Finished $r at $nu , time elapsed: " . $minutes_total . "m " . $seconds_total . "s\n";
	} else {
		echo "+++ERROR config file for " . $r . " not found, please check...\n";
	}
}

echo "*** Konec\n";

exit;



function processGitHub($repositoryname)
{
	global $supportedGitServers;
	global $supportedArtefactTypes;

	$currentdir = getcwd();

	$config = $repositoryname . "-config.php";

	if (is_file($config)) {
		include($config);
	} else {
		echo "+++ERROR repository config not properly configured: $config not found.\n";
		return;
	}

	if (!is_array($gitrepo)) {
		echo "+++ERROR No supported repositories defined in config file $config\n";
		return;
	}

	$gitservername2use = isset($gitrepo["gitserver"]) ? $gitrepo["gitserver"] : null;
	$gitserver2use = isset($supportedGitServers[$gitservername2use]) ? $supportedGitServers[$gitservername2use] : null;

	if (!$gitserver2use) {
		echo "+++ERROR No git server defined or requested git server $gitservername2use cannot be found in config file $config\n";
		return;
	}

	// copy some information from the config
	$name = $gitrepo["name"] ;
	$localdir = $gitrepo["local"] ;
	$branch = $gitrepo["branch"] ;
	$artdecormode = $gitrepo["artdecormode"] ;

	// credentials, maybe should be in a separate file?
	$credentials = $gitrepo["credentials"] ;
	if ($credentials) {
		$git_access_token = $credentials["git_access_token"];
		$git_user = $credentials["git_user"];
		$git_userdisplayname = $credentials["git_userdisplayname"];
		$git_email = $credentials["git_email"];
	} else {
		echo "+++ERROR No access defined for $gitservername2use in $config\n";
		return;
	}

	// some inits
	// determine a tmp file in the local tmp folder
	$TMPFOLDER = "tempgit";
	$TMPDIR = $TMPFOLDER . "/git-" . $localdir;
	if (!rrmdir($TMPDIR)) {
		echo "+++ERROR $TMPDIR cannot be completly removed.\n";
		return;
	}
	if (!mkdir ($TMPDIR)) {
		echo "+++ERROR An empty $TMPDIR cannot be created.\n";
		return;
	}

	$totalfilelist = array();

	// build the access string for now, that is a variation of the repository mentioned in the project config file
	// i.e. "https://<accesstoken>@github.com/name/repo.git";
	$targetrepourl = $gitrepo["repository"];
	$targeturlwithcredentials = "";
	if (strncmp_startswith($targetrepourl, "https://")) {
		switch ($gitservername2use) {
			case 'github.com':
				$targeturlwithcredentials = preg_replace( '/https:\/\//', "https://" . $git_access_token . "@" , $targetrepourl);
				break;
			case 'gitlab.com':
				$targeturlwithcredentials = preg_replace( '/https:\/\//', "https://" . $git_user . ":" . $git_access_token . "@" , $targetrepourl);
				break;
		}
		if (!$targeturlwithcredentials) {
			echo "+++ERROR repository name '" . $targetrepourl . "' is not formatted as expected, credentials cannot be determined\n";
			return;
		}
	} else {
		echo "+++ERROR repository name '" . $targetrepourl . "' is not formatted as expected, it must start with https://\n";
		return;
	}

	// echo $targetrepourl . "\n"; echo $string_after . "\n";exit;
	//$repo = Git::open($git);
	// clone the target remote repo into the tmp directory and then return GitRepo object
	// var_dump($targeturlwithcredentials);exit;
	echo "... [$repositoryname] Cloning of the repo '$targetrepourl'\n";
	$repo = strlen($targeturlwithcredentials) > 0 ? Git::clone_remote($TMPDIR, $targeturlwithcredentials, true) : null;
	if (Git::is_repo($repo)) {
		echo "... [$repositoryname] '$targetrepourl' cloned\n";
	} else {
		echo "+++ERROR cannot create a temporary new clone of the repo '$targetrepourl'\n";
		return;
	}
	// set username and email
	$repo->set_username_email($git_userdisplayname, $git_email);

	// var_dump($gitrepo["artefacts"]);exit;
	foreach ($gitrepo["artefacts"] as $artefact) {

		// store artefact url (source) and target dir
		// var_dump($artefact);exit;
		$artefacttype = $artefact["type"];
		if (!in_array($artefacttype, $supportedArtefactTypes)) {
			echo "+++ERROR '$artefactType' is not supported\n";
			return;
		}
		$artefactsourceserverapi = $artefact["sourceserverapi"];
		$artefactproject = $artefact["project"];
		$artefactcategory = $artefact["category"];
		$targetdir = $artefact["targetdir"];
		$onlyartefacts = $artefact["onlyartefacts"] ;
		$exportformats = $artefact["formats"] ;

		echo "... [$repositoryname] Processing artefacts category $artefactcategory for target folder '$targetdir' method $artefacttype\n";

		/*
			pre-process the artefact category: get a list of all artefacts of the requested category from the project

			VS
				<list xmlns:json="http://www.json.org" artifact="VS" elapsed="59" current="29" total="29" all="0" resolve="true" lastModifiedDate="2024-05-14T19:58:20.816+02:00" project="demo10-">
					<valueSet json:array="true" uuid="27346db4-d761-4ce2-b46d-b4d7611ce2cf" id="1.3.6.1.4.1.12009.10.1.2208" name="LL355-9" displayName="GCS_1_Eye" effectiveDate="2023-08-15T00:00:00" statusCode="final" versionLabel="Loinc_2.75" lastModifiedDate="2023-08-15T15:28:00" canonicalUri="http://loinc.org/vs/LL355-9">
						<valueSet json:array="true" uuid="98f6f75e-2bd4-41ed-b6f7-5e2bd4c1ed34" id="1.3.6.1.4.1.12009.10.1.2208" name="LL355-9" displayName="GCS_1_Eye" effectiveDate="2023-08-15T00:00:00" statusCode="final" versionLabel="Loinc_2.75" lastModifiedDate="2023-08-15T15:28:00" canonicalUri="http://loinc.org/vs/LL355-9"/>
					</valueSet>
					<valueSet json:array="true" uuid="3402d2ff-6e09-43e0-82d2-ff6e0953e015" id="1.3.6.1.4.1.12009.10.1.2209" name="LL356-7" displayName="GCS_2_Verbal" effectiveDate="2023-08-15T00:00:00" statusCode="final" versionLabel="Loinc_2.75" lastModifiedDate="2023-08-15T15:28:00" canonicalUri="http://loinc.org/vs/LL356-7">
						<valueSet json:array="true" uuid="56a30948-94da-4afd-a309-4894da9afdf8" id="1.3.6.1.4.1.12009.10.1.2209" name="LL356-7" displayName="GCS_2_Verbal" effectiveDate="2023-08-15T00:00:00" statusCode="final" versionLabel="Loinc_2.75" lastModifiedDate="2023-08-15T15:28:00" canonicalUri="http://loinc.org/vs/LL356-7"/>
					</valueSet>
					...
			CS
				<list xmlns:json="http://www.json.org" artifact="CS" elapsed="13" current="5" total="5" all="5" resolve="true" lastModifiedDate="2024-05-14T20:04:48.724+02:00" project="ihede-">
					<codeSystem json:array="true" uuid="abc32fa9-cd91-4f1e-832f-a9cd912f1e8e" id="1.2.276.0.76.5.114" name="S_BAR2_WBO" displayName="S_BAR2_WBO" url="http://localhost:8877/exist/apps/decor/services/" ident="ihede-" ref="1.2.276.0.76.5.114">
						<codeSystem json:array="true" uuid="174742d6-5d54-40c9-8742-d65d54c0c962" ref="1.2.276.0.76.5.114" name="S_BAR2_WBO" displayName="S_BAR2_WBO" url="http://localhost:8877/exist/apps/decor/services/" ident="ihede-"/>
					</codeSystem>
					<codeSystem json:array="true" uuid="233e9018-75af-46da-be90-1875af96da56" id="1.2.276.0.76.11.465" name="valueset-kdl-2020" displayName="Klinische Dokumentenliste KDL 2020" url="http://localhost:8877/exist/apps/decor/services/" ident="ihede-" ref="1.2.276.0.76.11.465">
						<codeSystem json:array="true" uuid="104495f8-960b-4d82-8495-f8960bcd82a6" ref="1.2.276.0.76.11.465" name="valueset-kdl-2020" displayName="Klinische Dokumentenliste KDL 2020" url="http://localhost:8877/exist/apps/decor/services/" ident="ihede-"/>
					</codeSystem>
					<codeSystem json:array="true" uuid="13195cef-9208-4b0f-995c-ef92084b0ff1" id="1.2.840.10008.6.1.2" name="DICOM_Anatomic_Region" displayName="DICOM PS3.16 CID 3 Anatomic Region" url="http://localhost:8877/exist/apps/decor/services/" ident="ihede-" ref="1.2.840.10008.6.1.2">
						<codeSystem json:array="true" uuid="e54af340-2ce5-4b82-8af3-402ce59b82a3" ref="1.2.840.10008.6.1.2" name="DICOM_Anatomic_Region" displayName="DICOM PS3.16 CID 3 Anatomic Region" url="http://localhost:8877/exist/apps/decor/services/" ident="ihede-"/>
					</codeSystem>
					...

		*/
		$artefacturl = "";
		$artefactprefix = "";
		switch ($artefactcategory) {
			case 'VS':
				$artefactprefix = "ValueSet-";
				$artefacturl = "$artefactsourceserverapi/api/valueset?prefix=$artefactproject";
				$pickoneartefacturl["decor"] = "$artefactsourceserverapi/api/valueset/{id}/{effectiveDate}/\$extract?prefix=$artefactproject";
				$pickoneartefacturl["fhir/4.0"] = "$artefactsourceserverapi/$artefacttype/$artefactproject/ValueSet/{id}/{effectiveDate}";
				// URL how to get the FHIR bundle of the artefacts: "http://localhost:8877/exist/apps/fhir/4.0/elgabbr-/ValueSet?_count=999"
				break;			
			case 'CS':
				$artefactprefix = "CodeSystem-";
				$artefacturl = "$artefactsourceserverapi/codesystem?prefix=$artefactproject";
				$pickoneartefacturl["decor"] = "$artefactsourceserverapi/codesystem/{id}/{effectiveDate}/\$extract?prefix=$artefactproject";
				$pickoneartefacturl["fhir/4.0"] = "$artefactsourceserverapi/$artefacttype/$artefactproject/CodeSystem/{id}/{effectiveDate}";
				break;
			default:
				echo "+++ERROR Unknow artefact category '$artefactcategory'\n";
				return;
				break;
		}
		if (!$artefacturl) {
			echo "+++ERROR Cannot get list from '" . $artefacturl . "' for transfer into repo dir\n";
			return;
		}

		echo "...... [$repositoryname] Get list from '" . $artefacturl . "', transferring into repo dir '" . $targetdir . "'\n";

		// GET the list of artefacts
		// create new cURL-Handle
		$ch = curl_init();
		// url and other options
		curl_setopt($ch, CURLOPT_URL, $artefacturl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Accept: application/xml',
		]);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// action
		$result = curl_exec($ch);
		curl_close($ch);
		// save the result in local tmp folder and check the bundle
		$BUNDLEFILE = $TMPDIR . ".xml";
		file_put_contents ($BUNDLEFILE, $result);
		$bundle = simplexml_load_string($result);

		// <list artifact="VS" elapsed="40" current="67" total="67" all="0" resolve="true" lastModifiedDate="2024-05-14T20:13:01.189+02:00" project="demo11-">
		// <valueSet uuid="..." id="2.16.840.1.113883.3.1937.99.60.11.11.8" name="demo11-single-abo" displayName="Single ABO Blood Group" effectiveDate="2022-02-17T00:00:00" statusCode="draft" lastModifiedDate="2022-05-05T13:46:05">
    
		$bundleCount = isset($bundle) ? $bundle["total"] : -1;

		// counters and infos
		$checked = 0;
		$filelist = array();

		$artefactlist = array();
		if ($bundleCount > 0) {
			foreach ($bundle->children() as $axml) {
				$ident = $axml["ident"];
				$aid = trim($axml["id"]);
				$aeffectiveDate = $axml["effectiveDate"];
				if (!$ident and $aid and $aeffectiveDate) {  // means this is an artefact defined in the project (and not referenced only)
					$aid = trim($axml["id"]);
					$aname = $axml["name"];
					$adisplayName = $axml["displayName"];
					$aeffectiveDate = $axml["effectiveDate"];
					if (!$aeffectiveDate) var_dump($axml);
					$aeffectiveDateshort = preg_replace( '/[\-:T]/', "", $aeffectiveDate);
					// echo "$aeffectiveDate - $aeffectiveDateshort\n";
					$acanoncial = $axml["canonicalUri"];
					$selected = "*";  // assume first, all artefacts are to be processed = put onto the list stack
					if (count($onlyartefacts) > 0) {
						// echo "CANONICAL: $acanoncial\n"; // var_dump($axml);
						// echo "VERSIONLABEL: " . $axml["versionLabel"] . "\n"; 
						if ($axml["versionLabel"] == '4.0-beta.1') var_dump($axml);
						$selected = "-";   // now assume that no artefact is to be processed = put onto the list stack
						foreach ($onlyartefacts as $oa) {
							// pattern to match is:
							//    A 1.2.40.0.34.10.44                    all versions with exact that id
							//    B 1.2.40.0.34.10.44--dynamic           only most recent version with that id
							//    C 1.2.40.0.34.10.44--20220217000000    only the version with that id denoted by that date
							//    D elga-runnersets                      all versions with that name
							//    E http://xxxxxx/elga-runnersets        all versions with that canonical uri
							// echo "$aid - $oa\n";
							if ($aid == $oa) {
								// echo "A pattern match $aid - $oa\n";
								$selected = "A";
							} else if ($aid == before("--dynamic", $oa)) {
								// echo "B pattern match $aid - $oa\n";
								$selected = "B";
							} else if ( ($aid == before("--", $oa)) and ($aeffectiveDateshort == after("--", $oa)) ) {
								// echo "C pattern match $aid - $oa\n";
								$aeffectiveDate = preg_replace( '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/', "$1-$2-$3T$4:$5:$6", after("--", $oa));
								$selected = "C";
							} else if ($aname == $oa) {
								// echo "D pattern match $aname - $oa\n";
								$selected = "D";
							} else if ($acanoncial == $oa) {
								// echo "E pattern match $acanoncial - $oa\n";
								$selected = "E";
							}
						}
					}
					switch ($selected) {
						case 'B':
						case 'C':
							// these mean *exactly the one* artefact to put onto the list stack
							$artefactlist[] = array(
								"artefactprefix" => $artefactprefix,
								"aid" => $aid,
								"aname" => "$aname",
								"adisplayName" => "$adisplayName",
								"aeffectiveDate" => "$aeffectiveDate",
								"aeffectiveDateshort" => "$aeffectiveDateshort",
								"acanoncial" => "$acanoncial",
							);
							break;
						case 'A':
						case 'D':
						case 'E':
						case '*':
							// these mean *all* versions of the artefact to put onto the list stack
							// '*'' means also *all* versions of *all* artefacts at the end
							// echo "-----\n" . $axml->asXML() . "\n-----\n";
							foreach ($axml->children() as $axs) {
								// var_dump($axs);
								$artefactlist[] = array(
									"artefactprefix" => $artefactprefix,
									"aid" => trim($axs["id"]),
									"aname" => $axs["name"],
									"adisplayName" => $axs["displayName"],
									"aeffectiveDate" => $axs["effectiveDate"],
									"aeffectiveDateshort" => preg_replace( '/[\-:T]/', "", $axs["effectiveDate"]),
									"acanoncial" => $axs["canoncialUri"],
								);
							}
							break;
						default:
							break;
					}
				}
				// if (!$ident) echo "$artefactprefix$name\n";
			}
		}
		// var_dump($artefactlist);

		$bundleCount = count($artefactlist);

		/* ------- process all supported artefact types 'fhir/4.0' | 'decor' ------- */
		if ( $bundleCount > 0 ) {

			echo "...... [$repositoryname] $bundleCount artefact candidate(s) found in $artefacturl\n";

			echo "...... [$repositoryname] GitHub repo $targetrepourl contacted, checking content\n";
			// store this dir path and go to the destination per artefact
			chdir($TMPDIR);
			// echo "DIR: " . getcwd() . "\n";
			if(!is_dir($targetdir)) {
				// No targetdir yet, try to create the nested structure with $recursive parameter specified
				if (mkdir($targetdir, 0700, true)) {
					$repo->add($targetdir);
					$filelist[] = $targetdir;
					echo "...... [$repositoryname] Target directory '$targetdir' created\n";
				} else {
					echo "+++ERROR cannot access or create target dir '$targetdir' in local repository\n";
					return;
				}
			} else {
				echo "...... [$repositoryname] Target directory is '$targetdir'\n";
			}

			echo "...... [$repositoryname] Checking each artefact for export format(s): " . implode(", ", $exportformats) . "\n";

			// process list bundle
			foreach ($artefactlist as $aaa) {

				$checked++;

				$artefactprefix = $aaa["artefactprefix"];

				foreach ($exportformats as $exportformat) {

					// create name of the artefact xml
					$afullid = $aaa["aid"] . "--" . $aaa["aeffectiveDateshort"];
					$afn = (strlen($targetdir) > 0 ? $targetdir . "/" : "") . $artefactprefix . $afullid . "." . $exportformat ;
					// echo "$afn\n";
					
					// GET this artefact
					// $picker = "http://localhost:8877/exist/apps/fhir/4.0/$artefactproject/ValueSet/{id}/{effectiveDate}";
					$picker = $pickoneartefacturl[$artefacttype];
					$picker = preg_replace( '/\{id\}/', $aaa["aid"], $picker);
					$aed = preg_replace( '/[:]/', "%3A", $aaa["aeffectiveDate"]);  // replace the : in the date
					$picker = preg_replace( '/\/\{effectiveDate\}/', strlen($aed) > 0 ? "/" . $aed : "" , $picker);

					// echo "$picker\n";

					// create new cURL-Handle
					$ch = curl_init();
					// url and other options
					curl_setopt($ch, CURLOPT_URL, $picker);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, [
						'Accept: application/' . $exportformat,
					]);
					// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					// action
					$result = curl_exec($ch);
					curl_close($ch);
					
					// if ($aaa["aeffectiveDate"] == '2022-03-04T12:58:48') var_dump($result);

					$newfilecontents = $result;
					$oldfilecontents = "";
					if (is_file($afn)) {
						$oldfilecontents = file_get_contents ($afn);
					}
					if (strlen($oldfilecontents) == 0 or $oldfilecontents != $newfilecontents) {
						echo "...... [$repositoryname] ...adding $artefactprefix artefact " . $afn;
						file_put_contents ($afn, $newfilecontents);
						$repo->add($afn);
						$filelist[] = $afn;
						$totalfilelist[] = $afn;
						echo " done\n";
					}

				}

			}
		} else {
			echo "...... [$repositoryname] +++ERROR cannot properly parse content for artefact type " . $artefacttype . "\n";
			echo "----\n" . substr($result, 1, 170) . "\n----\n";
		}

		// add an extra file to target dir if files have changed for info
		if (count($filelist) > 0) {
			$now = date("Y-m-d H:m:s");
			$infofilename = $targetdir . "/info.md";
			$info = "# " . $gitrepo["name"] . "\n\n";
			$info .= "+++ Please do not manually edit this file +++\n\n";
			$info .= "The following files in this directory are synchronized from ART-DECOR® (recently at " . $now . ", mode: " . $artdecormode . ")\n\n";
			foreach ($totalfilelist as $f) {
				$info .= "* " . $f . "\n";
			}
			file_put_contents ($infofilename, $info);
			$repo->add($infofilename);
			echo "...... [$repositoryname] Info File added\n";
		}
		// if anything changed update lastupdate.md and commit
		if (count($filelist) > 0) {
			// do commit
			try {
				echo "...... [$repositoryname] committing to local repository\n";
				// set commit message
				$repo->commit("Commit from ART-DECOR® (mode: " . $artdecormode . ")");
				echo "...... [$repositoryname] pushing to repo $targetrepourl branch $branch... ";
				$repo->push('origin', $branch);
				echo "done\n";
			} catch(Exception $e) {
				if (strpos($e->getMessage(), "nothing to commit")) {
					echo "$checked checked, nothing to commit\n";
				} else {
					echo "not done\n";
					echo $e->getMessage() . "\n";
				}
			}
		} else {
			echo "...... [$repositoryname] $checked checked, nothing to commit\n";
		}
		chdir($currentdir);

		// delete temp objects
		// unlink($BUNDLEFILE);
		echo "...... [$repositoryname] temp files removed\n";

		echo "... [$repositoryname] Finished artefacts $artefacttype for $targetdir\n";
	}

	// change lastupdate.md if anything has changed
	if (count($totalfilelist) > 0) {
		chdir($TMPDIR);
		$now = date("Y-m-d H:m:s");
		$lupfilename = "lastupdate.md";
		$currentlup = is_file($lupfilename) ? file_get_contents ($lupfilename) : "";
		$lup = "";
		$remainder = "";
		if (strlen($currentlup) > 0) {
			$lup .= before("##", $currentlup);
			$remainder = after("##", $currentlup);
		} 
		if (strlen($lup) == 0) {
			$lup = "# " . $gitrepo["name"] . "\n\n";
			$lup .= "Synchronized from ART-DECOR®.\n\n";
			$lup .= "+++ Please do not manually edit this file +++\n\n";
		}
		$lup .= "## List of updated files on " . $now . "\n\n";
		foreach ($totalfilelist as $f) {
			$lup .= "* " . $f . "\n";
		}
		if ($remainder) {
			$lup .= "\n\n##" . $remainder;
		}
		chdir($currentdir);
		chdir($TMPDIR);
		file_put_contents ($lupfilename, $lup);
		$repo->add($lupfilename);
		// do last commit
		try {
			echo "... [$repositoryname] Finally committing " . $lupfilename . " to local repository... ";
			// set commit message
			$repo->commit("Commit from ART-DECOR® (mode: " . $artdecormode . ")");
			$repo->push('origin', $branch);
			echo "done\n";
		} catch(Exception $e) {
			if (strpos($e->getMessage(), "nothing to commit")) {
				echo "$checked checked, nothing to commit\n";
			} else {
				echo "not done\n";
				echo $e->getMessage() . "\n";
			}
		}
	}
}


/*
 *  rrmdir - recursive remove directory
 *  function to remove a complete directory including all subdirectories and entries
 *  param dir - directory path to be deleted
 */
function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir);
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
           rrmdir($dir. DIRECTORY_SEPARATOR .$object);
         else
           unlink($dir. DIRECTORY_SEPARATOR .$object); 
       } 
     }
     if (!rmdir($dir)) {
		if (!unlink($dir. DIRECTORY_SEPARATOR . ".DS_Store")) {
			var_dump(scandir($dir));
			return FALSE;
		} else {
			rmdir($dir);
		}
	 }
   }
   return TRUE;
 }

function strncmp_startswith($haystack, $needle) {
	return strncmp($haystack, $needle, strlen($needle)) === 0;
}

function before ($s1, $inthat) {
        if ($inthat == null) return null;
        return substr($inthat, 0, strpos($inthat, $s1));
}

function after ($s1, $inthat) {
        if (!is_bool(strpos($inthat, $s1)))
            return substr($inthat, strpos($inthat,$s1)+strlen($s1));
}
	
?>