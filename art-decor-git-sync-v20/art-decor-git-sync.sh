#!/bin/sh
#

DIR=/opt/art-decor-git-sync-v10
DATE=`date +%Y%m`
LOG=logs/log-${DATE}.txt

cd ${DIR}

php art-decor-git-sync.php $* >> ${LOG}

# cleanup log files older than 50 days
find logs/* -mtime +50 -exec rm {} \;
