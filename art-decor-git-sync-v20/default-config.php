<?php

	// list of settings for projects to sync with git
	$gitrepo = array (
		// name of the git process
		"name" => "...",
		// git to use, can be github.com | gitlab.com | ...
		"gitserver" => "gitlab.com",
		// complete https url of the repository to use, shall end on .git 
		"repository" => "https://gitlab.com/....git",
		// branch of the repository to use, must exist in the repository already
		"branch" => "main",
		// the credentials as an array, need to be kept as a secret
		"credentials" => array (     
			// the git user name of the bot intended to work on github, having proper rights
			"git_user" => "...",
			// git user name to display
			"git_userdisplayname" => "...",
			// git email
			"git_email" => "...",
			// the secret password of that user, if needed
			"git_password" => "...",
			// personal access token
			"git_access_token" => "..."
		),
		// name of the local temp directory, shall be something with a uuid, must be unique
		"local" => "...-f6f1344d-4eba-451c-a10c-c082c341c450",
		// mode of operation, can be forced-commit-to-repository | copy-to-art-decor-cache
		"artdecormode" => "forced-commit-to-repository",
		// array of artefact definitions to be checked, analyzed and possibly checked in and pushed to repo
		"artefacts" => array (
			array (
				// server URL to the api where the sources of the artifcats reside, must not end on /
				"sourceserverapi" => "http://localhost:8877/exist/apps",
				// project prefix to use
				"project" => "...",
				// type of artefacts (determines also extract endpoint): fhir/4.0 | decor
				"type" => "fhir/4.0",
				// category of artefact: VS | CS
				"category" => "VS",
				// array of export format(s) of artefacts: xml | json
				"formats" => ['xml', 'json'],
				// the target dir in the repo where to put the artefacts
				"targetdir" => "...",
				// if non-empty, only artefacts that matches any of the list items will be synchronized
				//   "1.2.3.4.5.6.7",                   // all versions of that artefact
				//   "1.2.3.4.5.6.7--dynamic",          // only most recent version of that artefact
				//   "1.2.3.4.5.6.7--20220217000000",   // only this version of that artefact
				//   "demo11-malformations",            // all versions of that artefact denoted by name
				//   "http://fhir.de/ValueSet/../.."    // all versions of that artefact denoted by canonical uri
				"onlyartefacts" => array (
					 "1.2.3.4.5.6.7"
				)
			)
			,
			array (
				// ... a second array of artefact
			)
		)
	)

?>