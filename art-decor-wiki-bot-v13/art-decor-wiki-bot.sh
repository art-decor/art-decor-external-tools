#!/bin/sh
# Routine for nightly wiki bot actions
#
#

# Home
ADSVN="/opt/art-decor-wiki-bot"

# Logfile
LOG=${ADSVN}"/adawib.log"

cd $ADSVN

DATE=`date +%Y%m%d-%H%M`
echo "------${DATE} started" >> ${LOG}

php art-decor-wiki-bot.php >> ${LOG}

# delete tmp dirs older than 15 days
find tmp-2* -type d -ctime +15 -exec rm -rf {} \; 2> /dev/null
