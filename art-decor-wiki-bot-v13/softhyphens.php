<?php

/*  Soft Hyphenations List of Names
	List of String Patterns where # is replaced by zero length whitespace &#8203;
	
	CAVE
	[ must be escaped: \[
 */
	
$softhyphenlist = array(
"@context#Control#Code",
"@context#Conduction#Ind",
"@determiner#Code",
"data#element",
"hl7:administrative#Gender#Code",
"hl7:assigned#Authoring#Device",
"hl7:assigned#Person",
"hl7:associated#Entity",
"hl7:associated#Person",
"hl7:discharge#Disposition#Code",
"hl7:encompassing#Encounter",
"hl7:ethnic#Group#Code",
"hl7:guardian#Person",
"hl7:guardian#Organization",
"hl7:health#Care#Facility",
"hl7:information#Recipient",
"hl7:intended#Recipient",
"hl7:language#Communication",
"hl7:language#Code",
"hl7:manufactured#Material",
"hl7:manufacturer#Model#Name",
"hl7:marital#Status#Code",
"hl7:proficiency#Level#Code",
"hl7:preference#Ind",
"hl7:received#Organization",
"hl7:religious#Affiliation#Code",
"hl7:represented#Custodian#Organization",
"hl7:represented#Organization",
"hl7:responsible#Party",
"hl7:scoping#Organization",
"hl7:service#Provider#Organization",
"hl7:target#Site#Code",
"pharm:capacity#Quantity",
"pharm:container#Packaged#Medicine",
"TS.#DATE.#MIN",
"TS.#DATE#TIME.#MIN",
"INT.#NONNEG",




"[hl7:templateId#[@root#=#",
"[hl7:templateId #[@root#=#",
"[hl7:name/#@code#=#",


"Eingebettetes#ObjektEntry",
"Hospital#discharge#studies#summary#section",
"Body#NonXMLBody#Embedded",
"Body#NonXMLBody#Referenced",
"Header#Encompassing#Encounter",
"Header#Participant#Kostentraeger",
"Header#Information#Recipient",
"Header#Record#Target",
"-data#element#",




"sentinel"

);
	
?>