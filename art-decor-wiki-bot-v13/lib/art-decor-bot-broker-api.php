<?php
/*
 *
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
 *  see https://art-decor.org/mediawiki/index.php?title=Copyright
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the
 *  GNU Lesser General Public License as published by the Free Software Foundation; either version
 *  2.1 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
 *
 *  - - - - - - - - - - -
 *  Broker Library for all supported API's to support the ART-DECOR® Automatic Wiki Bot (ADAWIB)
 *  Supported platforms so far: mediawiki, confluence
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools 2013-2020
 */

class ARTDECORBotTargetFramework
{
    /* Library with API funtions, all typically constructed as
     *
     *   main generic function
     *   --> invokes main framework specific function, transparently
     *
     *  __construct
     *  login
     *  query
     *  checktoken
     *  getedittoken
     *  getpage
     *  editpage
     *  log
     */

    private $http;
    private $token;
    private $header;
    private $ecTimestamp;
    private $namespace;
    private $repeatOnHTTPError = 0;

    public $url;
    public $framework;
    public $debuglevel = 0;  /* 0 = see nothing , 1, 2, 3 or 4 = see all, 5 = crazy */

    const CONFLUENCE_ART_DECOR_NAMESPACE_KEY = 'ARTDECOR';
    //const EXTRACT_ID_4_CANONICALIZE1 = '/id="(_[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12})"/i';
    const EXTRACT_ID_4_CANONICALIZE1 = '/id="(_[\w+-]*)"/i';
    const EXTRACT_ID_4_CANONICALIZE2 = '/id="(__[\w+_]*)"/i';

    /**
     * This is our constructor.
     * @return void
     **/
    function __construct($url, $framework, $namespace = null, $hu = null, $hp = null) {
        $this->http = new http;
        $this->token = null;
        $this->url = $url;
        if ($namespace == null)
            /* use default namespace key here */
            $this->namespace = self::CONFLUENCE_ART_DECOR_NAMESPACE_KEY;
        else
            $this->namespace = urlencode($namespace);
        $this->framework = $framework;
        $this->ecTimestamp = null;
        if ($hu !== null)
            $this->http->setHTTPcreds($hu, $hp);
        // exception handler
        @set_exception_handler(array($this, 'exception_handler'));
    }

    public function exception_handler($exception) {
        $dieOnException = false;

        // these are our templates
        $traceline = "#%s %s(%s): %s(%s)";
        $msg = "\n--------------------------";
        $msg .= "\n+++ PHP Fatal error:  Uncaught exception '%s' with message '%s' in %s:%s";
        $msg .= "\n+++ Stack trace:\n%s\n+++  thrown in %s on line %s";

        // alter your trace as you please, here
        $trace = $exception->getTrace();
        foreach ($trace as $key => $stackPoint) {
            // I'm converting arguments to their type
            // (prevents passwords from ever getting logged as anything other than 'string')
            $trace[$key]['args'] = array_map('gettype', $trace[$key]['args']);
        }

        // build your tracelines
        $result = array();
        foreach ($trace as $key => $stackPoint) {
            $result[] = sprintf(
                $traceline,
                $key,
                $stackPoint['file'],
                $stackPoint['line'],
                $stackPoint['function'],
                implode(', ', $stackPoint['args'])
            );
        }
        // trace always ends with {main}
        $result[] = '+++#' . ++$key . ' {main}';

        // write tracelines into main template
        $msg = sprintf(
            $msg,
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine(),
            implode("+++\n", $result),
            $exception->getFile(),
            $exception->getLine()
        );

        // log or echo as you please
        //echo ($msg);
        echo "\n+++ Exception Caught: ". $exception->getMessage() ."\n";
        if ($dieOnException === true) die;
    }

    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: login ----
     * -------------------------------------
     */
    /**
     * This function takes a username and password and logs you into the target environment.
     * @param $user Username to login as.
     * @param $pass Password that corresponds to the username.
     * @return The api result, an array
     **/
    function login ($user, $pass, $token = '') {
        // check the API first, then login
        if ($this->framework=='mediawiki') {
            return $this->loginMW ($user, $pass, $token);
        } else if ($this->framework=='confluence') {
            $checkret = $this->queryCF(null, null, 0);
            if ($checkret == null) {
                throw new Exception("API error: it seems that the URL in the parameters is not a proper API\n");
                return false;
            }
            return $this->loginCF ($user, $pass, $token);
        } else if ($this->framework=='wordpress') {
            $checkret = $this->queryCF(null, null, 0);
            if ($checkret == null) {
                throw new Exception("API error: it seems that the URL in the parameters is not a proper API\n");
                return false;
            }
            return $this->loginWP ($user, $pass, $token);
        }
        else {
            return $this->loginDEBUG ($user, $pass, $token);
        }
    }
    private function loginMW ($user, $pass, $token = '') {
        $repeat = 3;  // try some times
        $ret = false;
        if ($repeat-- > 0 && !$ret) {
            // phase 1: get a login token, this is now required - see https://bugzilla.wikimedia.org/show_bug.cgi?id=23076
            $ret = $this->queryMW('?action=query&meta=tokens&type=login&format=php', array(), $this->repeatOnHTTPError);
            $logintoken = "";
            if (isset($ret['query']['tokens']['logintoken'])) $logintoken = $ret['query']['tokens']['logintoken'];
            if ($logintoken != "") {
                $post = array ("username" =>  $user, "password" => $pass, "logintoken" => $logintoken, "loginreturnurl" => $this->url);
                $ret = $this->queryMW('?action=clientlogin&format=php', $post, $this->repeatOnHTTPError);
            }
        }
        if ($ret['clientlogin']['status'] != 'PASS') {
            throw new Exception("Login error, error code " . $this->http->http_code() . " (" . unserialize($ret) . ") for user " . $user);
            die();
        } else {
            return $ret;
        }
    }
    private function loginCF ($user, $pass, $token = '') {
        /* set "token" in header for Basic Authentication, prefer api token over password */
        $key = ($token === '') ? $pass : $token;
        $this->token = base64_encode($user . ':' . $key);
        $header = array(
            "Authorization: Basic " . $this->token,
            "Content-Type: application/json",
            "Accept: application/json");
        $this->http->set_header($header);
        return true;
    }
    private function loginWP ($user, $pass, $token = '') {
        /* set "token" in header for Bearer Authentication */
        $post = array(
            "username" => $user,
            "password" => $pass,
            "permission_callback" => "__return_true"
        );
        $l = $this->http->post($this->url . "/jwt-auth/v1/token", $post);
        $lj = json_decode($l);
        if (!$lj) return false;
        // var_dump($lj);
        $this->token = $lj->token;
        $header = array(
            "Authorization: Bearer " . $this->token,
            "Content-Type: application/json",
            "Accept: application/json");
        $this->http->set_header($header);
        $v = $this->http->post($this->url . "/jwt-auth/v1/token/validate", null);
        $vl = json_decode($v);
        // var_dump($vl);
        if ($vl->code != 'jwt_auth_valid_token') return false;
        return true;
    }
    private function loginDEBUG ($user, $pass, $token = '') {
        return 'loginCF: login';
    }

    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: query ----
     * -------------------------------------
     */
    /**
     * Sends a query to the api.
     * @param $query The query string.
     * @param $post POST data if its a post request (optional).
     * @return The api result
     **/
    function query ($query, $post=null, $extraheaders=array()) {
        if ($this->framework=='mediawiki') {
            // get mediawiki page
            return $this->queryMW ($query, $post, $this->repeatOnHTTPError, $extraheaders);
        } else if ($this->framework=='confluence') {
            return $this->queryCF ($query, $post, $this->repeatOnHTTPError, $extraheaders);
        } else if ($this->framework=='wordpress') {
            return $this->queryWP ($query, $post, $this->repeatOnHTTPError, $extraheaders);
        } else {
            return $this->queryDEBUG ($query, $post, $this->repeatOnHTTPError, $extraheaders);
        }
    }
    private function queryMW ($query, $post=null, $repeat=0, $extraheaders=array()) {
        $time2wait = 3;  // start with 1 second to wait until retry, do it $repeat times
        if ($post==null) {
            $ret = $this->http->get($this->url . $query);
        } else {
            $ret = $this->http->post($this->url . $query, $post, $extraheaders);
            // echo "\nRET: " . $ret . "\n";
        }
        // check for api error, check for bad token and try re-login
        if (is_array($ret)) {
            if ($ret['error']['code'] == 'badtoken') {
                echo "...bad token, trying re-login...\n";
                $this->token = $this->getedittokenMW();
                if ($post==null) {
                    $ret = $this->http->get($this->url . $query);
                } else {
                    $ret = $this->http->post($this->url . $query, $post, $extraheaders);
                }
            }
        }
        $httpCode = $this->http->http_code();
        $qq = $this->before('login', $query) . "...";  // disguise
        if ($httpCode == "0") {
            echo "...stalled (code=undefined)...\n";
        } else if ($httpCode == "100") {
            // special treatment for error code 100 continue
            // wait seconds to calm the interface
            echo "...stalled (code=100), suspending for a while...\n";
            sleep($time2wait);
            if ($post==null) {
                $ret = $this->http->get($this->url . $query);
            } else {
                $ret = $this->http->post($this->url . $query, $post, $extraheaders);
            }
            if ($httpCode != "200")
                echo("HTTP Error code " . $this->http->http_code() . " for " . $this->url . " querying \"" . $qq . "\" - remaining retries: " . $repeat . "\n");
            return unserialize($ret);
        } else if ($httpCode != "200") {
            if ($repeat > 0) {
                    echo "...stalled_(code=" . $httpCode . "), retrying, repeat:" . $repeat . "...\n";
                    sleep($time2wait);  // wait a second, or so
                    return $this->queryMW($query, $post, --$repeat, $extraheaders);
            } else {
                    $qq = $this->before('login', $query) . "...";  // disguise
                    // throw new Exception("HTTP Error code " . $this->http->http_code() . " (" . $ret . ") for " . $this->url . " querying \"" . $qq . "\" - remaining retries: " . $repeat);
                    echo("HTTP Error code " . $this->http->http_code() . " for " . $this->url . " querying \"" . $qq . "\" - remaining retries: " . $repeat . "\n");
            }
        }
        return unserialize($ret);
    }
    private function queryCF ($query, $post=null, $repeat = 0, $extraheaders=array()) {
        $time2wait = 3;  // start with 1 second to wait until retry, do it $repeat times
        if ($post==null) {
            $ret = $this->http->get($this->url . $query);
        } else {
            $ret = $this->http->post($this->url . $query, $post, $extraheaders);
        }
        // echo "C" . $this->http->http_code() . " - ";
        $httpCode = $this->http->http_code();
        if ($httpCode == "404") {
            return false;
        } else if ($this->http->http_code() != "200") {
            if ($repeat > 0) {
                sleep($time2wait);  // wait a second, or so
                return $this->queryCF($query, $post, --$repeat, $extraheaders);
            } else {
                $qq = $this->before('login', $query) . "...";  // disguise
                // throw new Exception("HTTP Error code " . $this->http->http_code() . " (" . $ret . ") for " . $this->url . " querying \"" . $query . "\" - remaining retries: " . $repeat);
                echo("HTTP Error code " . $this->http->http_code() . " for " . $this->url . " querying \"" . $query . "\" - remaining retries: " . $repeat . "\n");
            }
        }
        return json_decode($ret);
    }
    private function queryWP ($query, $post=null, $repeat = 0, $extraheaders=array()) {
        $time2wait = 3;  // start with 1 second to wait until retry, do it $repeat times
        if ($post==null) {
            $ret = $this->http->get($this->url . $query);
        } else {
            $ret = $this->http->post($this->url . $query, $post, $extraheaders);
        }
        // echo "C" . $this->http->http_code() . " - ";
        $httpCode = $this->http->http_code();
        if ($httpCode == "404") {
            return false;
        } else if ($this->http->http_code() != "200") {
            if ($repeat > 0) {
                sleep($time2wait);  // wait a second, or so
                return $this->queryWP($query, $post, --$repeat, $extraheaders);
            } else {
                $qq = $this->before('login', $query) . "...";  // disguise
                // throw new Exception("HTTP Error code " . $this->http->http_code() . " (" . $ret . ") for " . $this->url . " querying \"" . $query . "\" - remaining retries: " . $repeat);
                echo("HTTP Error code " . $this->http->http_code() . " for " . $this->url . " querying \"" . $query . "\" - remaining retries: " . $repeat . "\n");
            }
        }
        return json_decode($ret);
    }
    private function queryDEBUG ($query, $post=null, $repeat = 0, $extraheaders=array()) {
        return 'queryDEBUG: query $query';
    }

    function setRepeatOnHTTPError ($repeat) {
        if (($repeat > 0) and ($repeat <= 20))
            // set repeat rate (and wait between retry of 1 second
            $this->repeatOnHTTPError = $repeat;
    }




    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: checktoken ----
     * -------------------------------------
     */
    /**
     * This function returns a new edit token for the current user if the check of
     * the current token failed
     * @return true if still valid, if re-newed return false.
     **/
    function checktoken () {
        if ($this->framework=='mediawiki') {
            // get mediawiki page
            return $this->checktokenMW ();
        } else if ($this->framework=='confluence') {
            return $this->checktokenCF ();
        } else if ($this->framework=='wordpress') {
            return $this->checktokenWP ();
        } else {
            return $this->checktokenDEBUG ();
        }
    }
    private function checktokenMW () {
        if ($this->queryMW('?action=checktoken&type=csrf&token=' . $this->token . '&format=php', null, $this->repeatOnHTTPError) != true) {
            $this->token = $this->getedittoken();
            return false;
        }
        return true;
    }
    private function checktokenCF () {
        return 'checktokenCF';
    }
    private function checktokenWP () {
        return 'checktokenWP';
    }
    private function checktokenDEBUG () {
        return 'checktokenDEBUG';
    }


    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: getedittoken ----
     * -------------------------------------
     */
    /**
     * This function returns the edit token for the current user.
     * @return edit token.
     **/
    function getedittoken () {
        if ($this->framework=='mediawiki') {
            // get mediawiki page
            return $this->getedittokenMW ();
        } else if ($this->framework=='confluence') {
            return $this->getedittokenCF ();
        } else if ($this->framework=='wordpress') {
            return $this->getedittokenWP ();
        } else {
            return $this->getedittokenDEBUG ();
        }
    }
    private function getedittokenMW () {
        $x = $this->queryMW('?action=query&prop=info&intoken=edit&titles=Main%20Page&format=php', null, $this->repeatOnHTTPError);
        foreach ($x['query']['pages'] as $ret) {
            return $ret['edittoken'];
        }
    }
    private function getedittokenCF () {
        return 'getedittokenCF';
    }
    private function getedittokenWP () {
        return 'getedittokenWP';
    }
    private function getedittokenDEBUG () {
        return 'getedittokenDEBUG';
    }


    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: getpage ----
     * -------------------------------------
     */
    /*
     * getpage
     * Gets the content of a page. Returns false on error.
     * @param $page The page name to fetch.
     */
    function getpage ($page) {
        if ($this->framework=='mediawiki') {
            // get mediawiki page
            return $this->getpageMW($page);
        } else if ($this->framework=='confluence') {
            return $this->getpageCF($page);
        } else if ($this->framework=='wordpress') {
            return $this->getpageWP($page);
        } else {
            return $this->getpageDEBUG($page);
        }
    }
    /**
     * Gets the content of a page. Returns false on error.
     * @param $page The wikipedia page to fetch.
     * @param $revid The revision id to fetch (optional)
     * @param $detectEditConflict wether to detect edit conflicts (optional)
     * @return The text for the page.
     **/
    private function getpageMW ($page, $revid=null, $detectEditConflict=false) {
        $append = '';
        if ($revid!=null)
            $append = '&rvstartid='.$revid;
            $x = $this->queryMW(
                '?action=query&format=php&prop=revisions&titles=' . urlencode($page) . '&rvlimit=1&rvprop=content|timestamp' . $append,
                null, $this->repeatOnHTTPError
        );
        if ($x['query']) {
            foreach ($x['query']['pages'] as $ret) {
                if (isset($ret['revisions'][0]['*'])) {
                    if ($detectEditConflict)
                        $this->ecTimestamp = $ret['revisions'][0]['timestamp'];
                    return $ret['revisions'][0]['*'];
                } else
                    return false;
            }
        } else {
            return false;
        }
    }
    private function getpageCF ($page) {
        $p = $this->queryCF("?title=" . rawurlencode($page) . "&spaceKey=$this->namespace&expand=body.storage",
            null, 0
        );
        // find out whether it is a request for a subpage/path, i.e. path1/path2 or a simple get
        /*
         *
         $pp = explode("/", $page);
        if (count($pp)==1) {
            $p = $this->queryCF("?title=" . rawurlencode($pp[0]) . "&spaceKey=$this->namespace&expand=body.storage");
        } else {
            $p = $this->queryCF("?title=" . rawurlencode($pp[1]) . "&spaceKey=$this->namespace&expand=body.storage");
        }
        */
        if (isset($p->results[0]))
            return $p->results[0]->body->storage->value;
        else
            return false;
    }
    private function getpageWP ($page) {
        $p = $this->queryWP("/wp/v2/pages/?slug=" . $this->sluggy($page),
            null, 0
        );
        if (isset($p[0]->content->rendered))
            return $p[0]->content->rendered;
        else
            return false;
    }
    private function getpageDEBUG ($page) {
        return 'getpageDEBUG: Content of page $page';
    }


    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: editpage ----
     * -------------------------------------
     */
    /**
     * Edits a page.
     * @param $page Page name to edit.
     * @param $data Data to post to page.
     * @param $summary Edit summary to use.
     * @return api result
     **/
    function editpage ($page, $data, $summary = '') {
        if ($this->framework=='mediawiki') {
            // edit mediawiki page
            return $this->editpageMW($page, $data, $summary);
        } else if ($this->framework=='confluence') {
            return $this->editpageCF($page, $data, $summary);
        } else if ($this->framework=='wordpress') {
            return $this->editpageWP($page, $data, $summary);
        } else {
            return $this->editpageDEBUG($page, $data, $summary);
        }
    }
    private function editpageMW ($page, $data, $summary = '') {
        $minor = false;   // Whether or not to mark edit as minor.  (Default false)
        $bot = true;      // Whether or not to mark edit as a bot edit.
        $section = null;
        $detectEC = false;
        $maxlag = '';

        if ($this->token == null) {
            $this->token = $this->getedittokenMW();
        }

        $uploadfilesha1 = sha1($page);

        $params = array(
            'title' => $page,
            'text' => $data,
            'token' => $this->token,
            'summary' => $summary . " (" . $uploadfilesha1 . ")",
            ($minor ? 'minor' : 'notminor') => '1',
            ($bot ? 'bot' : 'notbot') => '1'
        );
        if ($section != null) {
            $params['section'] = $section;
        }
        if ($this->ecTimestamp != null && $detectEC == true) {
            $params['basetimestamp'] = $this->ecTimestamp;
            $this->ecTimestamp = null;
        }
        if ($maxlag != '') {
            $maxlag = '&maxlag=' . $maxlag;
        }

        // $result = $this->http->post($this->url, $params);
        $result = $this->queryMW('?action=edit&format=php', $params, $this->repeatOnHTTPError);
        // var_dump($result);
        if ($result['edit']) {
            if ($result['edit']['result'] == 'Success') {
                return true;
            } else {
                echo "Error API: " . $result['edit']['result'] . "\n";
                return false;
            }
        } else {
            //var_dump($result);
            // sleep(3);    // sleep a while after this error
            if (isset($result['error'])) {
                if ($result['error']['code'] == 'badtoken') {
                    $this->token = $this->getedittokenMW();
                }
            } else {
                echo "Error API: result is not properly constructed.\n";
                var_dump($result);
            }
            return false;
        }
    }
    private function editpageCF ($page, $data, $summary = '') {
        // presets
        $aid = 0;      // ancestors's id, assume none is given
        // find out whether it is a request for a subpage/path, i.e. path1/path2 or a simple get
        // if it is a simple path, create or update the page
        // if it is a path path1/path2, create a path1 ancestor page if not existent, and create/update sub page path2
        $pp = explode("/", $page);
        // look if the page already exists
        $p = $this->queryCF("?title=" . rawurlencode($page) . "&spaceKey=$this->namespace&expand=version",
            null, $this->repeatOnHTTPError
        );
        // store page's id and version
        $pid = 0;
        $pversion = 1;
        if (is_array($p->results)) {
            if (count($p->results) > 0) {
                $pid = $p->results[0]->id;
                $pversion = $p->results[0]->version->number;
            }
        }

        if ($this->debuglevel > 3) echo "PID/VERSION: " .  $pid . "/" . $pversion . "\n";
        if ($pid > 0) {
            if ($this->debuglevel > 2) echo "DEBUG: " .  "update existing: " . $page . "\n";
            // update existing page, it has already the right ancestor stored
            // CURL action
            // curl -u admin:admin -X PUT -H 'Content-Type: application/json' -d '{"id":"3604482","type":"page",
            // "title":"new page","space":{"key":"TST"},"body":{"storage":{"value":
            // "<p>This is the updated text for the new page</p>","representation":"storage"}},
            // "version":{"number":2}}' http://localhost:8080/confluence/rest/api/content/3604482

            $uploadfilesha1 = sha1($page);

            $params = array(
                "id" => $pid,
                "type" => "page",
                "title" => $page,
                "space"=>[
                    "key" => $this->namespace
                ],
                "body" =>[
                    "storage"=>[
                        "value" => $data,
                        "representation" => "storage"
                    ]
                ],
                "version"=>[
                    "message" => $summary . " (" . $uploadfilesha1 . ")",
                    "number" => $pversion + 1
                ]
            );
            $result = $this->http->put($this->url . "/" . $pid, json_encode($params));
        } else {
            // create new page
            // CURL action
            // curl -u admin:admin -X POST -H 'Content-Type: application/json' -d '{"type":"page","title":"new page",
            // "space":{"key":"TST"},"body":{"storage":{"value":"<p>This is <br/> a new page</p>","representation":
            // "storage"}}}' http://localhost:8080/confluence/rest/api/content/

            // try also to get this page ancestor's id if any
            // $anc = $this->queryCF("?title=" . rawurlencode($page) . "&spaceKey=$this->namespace&expand=ancestors");
            // $aid = $anc->results[0]->ancestors[0]->id;  // ancestor's id

            if ((count($pp)) > 1) {
                // new sub page, check if it has no ancestor yet, if none create a blank ancestor page first
                $ancestorpage = $pp[0];
                $anc = $this->queryCF("?title=" . rawurlencode($ancestorpage) . "&spaceKey=$this->namespace&expand=version",
                    null, $this->repeatOnHTTPError
                );
                $aid = 0;
                if ($anc) {
                    if ($anc->results) {
                        $aid = $anc->results[0]->id;
                    }
                };  // ancestor's id or 0 on no ancestor
                if ($aid == 0) {
                    // no ancestor yet, create a blank ancestor page now first
                    if ($this->debuglevel > 2) echo "DEBUG: " .  "new ancestor: " . $ancestorpage . "\n";
                    $params = array(
                        "type" => "page",
                        "title" => $ancestorpage,
                        "space"=>[
                            "key" => $this->namespace
                        ],
                        "body" =>[
                            "storage"=>[
                                "value" => "<p>(Empty ancestor page)</p>",
                                "representation" => "storage"
                            ]
                        ],
                        "version"=>[
                            "message" => "default ancestor page",
                            "number" => 1
                        ]
                    );
                    $ancpage = $this->http->post($this->url, json_encode($params));
                    $ancpage = json_decode($ancpage);
                    if ($this->debuglevel > 3)  var_dump($ancpage);
                    $aid = $ancpage->id;
                    if ($this->debuglevel > 2) echo "NEW AID: " .  $aid . "\n";
                }

                // and get that id
                // $anc = $this->queryCF("?title=" . rawurlencode($ancestorpage) . "&spaceKey=$this->namespace&expand=ancestors");

            } else {
                if ($this->debuglevel > 2) echo "DEBUG: " .  "using existing ancestor\n";
            }
            
            // prepare new sub page
            if ($aid > 0) {
                if ($this->debuglevel > 2) echo "DEBUG: " .  "new sub page: " . $page . "\n";
                // ... and prepare the new ancestor ref param part
                $params = array(
                    "type" => "page",
                    "title" => $page,
                    "space"=>[
                        "key" => $this->namespace
                    ],
                    "ancestors" => [[
                        "id" => $aid
                    ]],
                    "body" =>[
                        "storage"=>[
                            "value" => $data,
                            "representation" => "storage"
                        ]
                    ],
                    "version"=>[
                        "message" => $summary,
                        "number" => 1
                    ]
                );
            } else {
                if ($this->debuglevel > 3) echo "DEBUG: " .  "new page" . "\n";
                // ... and prepare the page param part
                $params = array(
                    "type" => "page",
                    "title" => $page,
                    "space"=>[
                        "key" => $this->namespace
                    ],
                    "body" =>[
                        "storage"=>[
                            "value" => $data,
                            "representation" => "storage"
                        ]
                    ],
                    "version"=>[
                        "message" => $summary,
                        "number" => 1
                    ]
                );
            }
            $result = $this->http->post($this->url, json_encode($params));
        }

        if ($this->debuglevel > 4) var_dump($result);

        if ($result) {
            return true;
        } else {
            // var_dump($result);
            echo "Error API: editpageCF\n";
            // echo $this->url . "\n"; exit;
            return false;
        }
    }
    private function editpageWP ($page, $data, $summary = '') {
        if ($this->debuglevel > 3) echo "*** PROCESSING PAGE: " .  $page . " with chars=" . strlen($data) . "\n";
        // presets
        $aid = 0;      // parent's id, assume none is given
        // find out whether it is a request for a subpage/path, i.e. path1/path2 or a simple get
        // if it is a simple path, create or update the page
        // if it is a path path1/path2, create a path1 ancestor page if not existent, and create/update sub page path2
        $pp = explode("/", $page);
        // look if the page already exists
        $p = $this->queryCF("/wp/v2/pages/?slug=" . $this->sluggy($page), null, 0);
        // store page's id
        $pid = 0;
        if (isset($p[0]->id)) {
            $pid = $p[0]->id;
        }

        if ($this->debuglevel > 3) echo "PID: " .  $pid . " CHECKED:" . $this->sluggy($page) . "\n";

        if ($pid > 0) {
            if ($this->debuglevel > 2) echo "DEBUG: " .  "update existing: " . $page . "\n";
            // update existing page, it has already the right ancestor stored

            $uploadfilesha1 = sha1($page);

            $params = array(
                "title" =>  $page,
                "status" => "publish",
                "content" => $data,
                "slug" => $this->sluggy($page) // part of the URL usually
            );
            $result = $this->http->put($this->url . "/wp/v2/pages/" . $pid, json_encode($params));
            //var_dump(json_decode($result));exit;
        } else {
            // create new page

            // try also to get this page ancestor's id if any
            // $anc = $this->queryCF("?title=" . rawurlencode($page) . "&spaceKey=$this->namespace&expand=ancestors");
            // $aid = $anc->results[0]->ancestors[0]->id;  // ancestor's id

            if ((count($pp)) > 1) {
                // new sub page, check if it has no ancestor yet, if none create a blank ancestor page first
                $ancestorpage = $pp[0];
                $anc = $this->queryCF("/wp/v2/pages/?slug=" . $this->sluggy($ancestorpage),
                    null, 0 /* $this->repeatOnHTTPError */
                );
                // echo $this->sluggy($ancestorpage) . "\n";var_dump($anc);exit;
                $aid = $anc[0]->id;  // ancestor's id
                if ($aid == 0) {
                    // no ancestor yet, create a blank ancestor page now first
                    if ($this->debuglevel > 2) echo "DEBUG: new ancestor: " . $ancestorpage . "\n";
                    $params = array(
                        "title" =>  $ancestorpage,
                        "status" => "publish",
                        "content" => "<p>(Empty ancestor page)</p>",
                        // 'categories' => 5, // category ID
                        // 'tags' => '1,4,23' // string, comma separated
                        // 'date' => '2015-05-05T10:00:00', // YYYY-MM-DDTHH:MM:SS
                        // 'excerpt' => 'Read this awesome post',
                        // 'password' => '12$45',
                        "slug" => $this->sluggy($ancestorpage) // part of the URL usually
                        // more body params are here:
                        // developer.wordpress.org/rest-api/reference/posts/#create-a-post
                    );
                    $ancpage = $this->http->post($this->url . "/wp/v2/pages", json_encode($params));
                    $ancpage = json_decode($ancpage);
                    // var_dump($ancpage->slug);
                    // if ($this->debuglevel > 3) var_dump($ancpage);
                    $aid = $ancpage->id;
                    if ($this->debuglevel > 2) echo "NEW AID: " .  $aid . "\n";
                }

                // and get that id
                // $anc = $this->queryCF("?title=" . rawurlencode($ancestorpage) . "&spaceKey=$this->namespace&expand=ancestors");

            } else {
                if ($this->debuglevel > 2) echo "DEBUG: " .  "using existing ancestor\n";
            }
            
            // prepare new sub page
            if ($aid > 0) {
                $subpage = $pp[1];
                if ($this->debuglevel > 2) echo "DEBUG: " .  "new sub page: " .  $subpage . "\n";
                // ... and prepare the new ancestor ref param part
                $params = array(
                    "title" =>  $page,
                    "status" => "publish",
                    "content" => $data,
                    // 'categories' => 5, // category ID
                    // 'tags' => '1,4,23' // string, comma separated
                    // 'date' => '2015-05-05T10:00:00', // YYYY-MM-DDTHH:MM:SS
                    // 'excerpt' => 'Read this awesome post',
                    // 'password' => '12$45',
                    "slug" => $this->sluggy($page)    // part of the URL usually
                    //"parent" => $aid
                    // more body params are here:
                    // developer.wordpress.org/rest-api/reference/posts/#create-a-post
                );
            } else {
                if ($this->debuglevel > 3) echo "DEBUG: " .  "new page" . "\n";
                // ... and prepare the page param part
                $params = array(
                    "title" => $page,
                    "status" => "publish",
                    "content" => $data,
                    // 'categories' => 5, // category ID
                    // 'tags' => '1,4,23' // string, comma separated
                    // 'date' => '2015-05-05T10:00:00', // YYYY-MM-DDTHH:MM:SS
                    // 'excerpt' => 'Read this awesome post',
                    // 'password' => '12$45',
                    "slug" => $this->sluggy($page) // part of the URL usually
                    // more body params are here:
                    // developer.wordpress.org/rest-api/reference/posts/#create-a-post
                );
            }

            $result = $this->http->post($this->url . "/wp/v2/pages", json_encode($params));
            var_dump(json_decode($result)->slug);
        }
        //var_dump(json_decode($result));exit;
        if ($result) {
            return true;
        } else {
            // var_dump($result);
            echo "Error API: editpageWP\n";
            return false;
        }
    }
    private function editpageDEBUG () {
        return 'editpageDEBUG';
    }



    /*
     * --------------------------------------
     * ---- MAIN PUBLIC API CALL: upload ----
     * --------------------------------------
     */
    /*
     * upload
     * Upload an image. Returns false on error.
     * @param $page The destination file name.
     * @param $file The local file path.
     * @param $desc The upload discrption (defaults to '').
     * returns
     *   success ::  on success
     *   duplicate :: if the same image with the same filename and sha1 already exist
     *   failed :: if upload failed
     *   nofile :: if file to upload is not a file
     *   unsupportedmimetype :: mime type unsupported, as of now only svg is processed
     */
    function upload ($page, $file, $desc='') {
        if ($this->framework=='mediawiki') {
            return $this->uploadMW($page, $file, $desc);
        } else if ($this->framework=='confluence') {
            return $this->uploadCF($page, $file, $desc);
        } else if ($this->framework=='wordpress') {
            return $this->uploadWP($page, $file, $desc);
        } else {
            return $this->uploadDEBUG($page, $file, $desc);
        }
    }
    private function uploadMW ($page, $file, $desc) {
        if ($this->token == null) {
            $this->token = $this->getedittokenMW();
        }

        // determine extension of the image
        $extension = $this->after_last('.', $page);
        // determien mime type
        $mimetype ='';
        switch ($extension) {
            case 'svg':
                $mimetype = 'image/svg+xml';
                break;
            default:
                return 'unsupportedmimetype'; // give up
                break;
        }

        // file exist?
        if (!is_file($file)) return 'nofile';

        if (1==0) {
            $params = array(
                'filename'        => $page,
                'comment'         => $desc,
                'token'           => $this->token,
                'ignorewarnings'  => '1',
                'file'            => '@' . $file
            );
            //$xxx = $this->queryMW('?action=upload&format=php', $params, 0, $extraheaders);
            //var_dump($xxx);
            //return $xxx;
            // get sha1 of this file and check wether an image alreads exist in the mediawiki repo
            $sha1orig = sha1_file($file);
            echo $sha1orig . "\n";
            $sha1orig = sha1_file($file);
            echo $sha1orig . "\n";
            $md5orig = md5_file($file);
            echo $md5orig . "\n";
            $md5orig = md5_file($file);
            echo $md5orig . "\n";
            $params = array(
                'aiprop'  => 'sha1',
                'aifrom'  => $page,
                'aito'    => $page
            );
            $repo = $this->queryMW('?action=query&list=allimages&format=php', $params);
            //var_dump($repo);
            $params = array(
                'titles'  => 'File:' . $page
            );
            $repo = $this->queryMW('?action=query&prop=duplicatefiles&format=php', $params);
            //var_dump($repo);
            $dd = file_get_contents('http://wiki.hl7.de/images/TR-2.16.840.1.113883.3.1937.99.60.5.4.101-2014-07-08T000000.svg');
            $sha1orig1 = sha1($dd);
            $sha1orig2 = sha1_file($file);
            echo $sha1orig1 . "-" . $sha1orig2 . "\n";
            file_put_contents('__x1.txt', $dd);
            file_put_contents('__x2.txt', file_get_contents($file));

            $suchmuster = '/id="_[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}"/i';
            file_put_contents('__x1.txt', preg_replace($suchmuster, '', $dd));
            file_put_contents('__x2.txt', preg_replace($suchmuster, '', file_get_contents($file)));
            exit;
        }

        // get info about a maybe already existing file with the same name in repository
        $params = array(
            'aifrom'  => $page,
            'aito'    => $page,
            'aiprop'  => 'comment|sha1'  // get the comment and the sha1 to check for duplicate
        );
        // get info about this image
        $repo = $this->queryMW('?action=query&list=allimages&format=php', $params, $this->repeatOnHTTPError);
        //var_dump($repo);exit;
        // get the sha1 fot this image by analyzing the comment that looks like
        //   <summary text / desc> (sha1)
        // where sha1 is the sha1 of the canonicalized image (ie no id="_uuids" in it any more)
        $repofilesha1 = '';
        if (is_array($repo)) {
            $fcomment = $repo['query']['allimages'][0]['comment'];
            $repofilesha1 = $this->between_last('(', ')', $fcomment);
        }
        // get the canonicalized sha1 for the file to be uploaded
        $uploadfilesha1 = sha1($this->canonicalize(file_get_contents($file)));

        //file_put_contents('__x1.txt', $this->canonicalize(file_get_contents($file)) );
        //file_put_contents('__x2.txt', file_get_contents($file));

        if (strlen($repofilesha1)>0 and ($repofilesha1 === $uploadfilesha1))
            return 'duplicate';  // you're done if this is a duplicate contents wise

        // otherise (on no duplications): prepare the upload
        $cfile = new CURLFile($file, $mimetype, $page);
        $imgdata = array(
            'file' => $cfile,
            'filename' => $page,
            'comment' => $desc . " (" . $uploadfilesha1 . ")",  // add sha1 for canonicalized content
            'ignorewarnings'  => '1',
            'token' => $this->token
        );
        $extraheaders = array (
            'Content-Type: multipart/form-data',
            'X-Atlassian-Token: nocheck'
        );
        $res = $this->queryMW('?action=upload&format=php', $imgdata, $this->repeatOnHTTPError, $extraheaders);
        // var_dump($res);
        if (is_array($res)) {
            if ($res['upload']['result'] == "Success") return 'success';
        }
        $errmsg = "+++ Upload image failure, error " . $res['error']['code'] . " - " . $res['error']['info'] ;
        // throw new Exception("Upload image failure, error " . $res['error']['code'] . " - " . $res['error']['info']);
        echo "$errmsg\n";

        return 'failed';
    }
    private function uploadCF ($page, $file, $desc) {
    }
    private function uploadWP ($page, $file, $desc) {
        // determine name of the image without extension
        $title = $this->before_last('.', $page);
        // determine extension of the image
        $extension = $this->after_last('.', $file);
        // determine mime type
        $mimetype = '';
        switch ($extension) {
            case 'svg':
                $mimetype = 'image/svg+xml';
                break;
            default:
                return 'unsupportedmimetype'; // give up
                break;
        }

        // file exist?
        if (!is_file($file)) return 'nofile';

        // first check whether this image with this name and this sha1 (stored in alt_text) already exists
        $alim = $this->queryWP("/wp/v2/media?search=" . $title);
        $alimid = (isset($alim[0]->id)) ? $alim[0]->id : 0;
        $alimalttext = (isset($alim[0]->alt_text)) ? $alim[0]->alt_text : '';
        // var_dump($alim);
        $alisha1 = $this->between_last('(', ')', $alimalttext);

        $fcnt = file_get_contents($file);
        $uploadfilesha1 = sha1($this->canonicalize($fcnt));

        // echo "$alimid $alimalttext $alisha1 $uploadfilesha1\n";
        // exit;

        // is the file already uploaded?
        if ($alisha1 == $uploadfilesha1) {
            return 'duplicate';
        } else {
            // prepare the extra header for the binary upload
            $cfile = new CURLFile($file, $mimetype, $page);
            $imgdata = array(
                'name' => $cfile
            );
            $extraheaders = array (
                'Content-Type: multipart/form-data',
                "Cache-control: no-cache",
                "Content-Length: " . strlen($fcnt)
            );
            
            // upload it as update (PUT) on an existing image with different content or create (POST) otherwise
            if ($alimid > 0) {
                echo "PUT /wp/v2/media/" . $alimid . "\n";
                $result = $this->http->put($this->url . "/wp/v2/media/" . $alimid, $fcnt, $extraheaders);
            } else {
                echo "POST /wp/v2/media" . "\n";
                $result = $this->http->post($this->url . "/wp/v2/media", $imgdata, $extraheaders);
            }
            $jresult = json_decode($result);
            // var_dump($jresult); exit;
            $iid = (isset($jresult->id)) ? $jresult->id : 0 ;    // if succeeded image id is given
            if ($iid > 0) {
                // add alt text
                $params = array(
                    "alt_text" => $desc . " (" . $uploadfilesha1 . ")",  // add sha1 for canonicalized content
                );
                // var_dump($params);
                $extraheaders = array (
                    'Content-Type: application/json'
                );    
                $result = $this->http->post($this->url . "/wp/v2/media/" . $iid, json_encode($params));
                $jresult = json_decode($result);
                var_dump($jresult);
                return 'success';
            }
        }
        return 'failed';
    }
    private function uploadDEBUG ($page, $file, $desc) {
    }


    /*
     * --------------------------------------
     * ---- MAIN PUBLIC API CALL: checkEnv ----
     * --------------------------------------
     */
    /*
     * checkEnv
     * Checks whether the distant environment is set up correctly
     * For mediawiki
     * For confluence
     * - check whether the needed namespace exist. 
     * Returns false on error.
     */
    function checkEnv () {
        if ($this->framework=='mediawiki') {
            return $this->checkEnvMW();
        } else if ($this->framework=='confluence') {
            return $this->checkEnvCF();
        } else if ($this->framework=='wordpress') {
            return $this->checkEnvWP();
        } else {
            return $this->uploadDEBUG($page, $file, $desc);
        }
    }
    private function checkEnvMW () {
        return true;
    }
    private function checkEnvCF () {
        $url = $this->before_last('content', $this->url);
        $query = "space?spaceKey=" . self::CONFLUENCE_ART_DECOR_NAMESPACE_KEY;
        $p = $this->http->get($url . $query);
        $pp = json_decode($p);
        // var_dump($pp);
        // var_dump($pp->results);
        // echo count($pp->results);exit;
        if (count($pp->results) > 0) return true;
        return false;
    }
    private function checkEnvWP () {
        return true;
    }
    private function checkEnvDEBUG () {
        return true;
    }

    function getPageLatestSHA1FromCommentMW ($fn) {
        $params = array(
            'prop' => 'revisions',
            'rvlimit' => 1,
            'titles' => $fn,
            'rvprop' => 'comment'
        );
        $res = $this->queryMW('?action=query&format=php', $params, $this->repeatOnHTTPError);
        //var_dump($res);
        //var_dump($res['query']['pages']);
        if (is_array($res['query']['pages'])) {
            $pageid = array_keys($res['query']['pages'])[0];
            $comment = $res['query']['pages'][$pageid]['revisions'][0]['comment'];
            // echo $comment . "\n";
            if (strlen($comment)>0)
                return $this->between_last('(', ')', $comment);
        }
        return '';
    }

    /*
     * -------------------------------------
     * ---- MAIN PUBLIC API CALL: log ----
     * (along with getting the logfile name
     * -------------------------------------
     */
    function getLogfileName ($botuser) {
        $logfilesuffix = "-" . date("Ym");
        if ($this->framework=='mediawiki') {
            // a subpage of the bot user page
            return 'Benutzer:' . $botuser . '/log' . $logfilesuffix;
        } else if ($this->framework=='confluence') {
            // the log page in the art decor space
            return 'Log-ADbot' . $logfilesuffix;
        } else {
            return 'Log-ADbot' . $logfilesuffix;
        }
    }
    function log ($botuser, $data) {
        // Build teh summary text
        $summary = "update log entry for user " . $botuser;
        // get the log file name
        $logpage = $this->getLogfileName ($botuser);
        if ($this->framework=='mediawiki') {
            $actualpage = $this->getpageMW($logpage);
            $data = $data . $actualpage;
            return $this->editpageMW($logpage, $data, $summary);
        } else if ($this->framework=='confluence') {
            $actualpage = $this->getpageCF($logpage);
            $data = $data . $actualpage;
            return $this->editpageCF($logpage, $data, $summary);
        } else {
            $actualpage = $this->getpageWP($logpage);
            $data = $data . $actualpage;
            return $this->editpageWP($logpage, $data, $summary);
        }
    }

    function canonicalize($s) {
        $t = preg_replace(self::EXTRACT_ID_4_CANONICALIZE1, '', $s);
        return preg_replace(self::EXTRACT_ID_4_CANONICALIZE2, '', $t);
    }

    function sluggy($s) {
        $remove = array('.', '%2F');
        return str_replace($remove, "-", rawurlencode(strtolower($s)));
    }

    function tinymd5 ($str, $length) { // convert md5 to ~base(64 - count($remove)) and truncate to $length
        // remove vowels to prevent undesirable words and similarly + / which may be problematic
        $remove = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', '+', '/');
        return str_pad(substr(str_replace($remove, '', base64_encode(md5($str, TRUE))), 0, $length), $length, '=');
    }

    private function after ($s1, $inthat)
    {
        if (!is_bool(strpos($inthat, $s1)))
            return substr($inthat, strpos($inthat,$s1)+strlen($s1));
    }

    private function after_last ($s1, $inthat)
    {
        if (!is_bool($this->strrevpos($inthat, $s1)))
            return substr($inthat, $this->strrevpos($inthat, $s1)+strlen($s1));
    }

    private function before ($s1, $inthat)
    {
        if ($inthat == null) return null;
        return substr($inthat, 0, strpos($inthat, $s1));
    }

    private function before_last ($s1, $inthat)
    {
        return substr($inthat, 0, $this->strrevpos($inthat, $s1));
    }

    private function between ($s1, $that, $inthat)
    {
        return before ($that, after($s1, $inthat));
    }

    private function between_last($s1, $s2, $inthat)
    {
        return $this->after_last($s1, $this->before_last($s2, $inthat));
    }


    private function strrevpos($instr, $needle)
    {
        $rev_pos = strpos (strrev($instr), strrev($needle));
        if ($rev_pos===false) return false;
        else return strlen($instr) - $rev_pos - strlen($needle);
    }

}


/**
 * This class is designed to provide a simplified interface to cURL which maintains cookies.
 * @author Cobi
 **/
class http {
    private $ch;
    private $uid;
    private $header;
    public $cookie_jar;
    public $postfollowredirs;
    public $getfollowredirs;
    public $quiet=true;
    public $useragent="art-decor-bot-broker";

    public function http_code () {
        /*
         * [Informational 1xx]
            100="Continue"
            101="Switching Protocols"

            [Successful 2xx]
            200="OK"
            201="Created"
            202="Accepted"
            203="Non-Authoritative Information"
            204="No Content"
            205="Reset Content"
            206="Partial Content"

            [Redirection 3xx]
            300="Multiple Choices"
            301="Moved Permanently"
            302="Found"
            303="See Other"
            304="Not Modified"
            305="Use Proxy"
            306="(Unused)"
            307="Temporary Redirect"

            [Client Error 4xx]
            400="Bad Request"
            401="Unauthorized"
            402="Payment Required"
            403="Forbidden"
            404="Not Found"
            405="Method Not Allowed"
            406="Not Acceptable"
            407="Proxy Authentication Required"
            408="Request Timeout"
            409="Conflict"
            410="Gone"
            411="Length Required"
            412="Precondition Failed"
            413="Request Entity Too Large"
            414="Request-URI Too Long"
            415="Unsupported Media Type"
            416="Requested Range Not Satisfiable"
            417="Expectation Failed"

            [Server Error 5xx]
            500="Internal Server Error"
            501="Not Implemented"
            502="Bad Gateway"
            503="Service Unavailable"
            504="Gateway Timeout"
            505="HTTP Version Not Supported"
         */
        return curl_getinfo( $this->ch, CURLINFO_HTTP_CODE );
    }

    function data_encode ($data, $keyprefix = "", $keypostfix = "") {
        assert( is_array($data) );
        $vars=null;
        foreach($data as $key=>$value) {
            if(is_array($value))
                $vars .= $this->data_encode($value, $keyprefix.$key.$keypostfix.rawurlencode("["), urlencode("]"));
            else
                $vars .= $keyprefix.$key.$keypostfix."=".rawurlencode($value)."&";
        }
        return $vars;
    }

    function __construct () {
        $this->ch = curl_init();
        $this->uid = dechex(rand(0,99999999));
        curl_setopt($this->ch,CURLOPT_COOKIEJAR,'/tmp/cluewikibot.cookies.'.$this->uid.'.dat');
        curl_setopt($this->ch,CURLOPT_COOKIEFILE,'/tmp/cluewikibot.cookies.'.$this->uid.'.dat');
        curl_setopt($this->ch,CURLOPT_MAXCONNECTS,100);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($this->ch,CURLOPT_TIMEOUT,10);
        curl_setopt($this->ch,CURLOPT_CONNECTTIMEOUT,5);
        // CURLOPT_CLOSEPOLICY removed, never implemented curl_setopt($this->ch,CURLOPT_CLOSEPOLICY,CURLCLOSEPOLICY_LEAST_RECENTLY_USED);
        $this->postfollowredirs = 0;
        $this->getfollowredirs = 1;
        $this->cookie_jar = array();
    }

    function set_header ($header) {
        // sets http header to use on curl calls for all subsequent calls
        $this->header = $header;
    }

    function post ($url, $data, $extraheaders=array()) {
        //
        // echo 'POST: '.$url."\n";
        // var_dump($data);
        // var_dump(array_merge($extraheaders, $this->header));
        //
        $time = microtime(1);
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->useragent);
        /* Crappy hack to add extra cookies, should be cleaned up */
        $cookies = null;
        // var_dump($this->header);
        foreach ($this->cookie_jar as $name => $value) {
            if (empty($cookies))
                $cookies = "$name=$value";
            else
                $cookies .= "; $name=$value";
        }
        if ($cookies != null) {
            curl_setopt($this->ch, CURLOPT_COOKIE, $cookies);
        }
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $this->postfollowredirs);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array_merge($extraheaders, $this->header ? $this->header : array()));
        // var_dump(array_merge($extraheaders, $this->header));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($this->ch, CURLOPT_HTTPGET, 0);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, 0);
        // curl_setopt($this->ch, CURLOPT_VERBOSE, 1);
        //	curl_setopt($this->ch,CURLOPT_POSTFIELDS, substr($this->data_encode($data), 0, -1) );
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        // var_dump($data);
        $ret = curl_exec($this->ch);
        // var_dump($ret);
        $ra = json_decode($ret);
        //$hhtpr = curl_getinfo($this->ch); var_dump($hhtpr);
        //echo "+++Error POST: http=" . $hhtpr["http_code"] . " curl=" . curl_error($this->ch) . " #curl=" . curl_errno($this->ch) . " oo " . "\n";
        if (!$this->quiet)
            echo 'POST: ' . $url . ' (' . (microtime(1) - $time) . ' s) (' . strlen($ret) . " b)\n";
        return $ret;
    }

    function put ($url, $data, $extraheaders=array()) {
        $time = microtime(1);
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->useragent);
        /* Crappy hack to add extra cookies, should be cleaned up */
        $cookies = null;
        foreach ($this->cookie_jar as $name => $value) {
            if (empty($cookies))
                $cookies = "$name=$value";
            else
                $cookies .= "; $name=$value";
        }
        if ($cookies != null) {
            curl_setopt($this->ch, CURLOPT_COOKIE, $cookies);
        }
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $this->postfollowredirs);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array_merge($this->header, $extraheaders, array('Expect:')));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($this->ch, CURLOPT_HTTPGET, 0);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, 0);
        // curl_setopt($this->ch, CURLOPT_VERBOSE, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        $ret = curl_exec($this->ch);
        // var_dump($ret);
        $ra = json_decode($ret);
        // echo "Error: " . curl_error($this->ch) . "-#" . curl_errno($this->ch) . " oo " . $ra->statusCode . "::" . $ra->message . "\n"   ;
        // echo "Error: " . curl_error($this->ch) . "-#" . curl_errno($this->ch) . "\n" ;
        if (!$this->quiet)
            echo 'PUT: ' . $url . ' (' . (microtime(1) - $time) . ' s) (' . strlen($ret) . " b)\n";
        return $ret;
    }

    function get ($url) {
        //echo 'GET: '.$url."\n";
        $time = microtime(1);
        curl_setopt($this->ch, CURLOPT_URL,$url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->useragent);
        /* Crappy hack to add extra cookies, should be cleaned up */
        $cookies = null;
        foreach ($this->cookie_jar as $name => $value) {
            if (empty($cookies))
                $cookies = "$name=$value";
            else
                $cookies .= "; $name=$value";
        }
        if ($cookies != null)
            curl_setopt($this->ch,CURLOPT_COOKIE,$cookies);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION,$this->getfollowredirs);
        if (isset($this->header)) curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($this->ch, CURLOPT_HTTPGET,1);
        //curl_setopt($this->ch,CURLOPT_FAILONERROR,1);
        $data = curl_exec($this->ch);
        // echo "Error: ".curl_error($this->ch);
        // var_dump($data);
        //global $logfd;
        //if (!is_resource($logfd)) {
        //    $logfd = fopen('php://stderr','w');
        // $hhtpr = curl_getinfo($this->ch);
        //echo "+++Error GET: http=" . $hhtpr["http_code"] . " curl=" . curl_error($this->ch) . " #curl=" . curl_errno($this->ch) . " oo " . "\n";
        if (!$this->quiet)
            echo 'GET: ' . $url . ' (' . (microtime(1) - $time) . ' s) (' . strlen($data) . " b)\n";
        //}
        return $data;
    }

    function setHTTPcreds ($uname, $pwd) {
        curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->ch, CURLOPT_USERPWD, $uname . ":" . $pwd);
    }

    function __destruct () {
        curl_close($this->ch);
        @unlink('/tmp/cluewikibot.cookies.' . $this->uid . '.dat');
    }
}

?>
