<?php
/*
    ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses
    
    This file is part of the ART-DECOR® tools suite.
    
    Author: Kai U. Heitmann
*/
/*
 *
 *  - - - - - - - - - - -
 *  Main script for the ART-DECOR® Automatic Wiki Bot (ADAWIB) - server site part - manager
 *  Supported platforms so far: mediawiki, confluence, wordpress
 * 
 *  Usage
 *    scriptname without any parameters will process all projects/governance groups
 *      in the config file config.php
 *    scriptname with a project or governance group identifier will process that item only
 * 
 *    The folllowing options work only without a project or governance group identifier
 * 
 *    -transformnoload {tdir}
 *     no load again, but transform and upload, {tdir} shall point to existing tmpdir to use
 * 
 *    -uploadnotransform {tdir} or -forceduploadnotransform {tdir}
 *     no load again, no transform, but upload, {tdir} shall point to existing tmpdir to use
 * 
 *    -summary, -summarytable
 *     returns an overall statistics summary, -summarytable also shows the table
 * 
 *    -check
 *     checks only
 */

/* ----Overide default execution vars---- */
ini_set('memory_limit', '4096M');
set_time_limit (21600); /* = 6h */
date_default_timezone_set('Europe/Berlin');

/* ----PRESETS---- */
/*  name and version of this script */
$scriptname = "ADAWIB";
$botversion = "13.5";

/* -----OVERALL CONSTANTS AND GLOBAL VARS----- */
const AUTOMATIC_ADAWIB_PAGE = "Automatic ADBot page";
const AUTOMATIC_ADAWIB_IMAGE = "Automatic ADBot image";

const LOCAL_INDEX_FILENAME = "index.xml";
const LOCAL_LEFTOVER_FILENAME = "leftovers.xml";

const LOCAL_SUCCESS_FILENAME = "success.xml";
const LOCAL_PROGRESS_FILENAME = "progress.xml";
const LOCAL_OVERLOG_FILENAME = "overalllog.xml";
const LOCAL_OVERSTAT_FILENAME = "overallstat.json";

const TIX_DIRECTORY = "tix";
const TIX_INDEX_SUCCESS = "success";
const TIX_INDEX_PROGRESS = "progress";
const TIX_INDEX_RAWINPUT = "rawinput";
const TIX_INDEX_TARGET = "target";
const TIX_INDEX_ERROR = "error";
const TIX_INDEX_START = "laststart";
const TIX_INDEX_ID = "id";
const TIX_INDEX_TYPE = "type";
const TIX_INDEX_NAME = "name";
const TIX_INDEX_SHORT = "short";

// global parameters, feed by command line options, defaults set here
$_DO_LOAD_PROJECT_AGAIN = "1";
$_DO_LOAD_PROJECT_FORCED = FALSE;
$_DO_TRANSFORM_PROJECT_AGAIN = "1";
$_RECENT_TMPDIR_TO_USE = "";

$botname = "ADBot";

$tixfile = "";
$allTIX = array();
$leftoverfilename = "";
$successfilename = "";
$progressfilename = "";

// some more presets / constants
define("LEVEL1", 0);
define("LEVEL2", 1);
define("LEVEL3", 2);
define("LEVEL4", 3);

/* ----CONFIG START---- */

/*  include CONFIGS AND DEFAULTS */

/* config presets */
$url='';
$projectorgovernancegroups2serve='';
$xsl='';
$debuglevel='';
$targetFramework2use='';
// email adresses to send alerts, warnings and hints to
$info1email = "";
$info2email = "";
if (!is_dir(TIX_DIRECTORY)) {
    mkdir(TIX_DIRECTORY);
}

if (is_file("config.php")) {
    include("config.php");
} else {
    die ("+++ERROR not properly configured: config.php not found.\n");
}

/* check required settings */
if ($url=='') die ("+++ERROR not set or empty in config: url\n");
if ($projectorgovernancegroups2serve=='') die ("+++ERROR not set or empty in config: projectorgovernancegroups2serve\n");

// load the bot library
require('lib/art-decor-bot-broker-api.php');

/* ----GO THRU CONFIGS (file pattern {mnemonic}.config.php) AND PROCESS THEM ---- */

if (isset($argv[1])) {
    // if command line arg is given process that
    if ($argv[1] == '-summary' or $argv[1] == '-summarytable') {
        $ofwhom = "";
        if (isset($argv[2])) $ofwhom = $argv[2];
        $filemit = array();
        if ($argv[1] == '-summarytable') {
            /* say hello */
            echo "$scriptname [$botversion] for $botname\n";
            $hostname = trim(`hostname`);
            $nudate = trim(`date`);
            // emit the header already
            echo "[" . $scriptname . " v" . $botversion . " statistics as of " . $nudate . " at " . $hostname . "] \n";
            echo "Project (*active)  Elapsed    Open            % Last completed       Target / TIX\n";
            echo "------------------|----------|----------|------|--------------------|";
            echo "-------------------------------------------------------\n";
        }
        $tabemit = array();
        foreach (glob(TIX_DIRECTORY . "/*") as $filename) {
            $sss = array();
            $sof = file_get_contents($filename);
            $soj = json_decode($sof, true);
            // if ($filename == 'tix/xxx.mediawiki.DrMx3BD.ix.json') var_dump($soj);
            // if ($filename == 'tix/xxx.mediawiki.DrMx3BD.ix.json') var_dump($sss);
            $timeused = $soj['index']['success']["elapsed"];
            $open = $soj['index']['success']["errors"] - $soj['index']['success']["recovered"];
            $name = $soj['index']['success']["short"];
            $percent = $soj['index']['success']["percent"];
            $lastaccess = $soj['index']['success']["at"];
            $laststart = $soj['index']['laststart'];
            $sinces = time() - strtotime($laststart);  // last started since in s
            $sincem = $sinces / 60; // last started since in min
            $utarget = $soj['index']['target'];
            $ftarget = substr($utarget, 0, strpos($utarget, '@'));
            $pname = ($sinces > ($timeused * 1.1)) ? $name : $name . "*";
            if ($argv[1] == '-summarytable') {
                // lastaccess date as start for sorting it later maybe
                $tabemit[] = sprintf ("%s%-18.18s %9ds %10s %6d %s  %s\n %61dm ago  %s\n", 
                    $lastaccess, $pname, $timeused, $open, $percent, $lastaccess,
                    $ftarget, $sincem, $filename);
            }
            // prepare JSON statistics
            $sss[TIX_INDEX_ID] = $soj['index'][TIX_INDEX_ID];
            $sss[TIX_INDEX_TYPE] = $soj['index'][TIX_INDEX_TYPE];
            $sss[TIX_INDEX_NAME] = $soj['index'][TIX_INDEX_NAME];
            $sss[TIX_INDEX_SHORT] = $soj['index'][TIX_INDEX_SHORT];
            $sss[TIX_INDEX_START] = $soj['index'][TIX_INDEX_START];
            $sss[TIX_INDEX_TARGET] = $soj['index'][TIX_INDEX_TARGET];
            $sss[TIX_INDEX_ERROR] = $soj['index'][TIX_INDEX_ERROR];
            $sss['since'] = round($sincem) ;
            $sss[TIX_INDEX_SUCCESS] = $soj['index'][TIX_INDEX_SUCCESS];
            $sss[TIX_INDEX_PROGRESS] = $soj['index'][TIX_INDEX_PROGRESS];
            $filemit[] = $sss;
        }
        if ($argv[1] == '-summarytable') {
            arsort($tabemit);
            //var_dump($emit);
            foreach ($tabemit as $e) echo (substr($e, 19));
        } else {
            $s4f = json_encode($filemit);
            echo $s4f;
            exit;  // done here
            // file_put_contents(LOCAL_OVERSTAT_FILENAME, $s4f);
            // echo "*** Statistics written to " . LOCAL_OVERSTAT_FILENAME . "\n";
        }
    } else if ($argv[1] == '-transformnoload' or $argv[1] == '-uploadnotransform' or $argv[1] == '-forceduploadnotransform') {
        /* say hello */
        echo "$scriptname [$botversion] for $botname\n";
        // -transformnoload: no load again, no transform, but upload, argv[2] shall contain existing tmpdir to use
        // -uploadnotransform: no load again, but transform and upload, argv[2] shall contain existing tmpdir to use
        $_DO_LOAD_PROJECT_AGAIN = "0";
        $_DO_TRANSFORM_PROJECT_AGAIN = (($argv[1] == '-uploadnotransform' or $argv[1] == '-forceduploadnotransform') ? "0" : "1");
        $_DO_LOAD_PROJECT_FORCED = (($argv[1] == '-forceduploadnotransform') ? TRUE : FALSE);
        $_RECENT_TMPDIR_TO_USE = $argv[2];
        $forwhat = substr($_RECENT_TMPDIR_TO_USE, 19);
        if ($_DO_LOAD_PROJECT_AGAIN == 0 and !file_exists($_RECENT_TMPDIR_TO_USE . "/aaatmp.xml")) {
            echo "\n+++ERROR option -transformnoload/-uploadnotransform/-forceduploadnotransform tmpdir: tmpdir source file $_RECENT_TMPDIR_TO_USE/aaatmp.xml not found.\n";
            exit;
        }
        else if ($_DO_TRANSFORM_PROJECT_AGAIN == 0 and !file_exists($_RECENT_TMPDIR_TO_USE . "/index.xml")) {
            echo "\n+++ERROR option -transformnoload/-uploadnotransform/-forceduploadnotransform tmpdir: tmpdir index file $_RECENT_TMPDIR_TO_USE/index.xml not found.\n";
            exit;
        } else {
            foreach ($projectorgovernancegroups2serve as $mn) {
                if ($forwhat == $mn) {
                    try {
                        processprojectorgovernancegroup($mn, $url, $xsl, $debuglevel, $targetFramework2use);
                    } catch (Exception $e) {
                        echo "\n+++ Exception caught while processing " . $mn . 
                            " Message: " . $e->getMessage() . " [transformnoload, uploadnotransform, forceduploadnotransform]\n";
                    }
                }
            }
        }
    } else if ($argv[1] == '-check') {
         /* say hello */
        echo "$scriptname [$botversion] for $botname\n";
           $haserrors = false;
            $log = "";
            foreach (glob("tmp-*") as $filename) {
                $sfile = $filename . "/" . LOCAL_SUCCESS_FILENAME;
                if (file_exists($sfile)) {
                    $log .= file_get_contents($sfile);
                } else {
                    $log .= "<error dir=\"$filename\">";
                    $errf = $filename . "/logmsg.txt";
                    if (file_exists($errf)) {
                        $tmplx = file_get_contents($errf);
                        $tmplx = nl2br($tmplx, true);
                        $log .= $tmplx;
                    }

                    $log .= "</error>";
                    $haserrors = true;
                }
            }
            $nu = date('Y-m-d\TH:i:s', time());
            $log = "<log at=\"$nu\">" . $log . "</log>";
            file_put_contents(LOCAL_OVERLOG_FILENAME, $log);
            if (strlen($info1email) > 0) {
                if ($haserrors) {
                    //mail($info1email, "$scriptname $botversion error report", $log);
                    echo $log;
                }
            }
     } else {
        /* say hello */
        echo "$scriptname [$botversion] for $botname\n";
        foreach ($projectorgovernancegroups2serve as $mn) {
            if ($argv[1] == $mn) {
                try {
                    processprojectorgovernancegroup($mn, $url, $xsl, $debuglevel, $targetFramework2use);
                } catch (Exception $e) {
                    echo "+++ Exception caught while processing " . $mn . 
                        " Message: " . $e->getMessage() . " [transformnload some]\n";
                }
            }
        }
    }
} else {
    /* say hello */
    echo "$scriptname [$botversion] for $botname\n";
   foreach ($projectorgovernancegroups2serve as $mn) {
        // process all configured projects or governance groups, don't fail overall, if one fails
        try {
            processprojectorgovernancegroup($mn, $url, $xsl, $debuglevel, $targetFramework2use);
        } catch (Exception $e) {
            echo "\n+++ Exception caught while processing " . $mn . 
                " Message: " . $e->getMessage() . " [transformnload all]\n";
        }
    }
}

echo "*** konec --------\n";
exit;


function processprojectorgovernancegroup($mn, $url, $xsl, $debuglevel, $targetFramework2use)
{
    /* real life starts here */
    /* ===================== */
    /*
        mn            project or governance group to process, indicated as a mnemonic
                      as part of the config file
        url           url to RetrieveArtefacts4Wiki on server
        xsl           xsl to use
        debuglevel    debuglevel
        targetFramework2use
    */
    global $_DO_LOAD_PROJECT_AGAIN;
    global $_DO_LOAD_PROJECT_FORCED;
    global $_DO_TRANSFORM_PROJECT_AGAIN;
    global $_RECENT_TMPDIR_TO_USE;

    global $tixfile;
    global $allTIX;
    global $leftoverfilename;
    global $successfilename;
    global $progressfilename;

    if (is_file("softhyphens.php")) {
        include("softhyphens.php");
    } else {
        echo "+++ERROR not properly configured: softhyphens.php not found.\n";
        exit;
    }

    // error_reporting( E_ALL | E_STRICT );

    $starttime = date('YmdHis', time()); /* Mediawiki format */
    $nu = date('Y-m-d H:i:s', time());   /* readable format */

    $prefixtag = "[" . $mn . "]";

    // temporary output dir and file
    if ($_DO_LOAD_PROJECT_AGAIN === "1") {
        $tmpdir = "tmp-" . $starttime . "-" . $mn;
        mkdir($tmpdir);
    } else {
        $tmpdir = $_RECENT_TMPDIR_TO_USE;
    }
    $tmpfile = $tmpdir . "/aaatmp.xml";

    /* presets */
    $javacmd = "java -Xmx4096m -jar";
    $javajar = "saxon9he.jar";
    //$javajar="saxon9ee.jar";
    // params carries temp dir
    $params = "tmpdir=$tmpdir";
    $logfile = $tmpdir . "/logmsg.txt";
    $consolefile = $tmpdir . "/consolemsg.txt";
    $leftoverfilename = $tmpdir . "/" . LOCAL_LEFTOVER_FILENAME;
    $successfilename = $tmpdir . "/" . LOCAL_SUCCESS_FILENAME;
    $progressfilename = $tmpdir . "/" . LOCAL_PROGRESS_FILENAME;

    /* PHASE : get the ART-DECOR project object unref'd */

    // include the config for this project or governance group
    // presets
    $active = 0;
    $retriesOnError = 0;
    $artefacts2export = array();
    $projectorgovernancegroup = [];
    $includerefs = [];
    $onlyartefacts = [];
    $excludeartefacts = [];
    $mywikiapiurl = '';
    $botuser = '';
    $botpass = '';
    $botapitoken = '';
    $name='';

    $configfile = "config4-" . $mn . ".php";
    if (is_file($configfile)) {
        include($configfile);
        logmenl("Starting for $name ($mn)");
    } else {
        echo "+++ERROR $mn not properly configured: $configfile not found.\n";
        return;
    }

    /* ----Determine what framework to be used and load it, no defaults---- */
    if ($targetFramework2use=='') die ("+++ERROR not set or empty in config: targetFramework2use\n");
    $possibleFrameworks = array("mediawiki", "confluence", "wordpress");
    if (!in_array($targetFramework2use, $possibleFrameworks)) {
        die ("+++ERROR: " . $targetFramework2use . " is not a supported framework.\n");
    }

    logmenl("Target Framework is a $targetFramework2use", LEVEL2, $prefixtag);
    // another parameter for transform carries target environment in adram variable
    $target4adram = "adram=" . $targetFramework2use;

    if ($active == 0) {
        logmenl("This project or governance group skipped: 'not active' found in $configfile.", LEVEL2, $prefixtag);
        return;
    }

    // login in target environment
    $target = new ARTDECORBotTargetFramework ($mywikiapiurl, $targetFramework2use);
    logme("Login $targetFramework2use...", LEVEL2, $prefixtag);

    if ($target->login($botuser, $botpass, $botapitoken) != true) {
        logmenl("+++ Login failure", LEVEL2, $prefixtag);
        die();
    }
    logmeconfirm("done");
    logmenl("BOT username: $botuser through $targetFramework2use API $mywikiapiurl", LEVEL3, $prefixtag);

    // check the environment
    logme("Checking environment for $targetFramework2use API $mywikiapiurl", LEVEL3, $prefixtag);
    if ($target->checkEnv()) {
        logmeconfirm("ok");
    } else {
        logmeconfirm("not ok, exiting. Please contact the admin.");
        exit;
    }

    // create the overall (total) index file for this target, if not already existent
    $tixfile = TIX_DIRECTORY . "/" . $mn . "." . $targetFramework2use . "." . $target->tinymd5($mywikiapiurl, 7) . ".ix.json";
    logmenl("Writing to TIX file $tixfile", LEVEL2, $prefixtag);
    if (is_file($tixfile)) {
        $tix = file_get_contents($tixfile);             // get all index en tries
        $allTIX = json_decode($tix, true);              // store all index entries for later use
    }
    if ($allTIX == null) {
        // init the file
        $allTIX = array(
            'index' => array(
                'index_object' => 'sha1_of_repo_object'
            )
        );
        file_put_contents($tixfile, json_encode($allTIX));   // touch it, causes creation if it is not there
    }
    // record the target environment and the target URL in TIX
    setTIX(TIX_INDEX_TARGET, $targetFramework2use . "@" . $mywikiapiurl);
    // indicate start in TIX
    setTIX(TIX_INDEX_START, date('Y-m-d\TH:i:s', time()));
    // indicate short and name
    setTIX(TIX_INDEX_NAME, $name);
    setTIX(TIX_INDEX_SHORT, $mn);
    // erase any earlier error messages
    setTIX(TIX_INDEX_ERROR, "");
    
    // set the retries, if any
    if ($retriesOnError > 0)
        $target->setRepeatOnHTTPError($retriesOnError);

    logmenl("Milestone beacon", LEVEL2, $prefixtag);

    // determine what to do
    $processgovernance = false;
    $loadmsg = "";
    if (isset($projectorgovernancegroup['projectid'])) {
        $processgovernance = false;
        $theid = $projectorgovernancegroup['projectid'];
        $loadmsg = "Getting ART-DECOR project $theid ...";
        // tell tix about ID (OID) of project or governance group
        setTIX(TIX_INDEX_ID, $theid);
        setTIX(TIX_INDEX_TYPE, 'project');
    } else if (isset($projectorgovernancegroup['governancegroupid'])) {
        $processgovernance = true;
        $theid = $projectorgovernancegroup['governancegroupid'];
        $loadmsg = "Getting ART-DECOR governance group $theid ...";
        // tell tix about ID (OID) of project or governance group
        setTIX(TIX_INDEX_ID, $theid);
        setTIX(TIX_INDEX_TYPE, 'governancegroup');
    };

    // initailze progress
    setTIX(TIX_INDEX_PROGRESS, array ("percent" => 0, "phase" => "get_project"));

    // determine action level
    //  0 = no changes on the wiki, only check
    //  1 = do he real changes on the wiki
    $actionlevel = 0;
    if ($projectorgovernancegroup['actionlevel'] == '1') {
        $actionlevel = 1;
    }

    // automatic project 2 wiki

    // delete old tmp and create new empty one
    // rrmdir($tmpdir);
    // mkdir($tmpdir);

    if ($_DO_LOAD_PROJECT_AGAIN === "1") {
        logme($loadmsg, LEVEL2, $prefixtag);
        // prepare data array for the RetrieveArtefacts4Wiki
        $data = array(
            'id' => $theid,
            'isGovernanceGroup' => $processgovernance ? 'true' : 'false',
            'language' => $projectorgovernancegroup['language'],
            'withexperimentals' => 
                array_key_exists('withexperimentals', $projectorgovernancegroup) ?
                $projectorgovernancegroup['withexperimentals'] : 'false',
            'processHierarchicalGraphs' => 'false'
        );

        // create new cURL-Handle
        $ch = curl_init();

        // setze die URL und andere Optionen
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        if ($result = curl_exec($ch)) {
            file_put_contents($tmpfile, "<?xml-stylesheet type=\"text/xsl\" href=\"$xsl\"?>\n" . $result);
        } else {
            echo ("$prefixtag +++ERROR Curl error: " . curl_error($ch));
        }

        // close cURL-Handle
        curl_close($ch);

    } else {
        logme("Using stored artefacts for ART-DECOR project $mn...", LEVEL2, $prefixtag);
    }

    // check compilation
    $list = simplexml_load_file($tmpfile, "SimpleXMLElement", LIBXML_NOWARNING);

    $cmdt = $list["compilationDate"];
    if (strlen($cmdt) > 0) {
        logmeconfirm("done");
    } else {
        $errortxt = "$prefixtag +++ERROR getting ART-DECOR compiled version of the project as file $tmpfile thru $url.";
        setTIX(TIX_INDEX_ERROR, $errortxt);
        logmenl("Error", LEVEL2, $prefixtag);
        logmenl($errortxt, LEVEL3, $prefixtag);
        $x = file_get_contents($tmpfile);
        echo $x;
        die();
    }

    logmenl("Milestone beacon", LEVEL2, $prefixtag);

    // if this is a governance group loop through the projects listed
    // each project is processed separately
    // if this is a project continue with the transformation
    $toprocess = array();
    if ($processgovernance == 'true' and $_DO_LOAD_PROJECT_AGAIN === "1") {
        $ggdoc = new DOMDocument();
        if (!$ggdoc->load($tmpfile)) {
            $errortxt = "+++ Error reading temporary xml file...";
            setTIX(TIX_INDEX_ERROR, $prefixtag . " " . $errortxt);
            logmenl($errortxt, LEVEL3, $prefixtag);
            die();
        };
        foreach ($ggdoc->getElementsByTagName('identOfGovernanceGroup') as $igg) {
            // if ($toprocess != array()) continue;
            $ident = $igg->getAttribute('ident');
            $pid = $igg->getAttribute('pid');
            logme("Processing governance group member $ident ($pid): getting project... ", LEVEL2, $prefixtag);
            // prepare data array for the RetrieveArtefacts4Wiki
            $data = array(
                'id' => $pid,
                'isGovernanceGroup' => 'false',
                'language' => $projectorgovernancegroup['language'],
                'withexperimentals' => $projectorgovernancegroup['withexperimentals'] == 'true' ? 'true' : 'false'
            );
            // create new cURL-Handle
            $ch = curl_init();
            // setze die URL und andere Optionen
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $resulttmpfile = $tmpdir . "/aaatmp-" . $pid . ".xml";
            if ($result = curl_exec($ch)) {
                file_put_contents($resulttmpfile, $result);
            };
            // close cURL-Handle
            curl_close($ch);
            // check compilation
            $list = simplexml_load_file($resulttmpfile, "SimpleXMLElement", LIBXML_NOWARNING);
            $cmdt = $list["compilationDate"];
            if (strlen($cmdt) > 0) {
               logmeconfirm("done");
            } else {
                $errortxt = "+++ERROR getting ART-DECOR compiled version of the project as file $resulttmpfile thru $url.";
                setTIX(TIX_INDEX_ERROR, $prefixtag . " " . $errortxt);
                logmenl("Error", LEVEL2, $prefixtag);
                logmenl($errortxt, LEVEL3, $prefixtag);
                $x = file_get_contents($resulttmpfile);
                echo $x;
                die();
            }
            $toprocess[] = $resulttmpfile;
        }

        // you now have all projects from that governance group, do the merger now
        $resultallfile = $tmpdir . "/aaatmp-all.xml";
        foreach ($toprocess as $f) {
            logmenl("... merging $f", LEVEL2, $prefixtag);
            $x = file_get_contents($f);
            file_put_contents($resultallfile, $x, FILE_APPEND);
        }
        // and add the wrapping element
        $x = file_get_contents($resultallfile);
        file_put_contents($resultallfile, "<wrapper>" . $x . "</wrapper>");
        // do the merger
        $mergexsl = getmergerxsl();
        $mergexslfile = $tmpdir . "/merger.xsl";
        file_put_contents($mergexslfile, $mergexsl);
        $command = $javacmd . " " . $javajar . " -u -xsl:" . $mergexslfile . " -s:" . $resultallfile . " " . $params . " " . $target4adram . " 2> " . $logfile . " >> " . $consolefile;
        exec($command);
        $toprocess = array("$tmpdir/aaatmp-merged.xml");
        if (!file_exists("$tmpdir/aaatmp-merged.xml")) {
            $errortxt ="+++ERROR getting ART-DECOR merged version of the project to the merged file.";
            setTIX(TIX_INDEX_ERROR, $prefixtag . " " . $errortxt);
            logmenl("Error", LEVEL2, $prefixtag);
            logmenl($errortxt, LEVEL3, $prefixtag);
            die();
        }

    } else {
        // either not a governance group OR no load and merge
        if ($_DO_LOAD_PROJECT_AGAIN === "1") {
            $toprocess[] = $tmpfile;
        } else {
            if ($processgovernance == 'true') {
                $toprocess = array("$tmpdir/aaatmp-merged.xml");
            } else {
                $toprocess = array("$tmpdir/aaatmp.xml");
            }
            if (!file_exists($toprocess[0])) {
                $errortxt = "+++ERROR getting ART-DECOR merged version of the project to the merged file.";
                setTIX(TIX_INDEX_ERROR, $prefixtag . " " . $errortxt);
                logmenl("Error", LEVEL2, $prefixtag);
                logmenl($errortxt, LEVEL3, $prefixtag);
                die();
            }
        }
    }

    logmenl("Milestone beacon", LEVEL2, $prefixtag);

    /*
        check the sha1 of the overall file to process
        either aaatmp-merged.xml for governance groups or aaatmp.xml for projects and decide whether th process it
        keep in mind that the last run may not be completed successfully, if so process anyway
    */
    $aaasha1 = sha1($toprocess[0]);  // in the end, there is only one
    // update also tix file for this item
    // echo LOG $allTIX["index"][$tixkey] . "-" . $aaasha1 . "\n";
    if (getTIX(TIX_INDEX_RAWINPUT) == $aaasha1) {
        logmenl("new input set for transformation and last one are identical", LEVEL2, $prefixtag);
        $lstixo = getTIX(TIX_INDEX_SUCCESS);
        $lstix = json_encode($lstixo);
        $lastsuccess = json_decode('{"percent": 0, "at": "?"}');
        if (strlen($lstix) > 0) {
            $lastsuccess = json_decode($lstix);
        }
        if ($lastsuccess->percent == 100) {
            logmenl("Last run at " . $lastsuccess->at . " was succesful (0 errors, 100%) so we do not need to process anything!", LEVEL2, $prefixtag);
            if($_DO_LOAD_PROJECT_FORCED) {
                logmenl("Due to option 'forced' we start processing now...", LEVEL2, $prefixtag);
            } else {
                $toprocess = array();   // unless forced make it empty, causes that noting is processed
            }
        } else {
            // store new sha1 for the input set
            setTIX(TIX_INDEX_RAWINPUT, $aaasha1);
        }
    } else {
        setTIX(TIX_INDEX_RAWINPUT, $aaasha1);
    }

    // create a list of artefact refs that shall be included despite they might
    // come from a different governance group
    $increfs = "<includerefs>";
    $includereffile = $tmpdir . "/includerefs.xml";
    if (count($includerefs) > 0) {
        foreach ($includerefs as $ir) {
            $increfs .= "<ref ref=\"" . $ir . "\"/>";
        }
        $increfs .= "</includerefs>\n";
        file_put_contents($includereffile, $increfs);
    }

    // indicate progress
    setTIX(TIX_INDEX_PROGRESS, array ("percent" => 0, "phase" => "transform"));

    // do preps only if there is anything to process
    $onlystatus = 'draft-active-retired'; // export artefacts of all status categories
    if (count($toprocess) > 0) {
        /* ----PHASE : transform the extract into wiki fragments---- */
        logmenl("Milestone beacon", LEVEL2, $prefixtag);
        logmenl("Transforms (" . $projectorgovernancegroup['language'] . ") to $targetFramework2use", LEVEL2, $prefixtag);

        // additional artefacts?
        if (isset($artefacts2export['onlystatus'])) {
            if ($artefacts2export['onlystatus'] == 'active-retired') $onlystatus = 'active-retired';
        }
        logmenl("Status category axis of artefact(s): $onlystatus", LEVEL2, $prefixtag);
        $processDatasets = '';
        $processScenarios = '';
        $processTerminologyAssociations = '';
        $processValueSets = 'processValueSets=true processValueSetsStartheadlevel=1';
        $processTemplates = 'processTemplates=true processTemplatesStartheadlevel=1';
        // var_dump($artefacts2export);
        if (count($artefacts2export) > 0) {
            logmenl("additional/changed artefact(s):", LEVEL2, $prefixtag);
            if (isset($artefacts2export['DS'])) {
                if ($artefacts2export['DS']['export'] === true) {
                    $processDatasets = 'processDatasets=true';
                    logmenl("+ Datasets", LEVEL3, $prefixtag);
                } else {
                    $processDatasets = 'processDatasets=false';
                }
                if (strlen($artefacts2export['DS']['startheadlevel']) > 0)
                    $processDatasets .= ' processDatasetsStartheadlevel=' . $artefacts2export['DS']['startheadlevel'];
            }
            if (isset($artefacts2export['SC'])) {
                if ($artefacts2export['SC']['export'] === true) {
                    $processScenarios = 'processScenarios=true';
                    logmenl("+ Scenarios / Transactions", LEVEL3, $prefixtag);
                } else {
                    $processScenarios = 'processScenarios=false';
                }
                if (strlen($artefacts2export['SC']['startheadlevel']) > 0)
                    $processScenarios .= ' processScenariosStartheadlevel=' . $artefacts2export['SC']['startheadlevel'];
            }
            if (isset($artefacts2export['TA'])) {
                if ($artefacts2export['TA']['export'] === true) {
                    $processTerminologyAssociations = 'processTerminologyAssociations=true';
                    logmenl("+ Terminology Associations / Concepts Maps", LEVEL3, $prefixtag);
                } else {
                    $processTerminologyAssociations = 'processTerminologyAssociations=false';
                }
                if (strlen($artefacts2export['TA']['startheadlevel']) > 0)
                    $processTerminologyAssociations .= ' processTerminologyAssociationsStartheadlevel=' . $artefacts2export['TA']['startheadlevel'];
            }
            if (isset($artefacts2export['VS'])) {
                if ($artefacts2export['VS']['export'] === false) {
                    $processValueSets = 'processValueSets=false';
                    logmenl("- Value Sets", LEVEL3, $prefixtag);
                } else {
                    $processValueSets = 'processValueSets=true';
                }
                if (strlen($artefacts2export['VS']['startheadlevel']) > 0)
                    $processValueSets .= ' processValueSetsStartheadlevel=' . $artefacts2export['VS']['startheadlevel'];
            }
            if (isset($artefacts2export['TM'])) {
                if ($artefacts2export['TM']['export'] === false) {
                    $processTemplates = 'processTemplates=false';
                    logmenl("- Templates", LEVEL3, $prefixtag);
                } else {
                    $processTemplates = 'processTemplates=true';
                }
                if (strlen($artefacts2export['TM']['startheadlevel']) > 0)
                    $processTemplates .= ' processTemplatesStartheadlevel=' . $artefacts2export['TM']['startheadlevel'];
            }
        }
        // echo $processDatasets . "\n" . $processScenarios . "\n" . $processTerminologyAssociations . "\n" . $processValueSets . "\n" . $processTemplates . "\n" ;

        // prepare additional parameters for the transform process
        $language = 'language=' . $projectorgovernancegroup['language']; // language param for the transform
    }

    /* presets */
    $logentry = '';
    // counters
    $seen = 0;
    $checked = 0;
    $updated = 0;
    $errors = 0;
    $leftovers = 0;
    $recovered = 0;
    $statusskipped = 0;

    // update progress
    $progressxml = progressLine ('progress', 0, $name, $mn, $seen, $checked, $updated, $errors, $recovered, -1, $starttime, date('Y-m-d\TH:i:s', time()), $targetFramework2use);
    file_put_contents($progressfilename, $progressxml);
    $pxml = simplexml_load_string($progressxml);
    $pjson = json_encode($pxml, JSON_NUMERIC_CHECK);
    $pjson = json_decode($pjson, true);    
    $itmp = $pjson["@attributes"]["elapsed"];
    setTIX (TIX_INDEX_PROGRESS, $pjson["@attributes"]);

    /*
     * run through all entries in the converted index file with all element ix, this is tphase 1
     * for that purpose add the emtpy leftover file to the list of to be processed files
     * after tphase 1 look if there are any entries in the leftover file, same structure
     */
    $tphase = 0;  // transmission phase start
    addToLeftovers ("");   // init the leftover file
    if (count($toprocess) > 0)   // only if there is a regular index to process
        $toprocess[] = $leftoverfilename; // add the empty leftovers file that will contain the list of leftover processable entries, after pphase 1

    // determine overall number of items to process
    $total = 0;

    foreach ($toprocess as $f) {

        if ($f == $leftoverfilename) {
            if ($leftovers > 0) {
                logmenl("Processing $leftovers leftovers from processing phase $tphase", LEVEL2, $prefixtag);
            } else
                $tphase++;  // up the tphase so that we definitely will not do this phase
        }
        $tphase++;  // up the tphase
        // continue if you are above phase 2
        if ($tphase > 2) continue;

        // get and transform only if in tphase 1 (original ransformation phase, not the leftover phase)
        $indexfile = $tmpdir . "/" . LOCAL_INDEX_FILENAME;
        if ($tphase == 1) {

            if ($_DO_TRANSFORM_PROJECT_AGAIN === "1") {
                logmenl("Transforming $f", LEVEL2, $prefixtag);
                $input = $f;
                // -u : stylesheet is a URL, -xsl : the stylesheet, -s : the input file
                // 2> redirect message/errors to logmsg.txt, rest to /dev/null

                $command = $javacmd . " " . $javajar . " -u -xsl:" . $xsl . " -s:" . $input . " " .
                    $params . " " .
                    $target4adram . " " .
                    $processDatasets . " " .
                    $processScenarios . " " .
                    $processTerminologyAssociations . " " .
                    $processValueSets . " " .
                    $processTemplates . " " .
                    $language .
                    " 2> " . $logfile . " >> " . $consolefile;
                // echo($command); exit;
                exec($command);
            } else {
                logmenl("Using old transform from $f", LEVEL2, $prefixtag);
            }
            logmenl("Milestone beacon", LEVEL2, $prefixtag);
        } else {
            // in tphase 2 the index file is the leftover filename
            $indexfile = $leftoverfilename;
        }

        logmenl("Importing export fragments into $targetFramework2use", LEVEL2, $prefixtag);

        if (count($onlyartefacts) > 0) {
            logmenl("Note: List of artefacts to be updated only contains " . count($onlyartefacts) . " items!", LEVEL3, $prefixtag);
        }
        if (count($excludeartefacts) > 0) {
            logmenl("Note: List of artefacts to be excluded contains " . count($onlyartefacts) . " items!", LEVEL3, $prefixtag);
        }

        logmenl("Get index list", LEVEL3, $prefixtag);

        /* get list of all items created beforehand */
        $xmldoc = new DOMDocument();
        $call = $indexfile;
        if (!$xmldoc->load($call)) {
            $errortxt = "+++ Error reading index xml file $indexfile...";
            setTIX(TIX_INDEX_ERROR, $prefixtag . " " . $errortxt);
            logmenl("Error", LEVEL3, $prefixtag);
            logmenl($errortxt, LEVEL4, $prefixtag);
            die();
        }

        foreach ($xmldoc->getElementsByTagName('ix') as $ix) {
            $total++;
        }

        foreach ($xmldoc->getElementsByTagName('ix') as $ix) {

            $seen += 1;
            if ($seen % 20 == 0) {
                $percent = round( ($seen / $total) * 100, 0);
                logmenl("Milestone beacon ($percent%): seen $seen/$total", LEVEL3, $prefixtag);
                sleep(1); // doe rustig aan :-)
                // save progress to external file do that external processes can watch it
                $milestonetime = date('Y-m-d\TH:i:s', time());
                $progressxml = progressLine ('progress', $percent, $name, $mn, $seen, $checked, $updated, $errors, $recovered, $total, $starttime, $milestonetime, $targetFramework2use);
                file_put_contents($progressfilename, $progressxml);
                $pxml = simplexml_load_string($progressxml);
                $pjson = json_encode($pxml, JSON_NUMERIC_CHECK);
                $pjson = json_decode($pjson, true);    
                $itmp = $pjson["@attributes"]["elapsed"];
                setTIX (TIX_INDEX_PROGRESS, $pjson["@attributes"]);
            }
            // maybe we do a re-login here for renewal of credentials?
            if ($seen % 250 == 0) {
                if ($target->login($botuser, $botpass, $botapitoken) != true) {
                    logmenl("+++ Re-Login failure", LEVEL3, $prefixtag);
                    die();
                }
            }

            // check filename, no processing if empty
            $inputfile = $ix->getAttribute('fn');
            if (strlen($inputfile) == 0) continue;

            // check status, no processing if status is not in onlystatus categories
            $astatus = $ix->getAttribute('statusCode');
            if ($astatus) {
                // draft-active-retired is default, check others
                if ($onlystatus === 'active-retired') {
                    if (
                        ($astatus == 'draft') or 
                        ($astatus == 'new') or 
                        ($astatus == 'pending') or 
                        ($astatus == 'review')
                    ) {
                        $statusskipped++;
                        continue;
                    } 
                }
            }

            // process this thing only if its identifier is
            // in onlyartefacts or onlyartefacts is empty
            // or it is in includeartefacts
            $theObjectId = $ix->getAttribute('id');
            $processthisartefact = 0;
            if (count($onlyartefacts) == 0) {
                $processthisartefact = 1;
            } else {
                foreach ($onlyartefacts as $oa) {
                    if ($theObjectId == $oa) {
                        $processthisartefact = 1;
                    }
                }
                foreach ($includerefs as $ia) {
                    if ($theObjectId == $ia) {
                        $processthisartefact = 1;
                    }
                }
            }
            // skip this artefact if its identifier is
            // excludeartefacts
            if (count($excludeartefacts) > 0) {
                foreach ($excludeartefacts as $ea) {
                    if ($theObjectId == $ea) {
                        $processthisartefact = 0;
                    }
                }
            }
            if ($processthisartefact == 0) continue;

            if (!is_file($inputfile)) {

                logmenl("+++ Not a file $inputfile, please check missing file", LEVEL3, $prefixtag);

            } else {

                $wikiobject = $ix->getAttribute('wiki');
                $imgobject = $ix->getAttribute('img');
                $type = $ix->getAttribute('type');

                /*
                echo $recentversion->saveXML() ;
                die();
                */

                if ($debuglevel > 1) logme("Processing item $inputfile as $wikiobject", LEVEL3, $prefixtag);

                if (($type == 'text') or ($type == 'html') or ($type == 'svg')) {
                    $inputsource = file_get_contents($inputfile);
                } else {
                    $inputsource = "";
                }

                $newwikicontent = "";
                /* dependent from the type of the artefact (kind of mime type) act upon the upload type and process */
                if ($type == 'text') {
                    /* new content verbatim */
                    $newwikicontent = $inputsource;
                    /* eliminate unwanted chars at end of text */
                    $newwikicontent = rtrim($newwikicontent);
                } else if ($type == 'html') {
                    /* its HTML, strip the DOCTYPE if any */
                    $cpos = strpos($inputsource, "<!DOCTYPE");
                    if ($cpos !== false) {
                        /* DOCTYPE found, strip it */
                        $newwikicontent = substr($inputsource, strpos($inputsource, ">\n") + 1);
                    } else {
                        $newwikicontent = $inputsource;
                    }
                    /* add some soft hyphenations by interspersing &#8203; char :-) */
                    foreach ($softhyphenlist as &$r) {
                        $or = str_replace("#", "", $r);;
                        $rr = str_replace("#", "&#8203;", $r);
                        $newwikicontent = str_replace($or, $rr, $newwikicontent);
                    }
                    /* eliminate unwanted chars at end of text */
                    $newwikicontent = rtrim($newwikicontent);
                } else if ($type == 'svg') {
                    /* img handling is different */
                    $newwikicontent = $inputsource;
                }

                if (strlen($newwikicontent) > 0) {
                    /*
                     * compare sha1 for text/html to decide whether to update or not
                     */
                    $mustTest4Equality = 'yes';
                    $nowpage = '';     // an empty nowpage for now
                    $sha1input = '-1';
                    if (($type == 'text') or ($type == 'html')) {
                        // get sha1 of the new wiki content
                        $sha1input = sha1($newwikicontent);
                        // get sha1 of the repo content
                        $sha1repo = '';  // init
                        if ($targetFramework2use="mediawiki") {
                            // MW from comment: $sha1repo = ''; //$target->getPageLatestSHA1FromCommentMW($wikiobject);
                        } else if ($targetFramework2use="confluence") {
                            // no specific way to get SHA1 of repo content
                        } else if ($targetFramework2use="wordpress") {
                            // no specific way to get SHA1 of repo content
                            // $cpagecontent = $target->getpage($wikiobject);
                            // if (strlen($cpagecontent) > 0) $sha1repo = sha1(rtrim($cpagecontent));
                        }
                        // set only for html and text type of content if sha1 is different
                        if ($sha1input === $sha1repo) {
                            $mustTest4Equality = 'sha1';  // equal sha1 means equal input and repo
                        } else if (strlen($sha1repo) === 0) {
                            // no sha1 in comment, update the page anyway
                            $mustTest4Equality = 'no';
                            // append the sha1 to teh new wiki content so it is forced to be updated, also the comment WITH an sha1
                            $newwikicontent .= "\n<!--" . $sha1input . "-->";
                        } else {
                            $nowpage = $target->getpage($wikiobject);   // get the repo page, empty on images
                        }
                    } else if ($type == 'svg') {
                        /*
                         * for images (svg) always do the compare in the upload procedure
                         */
                    }
                    // echo $wikiobject . " R" . $sha1repo . " N" . $sha1input ."\n";
                    // var_dump($target->getpage($wikiobject));exit;

                    // get overall / total index file entry for this target item and check if input is new
                    $sha1recorded = '';
                    $tixkey = $wikiobject . $imgobject;
                    $sha1recorded = getTIX($tixkey);

                    $checked += 1;

                    // if recorded sha1 from TIX is equal this item's sha1, continue
                    if ($sha1recorded == $sha1input) {
                        if ($debuglevel > 1) logmenl(".", LEVEL3);
                        continue;
                    }

                    // decide upon the update, continue if no update is neeed (for various reasons)
                    if ($actionlevel == 0) {
                        if ($debuglevel > 0)
                            logmenl("page not updated due to action level 0 for $wikiobject$imgobject", LEVEL3, $prefixtag);
                        continue;
                    }

                    // on equal sha1, continue
                    if ($mustTest4Equality === 'sha1') {
                        if ($debuglevel > 1) logmenl("equal SHA1, no update", LEVEL3);
                        continue;
                    }

                    /* compare content of input and repo */
                    if ($mustTest4Equality === 'yes') {
                        if (strcmp($nowpage, $newwikicontent) === 0) {
                            if ($debuglevel > 1) logmenl("equal content, no change", LEVEL3);
                            continue;
                        }
                    }

                    /* check CRSF token */
                    if ($target->checktoken() != true) {
                        logme("tried to refresh edit token", LEVEL3, $prefixtag);
                    }

                    // echo LOG processing // " . $tixkey . "\n";

                    if (($type == 'text') or ($type == 'html')) {
                        /* upload html or text files to the wiki */
                        if ($target->editpage($wikiobject, $newwikicontent,
                                AUTOMATIC_ADAWIB_PAGE /* edit summary */
                            ) === true) {
                            if ($debuglevel > 1) logmeconfirm("ok");
                            if ($debuglevel > 0) logme("item $inputfile as $wikiobject updated", LEVEL3, $prefixtag);
                            /* ok, add to log entry */
                            $logentry .= "<li>page updated: " . $wikiobject . "</li>\n";
                            $updated += 1;
                            // if in lefover processing phase count success of recovered errors
                            if ($tphase == 2) $recovered += 1;
                            // update also tix file for this item
                            setTIX ($tixkey, $sha1input);
                        } else {
                            // an error occured
                            if ($debuglevel > 1) logmeconfirm("not ok");
                            if ($debuglevel > 0) logme("+++ page update failure for $inputfile as $wikiobject", LEVEL3, $prefixtag);
                            $logentry .= "<li>+++ page update failure " . $wikiobject . "</li>\n";
                            $errors += 1;
                            // add this id to the left overs for later processing
                            $lo = "<ix ";
                            foreach($ix->attributes as $aa) {
                                $lo .= $aa->name . "=\"" . $aa->value . "\" ";
                            }
                            $lo .= "/>";
                            addToLeftovers ($lo);
                            $leftovers++;
                        }
                    } else if ($type == 'svg') {
                        /* upload/update the image as file or attachment */
                        $retcode = $target->upload($imgobject, $inputfile,
                            AUTOMATIC_ADAWIB_IMAGE /* edit summary */);
                        // echo "RET:" . $retcode . "\n";
                        if ($retcode == 'success') {
                            if ($debuglevel > 1) logmeconfirm("ok");
                            if ($debuglevel > 0) logme("image $inputfile as $imgobject updated", LEVEL3, $prefixtag);
                            /* ok, add to log entry */
                            $logentry .= "<li>image updated: " . $imgobject . "</li>\n";
                            $updated += 1;
                            if ($tphase == 2) $recovered += 1;
                        } else if ($retcode == 'duplicate') {
                            if ($debuglevel > 1) echo "no change\n";
                        } else {
                            // an error occured
                            if ($debuglevel > 1) logmeconfirm("not ok");
                            if ($debuglevel > 0) logme("+++ image update failure for $imgobject with $inputfile (error: $retcode)", LEVEL3, $prefixtag);
                            $logentry .= "<li>+++ image update failure " . $imgobject . "</li>\n";
                            $errors += 1;
                            // add this id to the left overs for later processing
                            addToLeftovers ($ix);
                            $leftovers++;
                        }
                    } else if ($debuglevel > 0) logme("unknown type $type, skipped", LEVEL3, $prefixtag);

                } else {
                    /* strlen($newwikicontent) === 0, so no content found or filtered */
                    if ($debuglevel > 1) logmenl("skipped", LEVEL3, $prefixtag);
                }

                if ($debuglevel > 1) logmenl(".", LEVEL3, $prefixtag);

            }

        }

        // wrap the content of the leftover file with <index> tag,
        // if there are entries they are processed in the second round
        // and if this is transmission phase 1
        if ($tphase == 1) {
            if ($leftovers > 0) {
                $lo = file_get_contents($leftoverfilename);
                $lo = "<index>" . $lo . "</index>\n";
                file_put_contents($leftoverfilename, $lo);
            } else {
                logmenl("No leftovers", LEVEL3, $prefixtag);
            }

        }

        /* ----CREATE LOGFILE ENTRY---- */
        /* create comment of headline for this log entry */
        if (strlen($logentry) > 0) {
            $cont = "<h1>" . $nu . "</h1>\nList of changed objects for " . $mn . ":\n<ul>" . $logentry . "</ul>\n";
        } else {
            $cont = "";
        }
        /* update the bot user log page */
        if ($target->log($botuser, $cont) != true) {
            logmenl("+++ Update log page failure", LEVEL3, $prefixtag);
        }

    }

    // write success file
    $finishtime = date('Y-m-d\TH:i:s', time());
    $successxml = progressLine ('success', 100, $name, $mn, $seen, $checked, $updated, $errors, $recovered, $total, $starttime, $finishtime, $targetFramework2use);
    file_put_contents($successfilename, $successxml);

    // write success to TIX 
    // update also tix file for this item
    $inseconds = "";

    $sxml = simplexml_load_string($successxml);
    $sjson = json_encode($sxml, JSON_NUMERIC_CHECK);
    $sjson = json_decode($sjson, true);    
    $itmp = $sjson["@attributes"]["elapsed"];
    setTIX (TIX_INDEX_SUCCESS, $sjson["@attributes"]);
    // nullify progress
    setTIX(TIX_INDEX_PROGRESS, null);
    //$ihours = sprintf("%d", floor($itmp / 3600));
    //$iminutes = sprintf("%d", floor(($itmp / 60) % 60));
    //$iseconds = $itmp % 60;
    if ($itmp > 0) {
        $inseconds = " in ";
        //if ($ihours > 0) $inseconds .= " " . $ihours . "h";
        //if ($iminutes > 0) $inseconds .= " " . $iminutes . "m";
        //$inseconds .= " " . $iseconds . "s";
        $inseconds .= floor($itmp / 3600) . gmdate(":i:s", $itmp % 3600);
    }
    // number_format($itmp, 0, "." , ",") . " seconds";
    // sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);

    // say what you have done
    $res = "Summary: item statistics for $mn - $seen seen$inseconds, $checked checked, $updated updated, $errors errors, $statusskipped skipped for status";
    if ($recovered > 0) $res .= " of which $recovered were recovered.\n";
    if ($debuglevel > 0) logmenl($res, LEVEL3, $prefixtag);

}

/*
 * TIX file handle
 *
 */
function setTIX ($ix, $value) {
    global $allTIX;
    global $tixfile;
    $allTIX["index"][$ix] = $value;
    file_put_contents($tixfile, json_encode($allTIX));
}
function getTIX ($ix) {
    global $allTIX;
    if (isset($allTIX["index"][$ix]))
        return $allTIX["index"][$ix];
    else
        return "";
}
/*
 * function that adds a command to the leftover shell script for later processing
 * param id - the id to re-process
 * param tempdir - the tmp directory name
 *
 */
function addToLeftovers ($ix) {
    global $leftoverfilename;
    file_put_contents($leftoverfilename, $ix, FILE_APPEND);
}

/*
 *  rrmdir - recursive remove directory
 *  function to remove a complete directory including all subdirectories and entries
 *  param dir - directory path to be deleted
 */
function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir . "/" . $object) == "dir") rrmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
            }
        }
        reset($objects);
        rmdir($dir);
    }
}

/*
 * function to create a line of information for the success or the progress file
 * param $elm - name of the xml root element to create
 * param $percent - percent of progress so far, 100% with the success object
 * param $name
 * param $mn
 * param $seen
 * param $checked
 * param $updated
 * param $errors
 * param $recovered
 * param $starttime
 * param $milestonetime
 * param $targetFramework2use
 *
 */
function progressLine ($elm, $percent, $name, $mn, $seen, $checked, $updated, $errors, $recovered, $total, $starttime, $milestonetime, $targetFramework2use) {
    $intervalo = date_diff(date_create($starttime), date_create($milestonetime));
    $elapsed = $intervalo->s + ($intervalo->i * 60) + ($intervalo->h * 3600) + ($intervalo->d * 3600 * 24);
    return "<$elm percent=\"$percent\" name=\"$name\" short=\"$mn\" seen=\"$seen\" checked=\"$checked\" updated=\"$updated\" errors=\"$errors\" recovered=\"$recovered\" total=\"$total\" at=\"$milestonetime\" elapsed=\"$elapsed\" target=\"$targetFramework2use\"/>\n";
}

/*
 *  getmergerxsl - get the xslt code for the project file merger
 *  param - none
 */
function getmergerxsl()
{
    return <<<END
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:param name="tmpdir" select="'tmp'"/>
    
    <xsl:template match="/">
        <xsl:result-document href="{\$tmpdir}/aaatmp-merged.xml">
            <result>
            	<xsl:copy-of select="doc('aaatmp.xml')//(identOfGovernanceGroup|member)"/>
                <decor>
                    <xsl:copy-of select="//wrapper/result[1]/decor[1]/@*"/>
                    <xsl:copy-of select="(//wrapper/result/decor/project)[1]"/>
                    <datasets>
                        <xsl:for-each-group select="//wrapper/result/decor/datasets/dataset" group-by="concat(@id, @effectiveDate)">
                            <xsl:copy-of select=".[1]"/>
                        </xsl:for-each-group>
                    </datasets>
                    <ids>
                        <xsl:copy-of select="//wrapper/result/decor/ids/*"/>
                    </ids>
                    <terminology>
                        <xsl:for-each-group select="//wrapper/result/decor/terminology/valueSet" group-by="concat(@id, @effectiveDate)">
                            <xsl:copy-of select=".[1]"/>
                        </xsl:for-each-group>
                        <xsl:copy-of select="//wrapper/result/decor/terminology/(* except valueSet)"/>
                    </terminology>
                    <rules>
                        <xsl:for-each-group select="//wrapper/result/decor/rules/template" group-by="concat(@id, @effectiveDate)">
                            <xsl:copy-of select=".[1]"/>
                        </xsl:for-each-group>
                        <xsl:copy-of select="//wrapper/result/decor/rules/(* except template)"/>
                    </rules>
                </decor>
            </result>
        </xsl:result-document>
    </xsl:template>
    
</xsl:stylesheet>
END;
}

/*
 *  getuniqueidsxfromindexsl - get unique ids from index file and pit them into a command line (for later procressing)
 *  param - none
 */
function getuniqueidsxfromindexsl()
{
    return <<<END
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" exclude-result-prefixes="#all">
    
    <xsl:param name="tmpodir" select="'tmp-XXX-YYY'"/>
    
    <xsl:template match="/">
        
        <xsl:for-each-group select="index/ix" group-by="@id">
            <xsl:text>php wiki-transfer-redo.php  </xsl:text>
            <xsl:value-of select="$tmpodir"/>
            <xsl:text>  </xsl:text>
            <xsl:value-of select="@id"/>
            <xsl:text>&#10;</xsl:text>
        </xsl:for-each-group>
        
    </xsl:template>

</xsl:stylesheet>
	
?>
END;
}

/*
 * after_last (also in lib, copy here)
 * param s1 string
 * param inthat
 * returns string after last s1 in inthat
 * */
function after_last ($s1, $inthat) {
        if (!is_bool(strrevpos($inthat, $s1)))
            return substr($inthat, strrevpos($inthat, $s1)+strlen($s1));
}
function strrevpos($instr, $needle) {
        $rev_pos = strpos (strrev($instr), strrev($needle));
        if ($rev_pos===false) return false;
        else return strlen($instr) - $rev_pos - strlen($needle);
}

/* 
 * LOG functions
 * -------------
 */
function logme ($t, $level = 0, $ID = "") {
	echo date("Y-m-d H:i:s : ");
	if (strlen($ID) > 0) echo $ID;
	for($i = $level; $i > 0; $i--) echo "...";
    echo $t;
}
function logmenl ($t, $level = 0, $ID = "") {
    logme($t, $level, $ID);
    echo "\n";
}
function logmeconfirm ($t, $level = 0, $ID = "") {
    if (strlen($ID) > 0) echo "   " . $ID;
	for($i = $level; $i > 0; $i--) echo "...";
    echo " $t\n";
}