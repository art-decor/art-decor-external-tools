<?php
/*
 *
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
 *  see https://art-decor.org/mediawiki/index.php?title=Copyright
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the
 *  GNU Lesser General Public License as published by the Free Software Foundation; either version
 *  2.1 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
 *
 *  - - - - - - - - - - -
 *  CONFIG FILE FOR A SINGLE PROJECT OR GOVERNANCE GROUP using the ART-DECOR® Automatic Wiki Bot (ADAWIB)
 *  Supported platforms so far: mediawiki, confluence
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools 2013-2023
 */

// Project Human Readable Name
$name = "Some Project Name I Like";

// serves as a config for what minimal version of the script
$serves4 = "13.0";   // this config server for this version of the main script or higher

// whether this config profile - at all - should be processed (1) or not (0)
$active = 1;

// target framework to use
// either mediawiki or confluence or wordpress
$targetFramework2use = "confluence";

/*
 * Artefacts that shall be exported
 * 
 * The first parameter
 *   onlystatus
 * is a string to select artefacts only of this category of status.
 * Valid values are
 *   'draft-active-retired' - the default, exports all artefacts regardless of their status category
 *   'active-retired' - exports artefacts with status of category active or retired.
 * According to the ART-DECOR state machine
 *   draft includes also new, pending, review
 *   active includes also final
 *   retired includes also cancelled, rejected
 * 
 * By default
 * - Value Sets (VS) and
 * - Templates (TM)
 * are on (export=true)
 * To disable the export of Value Sets (VS) and Templates (TM) 
 * add the following statement to the parameter array
 * 
 *      "VS" => array(
 *	       "export" => false
 *      )
 * 
 * ...or with "TS" accordingly. In theory this works for the other artefacts too, but
 * their default is export => false anyway, and if mentioned in the array at all,
 * export => true is the default.
 *
 * To add other artefacts, add to this array any of these items
 * - DS (Datasets)
 * - SC (Scenarios)
 * - TA (Terminology Associations) per data set element
 * 
 * Default is an array with VS and TS and export => true
 * 
 * The parameter
 *   startheadlevel 
 * is an integer between 1..5 and denotes the heading level of heading used,
 * i.e. startheadlevel=3 means for example that <h3> ist used in the export for main headings
 * 
*/
$artefacts2export = array(
    "DS" => array(
        "startheadlevel" => 1,
        "export" => false
    ),
    "SC" => array (
        "startheadlevel" => 1,
        "export" => false
    ),
    "TA" => array (
        "startheadlevel" => 1,
        "export" => false
    ),
    "VS" => array (
        "startheadlevel" => 1,
        "export" => true
    ),
    "TM" => array (
        "startheadlevel" => 1,
        "export" => true
    )
);

// data for project or governance group to process
$projectorgovernancegroup = array(
    'projectid' => '--the ID (OID) of the project in ART_DECOR®--',
    // -- OR --
    //'governancegroupid' => '--the ID (OID) of the governance group in ART_DECOR®--',
    //
    // language, shall be en-US, de-DE etc.
    'language' => 'en-US',
    //
    // action level
    //  0 = no changes on the target environment, only check
    //  1 = do the real changes on the target environment
    'actionlevel' => '1',
    //
    // when processing governance group, include experimental projects true | false
    // needed to be 'true' for example or demo or experimental projects
    // in order to be processed
    'withexperimentals' => 'true'
);

// list of id's of the only artefacts that are required to be updated if needed
// if empty, every artefact is updated if needed
$onlyartefacts = array(
    '--the ID (OID) of the artefact in ART_DECOR®--',
    '--the ID (OID) of the artefact in ART_DECOR®--'
);

// list of artefact refs that shall explicitly be included in an update
// if empty no artefact is skipped
$includerefs = array(
    '--the ID (OID) of the artefact in ART_DECOR®--',
    '--the ID (OID) of the artefact in ART_DECOR®--'
);

// list of id's of the only artefacts that are to be excluded
// if empty no artefact is excluded
$excludeartefacts = array(
    '--the ID (OID) of the artefact in ART_DECOR®--',
    '--the ID (OID) of the artefact in ART_DECOR®--'
);

// Credential parameters
//
// example mediawiki:
// $mywikiapiurl = '--Mediawiki/Confluence/Wordpress API Url--';
// $botuser = '--Bot User Name--';
// $botpass = '--Bot Password--';
// $botapitoken = '--Bot Token--';
//
// example confluencec:
$mywikiapiurl = "--Confluencec API Url--";
$botuser = '--Bot User Name--';
$botpass = '--Bot Password--';
$botapitoken = "--Bot API Token--";

?>