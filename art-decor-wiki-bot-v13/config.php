<?php
/*
 *
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
 *  see https://art-decor.org/mediawiki/index.php?title=Copyright
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the
 *  GNU Lesser General Public License as published by the Free Software Foundation; either version
 *  2.1 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
 *
 *  - - - - - - - - - - -
 *  CONFIG FILE for this server to support the ART-DECOR® Automatic Wiki Bot (ADAWIB)
 *  Supported platforms so far: mediawiki, confluence
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools 2013-2023
 */

// the URL to the xquery to retrieve artefacts
// for a project or a governance group
//
$url = "http://art-decor.org/decor/services/RetrieveArtefacts4Wiki";

// supervisor email address to send alerts, warnings and hints to
$info1email = "x@y.de";

// list of projects or governance groups to process,
// indicated as a mnemonic
// each of this mnemonics must have a config file named
//   {mnemonic}.config.php
// containing the project parameters and the wiki url
// and ADbot signatures
$projectorgovernancegroups2serve = array (
	"governancegroup-x",
	"project-x"
) ;

// debuglevel
//  1 = show milestones and changes in wiki only
//  2 = show milestones and any wiki action
$debuglevel = 1;

// XSL action
$xsl="http://art-decor.org/ADAR/rv/DECOR2wiki.xsl";

?>
