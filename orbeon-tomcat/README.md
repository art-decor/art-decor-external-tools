# Orbeon and Tomcat for ART-DECOR Release 2

Please refer to our [documentation](https://docs.art-decor.org/administration/frontend2/).

## Versions of the art-decor.war package

| Filename                       | md5                              |
| ------------------------------ | -------------------------------- |
| [art-decor.war](art-decor.war) | 56ab6429b423d576b5ed737871507060 |
