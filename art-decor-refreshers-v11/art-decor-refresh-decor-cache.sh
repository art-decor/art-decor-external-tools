#!/bin/sh
# Refresh some ART-DECOR collections 
#

DIR=/opt/art-decor-refreshers
DATE=`date +%Y%m%d-%H%M`
LOG=logs/log-${DATE}.xml
cd ${DIR}

php art-decor-refresher.php --periodic-decor-cache-refresh >> ${LOG}

# cleanup log files older than 50 days
find logs/* -mtime +50 -exec rm {} \;

