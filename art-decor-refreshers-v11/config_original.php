<?php
// the URL to the refresh sandbox xquery
$urlsandbox = "localhost:8877/systemtasks/modules/refresh-sandbox.xquery" ;
// the URL to the refresh decor cache xquery
$urlcache = "localhost:8877/systemtasks/modules/refresh-decor-cache.xquery" ;

// secret data for the automatic notifier system
$data = array (
// the user name of the adbot user in Exist-DB, having the dba right
        'user' => 'adbot',
// the secret password of that user
        'password' => 'ADBOTPASSWORD',
// the refresher password, fixed
        'secret' => 'SECRET'
        );
?>
