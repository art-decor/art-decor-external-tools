<?php
/*
	ADRESH art-decor refresher service
	
    Copyright (C) 2013-2017 ART-DECOR Expert Group art-decor.org
    Copyright (C) 2016-2017 ART-DECOR Open Tools   art-decor-open-tools.net
    
    Author: Kai U. Heitmann

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.

    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
*/

	// name and version of this script
	$thisscriptname = "ADRESH";
	$thisscriptversion = "v1.2";  

	// get config data
	require "config.php";

	// look for command line argument [1] to determine action
	// defaults to periodic-sandbox-refresh
	// valid actions so far:
	//   --periodic-sandbox-refresh
	//   --periodic-decor-cache-refresh

	// set the default
	$action = "--periodic-sandbox-refresh";
	// get the action
	$argv2 = "";
	if ($argc > 1) $argv2 = htmlspecialchars(strtolower($argv[1]));
	if ($argc > 2) {
		echo "+++ERROR too many command line options (only 1 allowed at a time).\n";
		exit;
	}
	if (strlen($argv2)>0) $action = $argv2;
	// test if action is available
	$url = "";
	switch ($action) {
	  case '--periodic-sandbox-refresh' :
		$url = $urlsandbox;
		break;
	  case '--periodic-decor-cache-refresh' :
		$url = $urlcache;
		break;
	  default:
		echo "+++ERROR $action not a valid command line option.\n";
		// fail
		exit;
	}

	// append the action to the end of the URL data array
	$data["action"] = $action;

	// create new cURL-Handle
	$ch = curl_init();

	// setze die URL und andere Optionen
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// action for decor bbrs
	$data["cacheformat"] = "decor";
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	if ($result = curl_exec($ch) ) {
			echo "$result\n";
	} ;
	// action for fhir bbrs
	$data["cacheformat"] = "fhir";
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	if ($result = curl_exec($ch) ) {
			echo "$result\n";
	} ;

	// close cURL-Handle
	curl_close($ch);

?>
