# ART-DECOR® Tools and Support

This repository contains external documentation and tools of the ART-DECOR Expert Group and ART-DECOR Open Tools.

It is a public repository.

## External tools

### Ping

Ping script to test server status and to collection uptimes: [art-decor-ping-v11](/art-decor-ping-v11)

### ADRESH (Release 2 only)

ART-DECOR refresher service: [art-decor-refreshers-v11](/art-decor-refreshers-v11)

*Note that from ART-DECOR Release 3 on, all refresher services are build-in functions.*

### ADRAM

ART-DECOR release and archive manager: [art-decor-release-and-archive-manager-v20](/art-decor-release-and-archive-manager-v20)

### ADAWIB

ART-DECOR wiki bot: [art-decor-wiki-bot-v12](/art-decor-wiki-bot-v12)

### ADGITSY

ART-DECOR Git Synchronization (Github, Gitlab, Bitbucket)

### Performance 

Performance tester script: [art-decor-performancetester-v10](/art-decor-performancetester-v10)

## Setup and Maintenance Support

For **Setup and Maintenance Support**, please refer to our [documenation](https://docs.art-decor.org/administration/setupmaintain/).

## Testing

ART-DECOR backend tests: [art-decor-backend-tests-bash](/art-decor-backend-tests-bash)

