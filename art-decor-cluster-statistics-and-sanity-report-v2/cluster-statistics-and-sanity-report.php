<?php
/*
	ART-DECOR Cluster Statistics and Sanity Report
	
    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
    https://docs.art-decor.org/copyright/
    
    Author: Kai U. Heitmann

*/

// name and version of this script
$thisscriptname = "ADSTASAN";
$thisscriptversion = "v2.00";

date_default_timezone_set('Europe/Berlin') ;

// PHPmailer to be able to use mail via SMTP localhost
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php'; 

// constants
define("__PUBREPO", "PUBLIC REPOSITORY");
define("__NOTAVAILAPPIC", "n/a");

/*  include CONFIGS AND DEFAULTS */
if (is_file("config.php")) {
	include("config.php");
} else {
	echo "+++ERROR not properly configured: config.php not found.\n";
	exit;
}

/* check required settings */
if (!isset($dosendmail)) die ("+++ERROR not set in config: dosendmail\n");
if (!$recipients) die ("+++ERROR not set in config: recipients\n");
if (!$servers) die ("+++ERROR not set in config: servers\n");

// start the analysis for the report
$nowh = date("Y-m-d H:i:s");
$now = date("YmdHis");

// say hello
echo $nowh . " " . $thisscriptname . " " . $thisscriptversion . "\n";

// output init
$headline = 'Cluster Statistics and Sanity Report (' . date("Y-m-d H:i:s") . ')';
$out = '';           // html output file
$summary = array();  // a quick summary of all servers
$ignored = '';

$out .= '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
$out .= '<title>' . $headline . '</title>';
$out .= '<style>' . _getCSS() . '</style>';
$out .= '</head><body>';
$out .= '<div class="container"><div class="main-body">';
$out .= '<h3>' . $headline . '</h3>';
$out .= '<span class="summary"></span>';

// hush through all server definitions

// report cards
$out .= '<div class="card-container">';

foreach ( $servers as $skey => $server ) {

	$servername = $server['name'];
	$desc = $server['desc'];
	$url = $server['baseurl'];

	echo "Querying " . $servername . "\n";

	$errorseverity = 0; // assume no errors occurred

	// to do https://develop.art-decor.org/surveillance/disks/ contains disk-ok
	// https://repository.art-decor.org/stable3 contains <h2>stable3</h2>

	// presets
	$uptime = __NOTAVAILAPPIC;
	$existdbversion = __NOTAVAILAPPIC;
	$existdbbuild = __NOTAVAILAPPIC;
	$branch = __NOTAVAILAPPIC;
	$api = __NOTAVAILAPPIC;

	$info1 = _getSRVINFO($url, "/exist/apps/api/api");
	if ($info1['ok']) {
		// this is a proper exist db with ART-DECOR API
		$api = $info1['result']['version'];
		// investigate ART-DECOR server details
		$info2 = _getSRVINFO($url, "/exist/apps/api/server");
		if ($info2['ok']) {
			// var_dump($info2['database'][0]);exit;
			$uptime = $info2['result']['database'][0]['uptime'];
			$uptime = _uptime($uptime);
			$existdbversion = $info2['result']['database'][0]['version'];
			$existdbbuild = " build " . substr($info2['result']['database'][0]['build'], 0, 10);
			$branch = str_contains($api, 'beta') ? 'DEVELOPMENT' : 'MASTER' ;
		} else {
			$existdbversion = '<span style="color: #de3163">' . __NOTAVAILAPPIC . '</span>';
			$existdbbuild = "";
			$branch = '<span style="color: #de3163">' . __NOTAVAILAPPIC . '</span>';
			$errorseverity++;
		}
	} else {
		// not an ART-DECOR API, investigate for public repository
		$info1 = _getSRVINFO($url, "/stable3");
		// var_dump($info1);
		if ($info1['ok']) {
			$api = __PUBREPO;
			$existdbversion = __NOTAVAILAPPIC;
			$existdbbuild = "";
			$branch = "";
		} else {
			$api = '<span style="color: #de3163">HTTP ' . $info1['http'];
			$api .= '<br/>' . $info1['curl'] . '</span>';
			$errorseverity++;
		}
	}

	// investigate disk space if service is installed
	$info3 = _getSRVINFO($url, "/surveillance/disks/", false);
	if ($info3['ok']) {
		// var_dump($info3);exit;
		$da = explode('|', $info3['result']);
		$disks = $da[0] == 'disk-ok' ? '<span style="color: #2a804e">ok</span>' : '<span style="color: #de3163">issue</span>';
		$disks .= '&nbsp;(' . $da[4] . ' used of total ' . _format_bytes($da[1]) . ')';
	} else {
		$disks = __NOTAVAILAPPIC;
		// var_dump($info3);
		if ($info3['http'] != '404') $errorseverity++;
	}

	// get OS system
	$info4 = _getSRVINFO($url, "/surveillance/os/", false);
	if ($info4['ok']) {
		// var_dump($info4);exit;
		$os =  $info4['result'];
	} else {
		$os = '?';
	}

	// is AD2 still up?
	$info4 = _getSRVINFO($url, "/art-decor/home", false);
	if ($info4['ok']) {
		// var_dump($info4);exit;
		$tmp =  $info4['result'];
		$ad2 = strstr($tmp, 'Advanced Requirements Tooling') != NULL ? 'yes' : 'no';
	} else {
		$ad2 = 'no';
	}

	// get memory
	$info5 = _getSRVINFO($url, "/surveillance/mem/", false);
	if ($info5['ok']) {
		// var_dump($info5);exit;
		$ma = explode('|', $info5['result']);
		$mem = _format_bytes($ma[0] * 1024) . " total / " . _format_bytes($ma[1] * 1024) . " free<br/>";
		$mem .= $ma[4] == 'ok' ? '<span style="color: #2a804e">ok</span>' : '<span style="color: #ff9d00">' . $ma[4] . '</span>';
	} else {
		$mem = '?';
	}
	// get VUE APP version and build number in meta art-decor-appversion and art-decor-buildnumber
	$info6 = ($api === __PUBREPO) ? array() : extract_tags_from_url($url . "/ad/#/home");
	// var_dump($info6);
	$appversion  = isset($info6["art-decor-appversion"])  ? $info6["art-decor-appversion"]  : __NOTAVAILAPPIC;
	$buildnumber = isset($info6["art-decor-buildnumber"]) ? $info6["art-decor-buildnumber"] : __NOTAVAILAPPIC;

	// output the stats for this server green #27AE60 orange #ff9d00 red #de3163
	// assume green first
	$overallcolor = '#27AE60';
	$cardbackground = '#f4fbf5';
	if ($errorseverity > 0) {
		$overallcolor = '#ff9d00';
		$cardbackground = '#fff9f0';
	}
	if ($errorseverity > 1) {
		$overallcolor = '#de3163';
		$cardbackground = '#fcedf2';
	}

	$out .= '<div class="card" style="background-color: ' . $cardbackground . ' !important">';
	$out .= '<table><tr>';
	$out .= '<td><span class="dot" style="background-color: ' . $overallcolor . '"> </span></td>';
	$out .= '<td colspan="2"><b>' . $servername . '</b><br/>' . $url . '<br/>' . $desc . '</td>';
	$out .= "</tr>";

	$out .= "<tr><td></td><td><b>Linux</b></td><td>" . $os . "</td></tr>";
	$out .= "<tr><td></td><td><b>Database</b></td><td>" . $existdbversion . $existdbbuild . "</td></tr>";
	$out .= "<tr><td></td><td><b>  Uptime</b></td><td>" . "Up since " . $uptime . "</td></tr>";
	$out .= "<tr><td></td><td><b>Backend</b></td><td>" . $api . "</td></tr>";
	$out .= "<tr><td></td><td><b>Frontend</b></td><td>";
	if ($appversion != __NOTAVAILAPPIC) $out .= $appversion . " build " . $buildnumber . "<br/>";
	$out .= $branch . "</td></tr>";
	$out .= "<tr><td></td><td><b>Release 2</b></td><td>" . $ad2 . "</td></tr>";
	$out .= "<tr><td></td><td><b>Disks</b></td><td>" . $disks . "</td></tr>";
	$out .= "<tr><td></td><td><b>Memory</b></td><td>" . $mem . "</td></tr>";

	$out .= "</table></div>\n";

}

// coda
$out .= '</div></div><br/>';

// until here output is identical, now split for mail message (no expandables) and html file (with expandables)
$mailmsg = $out;

// eliminate the collapsiblecontent class
$mailmsg = str_replace("collapsiblecontent row", "row", $mailmsg);

// add javascript for the expandables
$out .= '<script>' . _getJS() . '</script>';
$out .= "</body></html>";
$mailmsg .= "</body></html>";

// first output...
$reportfile = "_reports/_" . $now . ".html";
file_put_contents($reportfile, $out);

// ...then email: send via email to back report ninjas (if desired)
if ($dosendmail === true) {
	$mail = new PHPMailer(true); // Passing `true` enables exceptions
	try {
    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;  //Enable verbose debug output
    $mail->isSMTP();  // Set mailer to use SMTP
    $mail->Host = $SMTPHOST;  // Specify main and backup SMTP servers
    $mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = false;
		$mail->SMTPAutoTLS = false;
		$mail->Username = $SMTPUSER; // SMTP username
		$mail->Password = $SMTPPASS;  // SMTP password

		// Sender
		$mail->setFrom('reply-not-possible@art-decor.email', 'ART-DECOR Notifier');
    $mail->addReplyTo('reply-not-possible@art-decor.email', 'ART-DECOR Notifier');

		//Recipients
		foreach ( $recipients as $rdkey => $remail) {
			$mail->addAddress($remail);     // Add a recipient
		}

    //Content
    $mail->isHTML(true);                          // Set email format to HTML
    $mail->Subject = "[art-decor] " . $headline;
		$mail->Body = wordwrap($mailmsg, 68);
    $mail->AltBody = 'There is no body in plain text for non-HTML mail clients, sorry!';
		$mail->send();
    echo "Message has been sent\n";

	} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
}

// bye bye
exit;



function _getSRVINFO ($url, $service, $json = TRUE) {
	// create new cURL-Handle
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . $service);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	$tmp = curl_exec($ch); // Execute the cURL statement
	$curlerr = curl_error($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch); // Close the cURL connection
	// var_dump($tmp);
	// raw or json array?
	$r = $json  ? json_decode($tmp, TRUE) : $tmp;
	$result = array (
		'ok' => $httpcode == '200' ? TRUE : FALSE,
		'curl' => $curlerr,
		'http' => $httpcode,
		'result' => $r
	);
	// var_dump($result);
	return $result;
}

function _uptime($str) {
	
	if (strlen($str) == 0) return __NOTAVAILAPPIC;

	// something like P18DT19H3M33.749S, needs to get rid of the fragment seconds first
	$str = substr($str, 0, strpos($str, '.'));

	if (strlen($str) == 0) return $str;

	$str .= "S";
    $duration = new DateInterval($str);
	// print_r( $duration ); exit;
	$ohm = "";
	$days_total = $duration->d;
    if ($days_total > 2) {
        $ohm = $days_total . ' days';
    } else {
        if ($days_total > 0) {
            $ohm .= sprintf("%dd %dh %dm", $days_total, $duration->h, $duration->i);
        } else if ($duration->h > 0) {
            $ohm .= sprintf("%dh %dm", $duration->h, $duration->i);
        } else {
            $ohm .= sprintf("%dm", $duration->i);
        }
    }
    return $ohm;
}

function _format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' kB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YB';
    }
}

/**
 * Extract metatags from a webpage
 */
function extract_tags_from_url($url) {
  $tags = array();

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

  $contents = curl_exec($ch);
  curl_close($ch);

  if (empty($contents)) {
    return $tags;
  }

  if (preg_match_all('/<meta([^>]+)content="([^>]+)>/', $contents, $matches)) {
    $doc = new DOMDocument();
    $doc->loadHTML('<?xml encoding="utf-8" ?>' . implode($matches[0]));
    $tags = array();
    foreach($doc->getElementsByTagName('meta') as $metaTag) {
      if($metaTag->getAttribute('name') != "") {
        $tags[$metaTag->getAttribute('name')] = $metaTag->getAttribute('content');
      }
      elseif ($metaTag->getAttribute('property') != "") {
        $tags[$metaTag->getAttribute('property')] = $metaTag->getAttribute('content');
      }
    }
  }

  return $tags;
}

function _getCSS() {
	return <<<END
* {
        font-family: Verdana, Arial, sans-serif;
        font-size: 9.0pt;
    }
    body {
        color: #1a202c;
        text-align: left;
        background-color: #e2e8f0;
    }
    .container {
        margin: 7px;
    }
    .main-body {
        padding: 10px;
    }
    h3 {
        font-size: 10.0pt;
    }
	.sdesc {
		margin: 10px;
	}
    .card-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: left;
        /* max-width: 1200px; */
    }

    .card {
        flex: 1 1 300px;
        display: flex;
        justify-content: left;
        align-items: center;
        padding: 20px;
        margin: 10px;
        border-radius: 10px;
        box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
        transition: transform 0.3s ease;
        color: #000;
        background-color: #fff9f0;
        max-width: 400px;
    }

    .newlabel {
        color: #bbbbbb;
    }
    
    .status-green {
        border-left: 7px solid #c9e9c9;
        border-top: 1px solid #c9e9c9;
        padding: 3px 0 0 15px;
    }
    .status-blue {
        border-left: 7px solid #D1DDFF;
        border-top: 1px solid #D1DDFF;
        padding: 3px 0 0 15px;
    }
    .status-red {
        border-left: 7px solid #fadbdb;
        border-top: 1px solid #fadbdb;
        padding: 3px 0 0 15px;
    }
    .status-orange {
        border-left: 7px solid #fffaf4;
        border-top: 1px solid #fffaf4;
        padding: 3px 0 0 15px;
    }
    .status-grey {
        border-left: 7px solid #eaeaea;
        border-top: 1px solid #eaeaea;
        padding: 3px 0 0 15px;
    }
    .tt {
        font-family: Monaco, Courier, monospace !important;
    }
	.dot {
		height: 20px;
		width: 20px;
		background-color: #bbb;
		border-radius: 50%;
		display: inline-block;
	}
	th {
		text-align: left;
	}
	td {
		text-align: left;
	}
	td.right {
		text-align: right;
	}
	td.center {
		text-align: center;
	}
	td, th {
		padding: 3px;
	}
	table, th, td {
		border-collapse: collapse;
		padding: 7px;
	}
	table.cellpadding {
		border-spacing: 10px !important;
	}
END;
}

function _getJS() {
	return <<<END
	var coll = document.getElementsByClassName("collapsible");
	var i;

	for (i = 0; i < coll.length; i++) {
	coll[i].addEventListener("click", function() {
		this.classList.toggle("active");
		var content = this.nextElementSibling;
		if (content.style.maxHeight){
			content.style.maxHeight = null;
		} else {
			content.style.maxHeight = content.scrollHeight + "px";
		} 
	});
	}
END;
}