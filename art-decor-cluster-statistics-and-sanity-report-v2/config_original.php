<?php
/*
	Config file for the ART-DECOR cluster statistics and sanity report
	
    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://art-decor.org/mediawiki/index.php?title=Copyright

*/

// servers under surveillance
$servers = array();
$servers[] = array (
    'name' => "NAME (IP)",
    'desc' => "Description",
    'baseurl' => 'https://the-real-url'
);

// only if $dosendmail is true mail is sent out to $recipients 
$dosendmail = true;
$SMTPHOST = "localhost";
$SMTPUSER = "...";
$SMTPPASS = "...";
$recipients = array(
	'x@y.de'
);
?>
