<?php

exec("hostnamectl", $tmp);
/* gives something like

Static hostname: h2990163.stratoserver.net
         Icon name: computer-desktop
           Chassis: desktop
        Machine ID: 417e6127812340169eba3f77ec8f6694
           Boot ID: c50331be8ce941cd9b773f755308515a
  Operating System: CentOS Linux 7 (Core)
       CPE OS Name: cpe:/o:centos:centos:7
            Kernel: Linux 3.10.0-1160.76.1.el7.x86_64
      Architecture: x86-64
*/
echo trim(strstr($tmp[5], ': '), ': ') . "\n";

?>