<?php

exec('cat /proc/meminfo', $memInfo);
// var_dump($memInfo);

$DESIREDFREEMEM = 20;
$DESIREDFREESWAP = 50;

$totalMemory = preg_split("/[\s]+/", $memInfo[0])[1];
$freeMemory = preg_split("/[\s]+/", $memInfo[1])[1];
$swapTotalMemory = preg_split("/[\s]+/", $memInfo[14])[1];
$swapFreeMemory = preg_split("/[\s]+/", $memInfo[15])[1];

$r = "ok";
if (($totalMemory / 100.0) * $DESIREDFREEMEM > $freeMemory) {
  if (($swapTotalMemory / 100.0) * $DESIREDFREESWAP > $swapFreeMemory) {
     $r = "Less than $DESIREDFREEMEM% free memory and less than $DESIREDFREESWAP% free swap space";
  }
  $r = "Less than $DESIREDFREEMEM% free memory";
}

echo $totalMemory . "|" .  $freeMemory . "|" . $swapTotalMemory . "|" . $swapFreeMemory . "|" . $r . "\n";

?>