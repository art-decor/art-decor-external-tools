#!/bin/bash

export SERVER=`hostname`
export URLPREFIX=http://localhost:8877/exist
#export URLPREFIX=http://art-decor.org
#export URLPREFIX=http://build.art-decor.org:8877/exist


export TIMEFORMAT="<real>%R</real><user>%U</user><system>%S</system>"

function docurl {
    # $1: the url $2: repetion count
    echo $2: $1 >&2 # Show progress
    (time curl --silent --output /dev/null "$1") 2>&1
}

function threetimes {
    echo "<test id=\"$3\">"
    echo "<url>$(echo $2 | sed -e 's/&/&amp;/g')</url>"
    for n in 1 2 3
    do
       echo "<request n=\"$n\">"
       echo "<times>"
       "$@" $n
       echo "</times>"
       echo "</request>"
    done
    echo '</test>'
}

function doTests {
    
    # the performance test have a URL to the demo 9 project (peformance test project)
    # to be directly called and an id for identification of the test
    # id component 1 = R read, W write
    # id component 2 = artefact code, PR project, TM template, VS value set etc.
    # id component 3 = size indication, e.g. VS-200 means a value set with 200 coded concepts
    # id component 4 = format, H HTML or if omitted raw XML

    threetimes docurl $URLPREFIX'/decor/services/RetrieveProject?prefix=demo9-&format=xml&mode=verbatim' R-PR-V
    threetimes docurl $URLPREFIX'/decor/services/RetrieveProject?prefix=demo9-&format=xml&mode=compiled' R-PR-C
    
    threetimes docurl $URLPREFIX'/decor/services/RetrieveValueSet?id=2.16.840.1.113883.3.1937.99.60.9.11.2&prefix=demo9-&format=xml' R-VS-2
    threetimes docurl $URLPREFIX'/decor/services/RetrieveValueSet?id=2.16.840.1.113883.3.1937.99.60.9.11.3&prefix=demo9-&format=xml' R-VS-6
    threetimes docurl $URLPREFIX'/decor/services/RetrieveValueSet?id=2.16.840.1.113883.3.1937.99.60.9.11.1&prefix=demo9-&format=xml' R-VS-200
    threetimes docurl $URLPREFIX'/decor/services/RetrieveValueSet?id=2.16.840.1.113883.3.1937.99.60.9.11.4&prefix=demo9-&format=xml' R-VS-6000
    threetimes docurl $URLPREFIX'/decor/services/RetrieveValueSet?id=2.16.840.1.113883.3.1937.99.60.9.11.5&prefix=demo9-&format=xml' R-VS-21800

    threetimes docurl $URLPREFIX'/decor/services/RetrieveTemplate?id=2.16.840.1.113883.3.1937.99.60.9.10.3001&prefix=demo9-&format=xml' R-TM-L

    threetimes docurl $URLPREFIX'/decor/services/RetrieveValueSet?id=2.16.840.1.113883.3.1937.99.60.9.11.1&prefix=demo9-&format=html' R-VS-200-H
    threetimes docurl $URLPREFIX'/decor/services/RetrieveTemplate?id=2.16.840.1.113883.3.1937.99.60.9.10.3001&prefix=demo9-&format=html' R-TM-H

}

echo '<?xml version="1.0"?>'
echo "<tests server=\"$SERVER\" prefix=\"$URLPREFIX\">"
#doTests
echo '</tests>'
