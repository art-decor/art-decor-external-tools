#!/bin/bash
# simple backend testscript for ART-DECOR: prepare eXist-db server
# Start with an empty installation
# End result should be an installed eXist-db with API/ART/Examples/Bots

# USAGE
# Make the script executable: chmod +x ./<script>
# Run this script with: ./<script>

# VERSION
# 2024 10 16: initial version

# VARIABLES
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# xst secret
XST_SECRET=./secret/.xstrc

echo ${SCRIPT}: Prepare database: start from new installed database, and install ART-DECOR-API and other packages. Also sets up bot users. Date: `date`

# NB: this assumes that you run this scripts on the local server where eXist-db is also installed

# Restore from previous backup
# We start from a freshly installed eXist-db. Another route is to run ./install_exist.sh. By restoring from this RestorePath, we skip that step
./exist_restore_stop_backup.sh --yes --RestorePath /usr/local/exist_atp//data_backup_202405241029_empty

# wait to complete
wait

# Adjust the repository setting to ART-DECOR repository
xst --config ${XST_SECRET} up ./upload/apps/packageservice/configuration.xml /apps/packageservice/

# Install ART-DECOR-API and depedencies
xst --config ${XST_SECRET} package install registry "http://art-decor.org/ns/api" --registry "https://repository.art-decor.org/exist/apps/development3/"

# Install DECOR-examples
xst --config ${XST_SECRET} package install registry "http://art-decor.org/ns/examples" --registry "https://repository.art-decor.org/exist/apps/development3/"

# Install ADA
# We need, or needed ADA as a dependency from API? But it does not install through API?
xst --config ${XST_SECRET} package install registry "http://art-decor.org/ns/ada" --registry "https://repository.art-decor.org/exist/apps/development3/"

# List which packages are installed?
# xst --config ${XST_SECRET} package list

# install bot users to test with
xst --config ${XST_SECRET} execute --file ./xquery/demo_testauthor.xq

# Create a new database snapshot
echo y | /opt/art-decor-linux/tooling/scripts/exist_stop_backup.sh -b API_ART_ADA_EX_bots

echo ${SCRIPT}: Done: Prepare database. Finished at date: `date`
# EOF

