#  ART-DECOR(R) backend tester

:warning:
This script tries to break data or your eXist-db database consistency.  
Run a backup before testing.

# USAGE
Make the script executable: chmod +x ./<script>  
Place your variables in settings_tests.txt  
Run this script with: ./<script>  

# INFO
The script will a multiple calls to the ART-DECOR backend API.  
The idea is that this script will fake a user doing multiple patch calls to the backend, and that the check on index might start to fail at one stage

# Introduction
A couple things have to be setup first.  
Once these are done, we can run automated tests with different payloads.

# One-time setup

# Install Ant
ant -version  
sudo yum install epel-release  
sudo yum install snapd  
sudo systemctl enable --now snapd.socket  
sudo ln -s /var/lib/snapd/snap /snap  
sudo snap install ant --classic  
exit  
Log back in as root user  
snap install ant --classic  
ant -version  

# Setup files and scripts from repo
```
sudo su -l root  
cd /opt/  
git clone https://bitbucket.org/art-decor/art-decor-external-tools.git  
cd /opt/art-decor-external-tools/  
git pull  
  
cd /opt/art-decor-linux/tooling/scripts/  
cp /opt/art-decor-external-tools/art-decor-exist-db-installer/scripts/exist_stop_backup.sh .  
Make all scripts executable  
chmod +x *.sh  
  
mkdir /opt/art-decor-backend-tests-bash-local/  
cd /opt/art-decor-backend-tests-bash-local/  
cp -R /opt/art-decor-external-tools/art-decor-backend-tests-bash/* .  
Make all scripts executable  
chmod +x *.sh  
```
  
# Make sure your database is properly installed  
Point your package manager installation source to:
/db/apps/packageservice/configuration.xml
```
<settings>
    <repositories>
        <!--<repository active="true" default="true">https://exist-db.org/exist/apps/public-repo</repository>-->
        <repository active="true" default="true">https://repository.art-decor.org/stable3</repository>
```

Install the following packages with the package manager:  
API, decor examples, ADA  

# Setup users  
Manually create a dba user to create testbot users with.  
  
cd /opt/art-decor-backend-tests-bash-local/xquery  
set username and password for that dba user in local.properties  
  
Run manually to create test users:  
ant -buildfile ./ant-run-xquery.xml -DsourceFile=demo_testauthor.xq all  
This creates: user: testbackendbot1 (/2 /3)  
And adds these authors to all demo examples demo1- etc projects.  
Payload comes from: demo_testauthor.xq 

# Create a backup of your database
Create copy of stopped database, start eXist-db:  
/opt/art-decor-linux/tooling/scripts/exist_stop_backup.sh  
Store the name of the backup, manually to:  
art-decor-backend-tests-bash/settings_exist_restore_stop_backup  
# Path to restore data from  
Example entry:  
RestorePath=/usr/local/exist_atp/data_backup_202403191458  
These settings are used by: exist_restore_stop_backup.sh

# Get your token for a bot user
cd art-decor-backend-tests-bash/get-token  
set the user, password and server in: settings_user.txt (or create multiple files)  
The script will later run: ./backend_test_get_token.sh  
Optionally you can configure other users: Run for another user: ./backend_test_get_token.sh -f settings_user2.txt  
The script will place the token from the response in: art-decor-backend-tests-bash/settings_tests_generic.txt  
Manually: set the server in art-decor-backend-tests-bash/settings_tests_generic.txt  

# Define your tests
Define/adapt your tests in art-decor-backend-tests-bash/settings_tests.txt  
You can create multiple definition files, and run them simulaniously  
Name each test definition in backend_all_tests.sh, like: ./backend_test_index.sh -f settings_tests_demo05.txt &  

# Try it manually
Run your test, and generate report:  
./backend_test_index.sh -f ./settings_tests.txt  
Add --Debug to see the output and run just one call.  

# Automated usage

Usage: To run automated, the user should:  
start a screen session, because your SSH-session might get interrupted but this way the tests continue  
screen -r -a -d mine || screen -S mine

log in as root  
sudo su -l root  
cd /opt/art-decor-backend-tests-bash-local

mkdir report
./report_backend_all_tests.sh  
End of user actions  

# Info on usage
What the script does:  
- report_backend_all_tests.sh calls ./backend_all_tests.sh and saves a report  
- backend_all_tests.sh calls  
    - ./exist_restore_stop_backup.sh: restores your database from the RestorePath you defined  
    - ./get-token/backend_test_get_token.sh: saves a token to settings_tests_generic.txt  
    - ./backend_test_index.sh: run tests.  
     Uses: settings_tests_generic.txt: token and server    
	 Uses: settings_tests.txt: test definition file(s)  
	 
# Reports
Reports are saved in ./report  
These mainly include ./report_<datetime>.txt with output from tests  
Also a dump is made here, per testrun, of all eXist-db logfiles.  
