#!/bin/bash
# simple backend testscript for ART-DECOR: fire all tests and report
# USAGE
# Make the script executable: chmod +x ./<script>
# Place your variables in settings_tests.txt
# Run this script with: ./<script>

# INFO
# The script will a multiple calls to the ART-DECOR backend API
# Make sure that the token/user can edit in the project
# The idea is that this script will fake a user doing multiple patch calls to the backend, and that the check on index might start to fail at one stage

# VERSION
# 2024 03 26: initial version

ReportFolderDate=$(date '+%Y%m%d%H%M')
ReportFolder=./report/report_${ReportFolderDate}
mkdir ${ReportFolder}
echo Writing report to ${ReportFolder}
./backend_all_tests.sh > ${ReportFolder}/report_${ReportFolderDate}.txt
tar zcpvf ${ReportFolder}/${ReportFolderDate}_exist_log.tgz /usr/local/exist_atp/logs/*

