#!/bin/bash
# simple backend testscript for ART-DECOR to fetch a token
# USAGE
# Make the script executable: chmod +x ./<script>
# Place your variables in settings_user.txt
# ! Note that these contain your testuser password, don't share
# Run this script with: ./<script>

# VERSION
# 2024 03 14: initial version
# 2024 04 03: store token

# Variables that we need to set in the script
# What is the scriptname
SCRIPT=$(readlink -f "$0")
# get the directory where this script is located
SCRIPT_DIR=$(dirname $(readlink -f $0))
# What is the filename for the settings file. We set a default, but can override this
SettingsFileName=./settings_user.txt
# Trigger debug to give more output
Debug=false
# Token settingsfile: where to store the token. Relative to current script
SettingsFileToken=../settings_tests_generic.txt

# sanity check
function file_present ()
{
   file=$1
   # function to check if a file is present or exit
   # test if file is present and readable
   [ -f "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT FOUND: ${1}" ; usage ;}
   [ -r "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT READABLE: ${1}" ; usage ;}
}

function variable_set ()
{
    # options
    #  --DONT_DISPLAY: do not display contents of variable if set, for passwords, etc

    # process arguments
    case $2 in
        --DONT_DISPLAY )          local DONT_DISPLAY=1
                                  ;;
    esac

    # check whether variable is set
    if [[ ! -v $1 ]];
        then
           echo $0 ERROR: $1 is not set!
           echo "Either put it in the file '${SettingsFileName}' or see --help"
           echo ""
           usage
        else
            if [[ ${DONT_DISPLAY} ]]
                then
                    echo "    ${1} = <not showing>"
                else
                    echo "    ${1} = ${!1}"
            fi
    fi
}
# end of function variable_set

# Main functions

function check ()
{
# Check if the index for the project is there
curlOutputRaw=$(curl -X 'POST' \
  ${Path} \
  -H 'Content-Type: application/json' \
  -H 'accept: application/json' \
  -d '{"username": "'${Username}'","password": "'${Password}'"}')

  # Find the token in the curl output, store as variable
  curlOutputToken=`echo ${curlOutputRaw} | sed -n 's/.*"token" : "\(.*\)/\1/p'|sed 's/", .*//g'`
  # Replace in the tokenfile, line Token= with Token="X-Auth-Token: followed by curlOutputToken
  sed -i 's/Token=.*/Token=\"X-Auth-Token: '"${curlOutputToken}"'\"/' ${SettingsFileToken}
}

function usage ()
{
        echo "Usage: $0 "
        echo "
             -f | --SettingsFileName Which settings file to use
             -d | --Debug )          Debug mode on. Just one run and show curl output
             -h | --help )           Display this usage function

        Please note that you have to set variables in the file '${SettingsFileName}'.
                Run the script without any parameters to report missings variables.
        "
        exit 1
# end of function usage
}
# Main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -f | --SettingsFileName ) shift
                                  SettingsFileName=$1
                                  ;;
        -d | --Debug )            Debug=true
                                  ;;
        -h | --help )             usage
                                  ;;
        * )                       usage
    esac
    shift
  done

# change directory to current script
cd ${SCRIPT_DIR}

# Get other common parameters from settings script
. ${SettingsFileName}
## Example values for variables:
## username
## password
## server
## path

# Sanity check
file_present ${SettingsFileName}
file_present ${SettingsFileToken}
echo $SCRIPT: Running this script with the following settings:
variable_set SettingsFileName
variable_set SettingsFileToken
variable_set Username
variable_set Password --DONT_DISPLAY
variable_set Server
variable_set Path

# Debug
if [ ${Debug} = true ]; then
   echo TODO DEBUG TRUE
   # CurlSilent="-v"
     # For debug, just do one run and exit
     check
     echo INFO: curlOutputRaw is ${curlOutputRaw}
     echo INFO: curlOutputToken is ${curlOutputToken}
     # Display contents of the tokenfile, after edit
     echo cat $SettingsFileToken
     cat $SettingsFileToken
   else
     check
fi

echo INFO: Done. Stored the token in ${SettingsFileToken}

# EOF
