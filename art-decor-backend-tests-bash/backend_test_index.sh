#!/bin/bash
# simple backend testscript for ART-DECOR
# USAGE
# Make the script executable: chmod +x ./<script>
# Place your variables in settings_tests.txt
# Run this script with: ./<script>

# INFO
# The script will a multiple calls to the ART-DECOR backend API
# After each call it will fetch scheduled tasks from the backend API, since this will fail if the index got lost in eXist-db
# Make sure that the token/user can edit in the project
# The idea is that this script will fake a user doing multiple patch calls to the backend, and that the check on index might start to fail at one stage

# VERSION
# 2024 03 14: initial version
# 2024 03 15: added debug mode
# 2024 03 19: fixed path to where we check the index
# 2024 03 26: improved reporting. Payload and method are now moved to settings

# Variables that we need to set in the script
# What is the scriptname
SCRIPT=$(readlink -f "$0")
# What is the filename for the settings file. We set a default, but can override this
SettingsFileName=./settings_tests.txt
# Get generic settings: Token and Server
SettingsGenericFileName=./settings_tests_generic.txt
# Trigger debug to give more output
Debug=false

# sanity check
function file_present ()
{
   file=$1
   # function to check if a file is present or exit
   # test if file is present and readable
   [ -f "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT FOUND: ${1}" ; usage ;}
   [ -r "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT READABLE: ${1}" ; usage ;}
}

function variable_set ()
{
    # options
    #  --DONT_DISPLAY: do not display contents of variable if set, for passwords, etc

    # process arguments
    case $2 in
        --DONT_DISPLAY )          local DONT_DISPLAY=1
                                  ;;
    esac

    # check whether variable is set
    if [[ ! -v $1 ]];
        then
           echo $0 ERROR: $1 is not set!
           echo "Either put it in the file '${SettingsFileName}' or see --help"
           echo ""
           usage
        else
            if [[ ${DONT_DISPLAY} ]]
                then
                    echo "    ${1} = <not showing>"
                else
                    echo "    ${1} = ${!1}"
            fi
    fi
}
# end of function variable_set

# Main functions

function edit1 ()
{
# PATCH
# we need to escape the " into \"
PathPayload=`echo $PathPayload | sed 's/"/\\"/g'`
curl -X ${PathMethod} \
  ${Path} \
  ${CurlSilent} \
  --write-out "%{http_code}\n" \
  -H 'accept: application/json' \
  -H "${Token}" \
  -H 'Content-Type: application/json' \
  -d "${PathPayload}"
}

function check ()
{
# Check if the index for the project is there
curl -X 'GET' \
  ${PathCheck} \
  ${CurlSilent} \
  --write-out "%{http_code}\n" \
  -H 'accept: application/json'
}

function usage ()
{
        echo "Usage: $0 "
        echo "
             -t | --SettingsGenericFileName Token and Server
             -f | --SettingsFileName        Which settings file to use
             -d | --Debug )                 Debug mode on. Just one run and show curl output
             -h | --help )                  Display this usage function

        Please note that you have to set variables in the files '${SettingsGenericFileName}' and '${SettingsFileName}'.
                Run the script without any parameters to report missings variables.
        "
        exit 1
# end of function usage
}
# Main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -f | --SettingsFileName ) shift
                                  SettingsFileName=$1
                                  ;;
        -t | --SettingsGenericFileName ) shift
                                  SettingsGenericFileName=$1
                                  ;;
        -d | --Debug )            Debug=true
                                  ;;
        -h | --help )             usage
                                  ;;
        * )                       usage
    esac
    shift
  done


# Sanity check
echo $SCRIPT: Running this script with the following settings:
variable_set SettingsFileName
variable_set SettingsGenericFileName

# Get generic parameters from settings script
file_present ${SettingsGenericFileName}
. ${SettingsGenericFileName}
# Get other common parameters from settings script
file_present ${SettingsFileName}
. ${SettingsFileName}

variable_set Token --DONT_DISPLAY
variable_set server
variable_set ItemId
variable_set ItemEffectiveDate
variable_set ProjectPrefix
variable_set Path
variable_set PathPayload
variable_set PathMethod
variable_set PathCheck
variable_set SleepTime
variable_set NumberRuns
variable_set Debug

# Debug
if [ ${Debug} = true ]; then
   CurlSilent="-v"
     # For debug, just do one run and exit
     edit1
     check
     exit
   else
     CurlSilent="--silent --output /dev/null"
fi

## Did we encounter problems in edit?
DidScriptFailEdit=false
## Did we encounter problems in check?
DidScriptFailCheck=false
CurrentRun=0
while [ "$CurrentRun" -lt ${NumberRuns} ]; do {
  CurrentRun=$(( CurrentRun + 1 ))
  # disabled stop on fail
  # if [ ${DidScriptFailCheck} = false ]; then
    echo Current run is: ${CurrentRun} for ${SettingsFileName}
    {
        status=$(edit1)
        echo $status
        if [[ ${status} -eq 200 ]]; then
           echo $(date) Succes edit1: ${status} on Path ${Path}
           else {
             echo $(date) FAIL edit1: ${status} on Path ${Path}
                   DidScriptFailEdit=true
                   # Only set this number on first fail
                   if [ ! ${DidScriptFailEditRun} ]; then
                      DidScriptFailEditRun=$CurrentRun
                   fi
                }
        fi

        status=$(check)
        echo $status
        if [[ ${status} -eq 200 ]]; then
           echo $(date) Succes index: ${status} on PathCheck ${PathCheck}
           else {
             echo $(date) FAIL index: ${status} on PathCheck ${PathCheck}
                   DidScriptFailCheck=true
                   # Only set this number on first fail
                   if [ ! ${DidScriptFailCheckRun} ]; then
                     DidScriptFailCheckRun=$CurrentRun
                   fi
                }
        fi

    sleep ${SleepTime}
  }
  # disabled stop on fail
  # fi
}
done

echo ""
echo Summary:
echo Ran this number of edits: ${CurrentRun} out of planned ${NumberRuns} NumberRuns on this Path:
echo $Path
if [ ${DidScriptFailEdit} = false ]; then
   echo Succes on all edits. No problems encountered while editing.
   else
     echo FAIL: on at least one edit. Started failing at ${DidScriptFailEditRun}
fi
if [ ${DidScriptFailCheck} = false ]; then
   echo Succes on all checks on the index. No problems encountered.
   else
     echo FAIL: on at least one project index check. Started failing at ${DidScriptFailCheckRun}
fi
echo Script finished at `date`

# EOF
