xquery version "3.1";
(: Testscript to fake the actions that this API code normally does: 
 :   /db/apps/api/modules/concept-api.xqm
 :     declare function deapi:patchConcept
 :)

(: Fetch data from the project :)
let $colDecorData   := collection(repo:get-root() || 'decor/data')

(: http://localhost:8877/exist/apps/api/concept/2.16.840.1.113883.3.1937.99.62.3.2.14/2012-05-30T14%3A18%3A23 :)
(: Select concept :)
let $storedConcept        := $colDecorData//concept[@id = '2.16.840.1.113883.3.1937.99.62.3.2.14'][@effectiveDate = '2012-05-30T14:18:23']

(: Add an attribute versionLabel to concept with contents 'test' :) 
let $preparedConcept      :=
    <concept id="2.16.840.1.113883.3.1937.99.62.3.2.14"
        effectiveDate="2012-05-30T14:18:23" statusCode="draft" type="item"
        versionLabel="test" lastModifiedDate="2024-04-02T14:48:52">
        <name language="en-US" lastTranslated="2024-04-02T14:48:52">Number of children</name>
        <name language="nl-NL" lastTranslated="2024-04-02T14:48:52">Aantal kinderen</name>
        <desc language="en-US" lastTranslated="2024-04-02T14:48:52">Number of children</desc>
        <desc language="nl-NL" lastTranslated="2024-04-02T14:48:52">Aantal kinderen</desc>
        <valueDomain type="count">
            <example>2</example>
        </valueDomain>
    </concept>

(: Store to database :)
let $update              := update replace $storedConcept with $preparedConcept

return ()