xquery version "3.1";
let $usernames         := ('testbackendbot1', 'testbackendbot2', 'testbackendbot3')
for $username in $usernames

let $update-user      :=
if (sm:user-exists($username)) then () else (
sm:create-account($username, $username, 'decor', ('ada-user', 'decor', 'decor-admin', 'editor', 'issues'), $username, $username)
    )

let $update-projects  :=
for $project in collection('/db/apps/decor/data/examples')/decor/project
let $author     :=
<author id="{max($project/author/@id) + 1}" username="{$username}" effectiveDate="{substring(string(current-dateTime()), 1, 19)}">{$username}</author>
return
if ($project/author[@username = $username]) then () else (
update insert $author following $project/author[last()]
        )

return ()
