#!/bin/bash
# simple backend testscript for ART-DECOR: fire all tests
# USAGE
# Make the script executable: chmod +x ./<script>
# Run this script with: ./<script>

# VERSION
# 2024 03 19: initial version
# 2024 03 26: moved usage to readme
# 2024 04 03: use get-token to fetch a token

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

echo ${SCRIPT}: Running all tests on date: `date`

# NB: this assumes that you run this scripts on the local server where eXist-db is also installed
# Restore from previous backup
./exist_restore_stop_backup.sh --yes

# Get your token for a bot user. Tokens time out after a number of hours, so we need a new one
./get-token/backend_test_get_token.sh -f settings_user_testbackendbot1.txt

# Run all curl tests
# ./backend_test_index.sh -f settings_tests_demo1.txt &
# ./backend_test_index.sh -f settings_tests_demo5.txt &

# SD CP Name change
# ./backend_test_index.sh -f settings_tests_demo1_DSCPname.txt &
# ./backend_test_index.sh -f settings_tests_demo5_DSCPname.txt &

# SD CP status change
# ./backend_test_index.sh -f settings_tests_demo1_DSCPstatus.txt &
# ./backend_test_index.sh -f settings_tests_demo5_DSCPstatus.txt &

# Run all xquery tests
./backend_test_index_xquery.sh -f settings_tests_demo1_xquery.txt

# wait for tests to complete
wait

echo ${SCRIPT}: Finished all tests on date: `date`

# EOF

