#!/bin/bash
# script to stop exist-db, restore copy or data directory, start exist-db again

# version
# 2024 03 21: initial version
# 2024 03 26: added RestorePath
# 2024 03 29: improved the mv for restoring /export
# 2024 10 16: added yes option to usage

# What is the filename for the settings file. We set a default, but can override this
SettingsFileName=settings_exist_restore_stop_backup

# Variables that we need to set in the script
# Whether to force a yes and always run the script. For automation purposes
ForceYes=0
# What is the scriptname
SCRIPT=$(readlink -f "$0")
# what is the eXist-db data directory. We will restore to this folder
OrgFolder=/usr/local/exist_atp/data
# export-temp folder
# this usually would be someting like /usr/local/exist_atp/data_export_temp
ExportTemp=${OrgFolder}/../data_export_temp
# Which folder to get data from that we will restore
  # From the settingsfile, RestorePath

function ask_user_abort () {
   echo $1
   echo Do you want to continue anyway?
   read -p "Continue (y/n)? " choice
   case "$choice" in
     y|Y ) echo Continuing;;
     n|N ) abort_script;;
     * ) abort_script;;
   esac
}

function abort_script ()
{
    echo Cancelling script
    exit 0

# end of function abort_script
}

function file_present ()
{
   file=$1
   # function to check if a file is present or exit
   # test if file is present and readable
   [ -f "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT FOUND: ${1}" ; usage && exit ;}
   [ -r "${1}" ] || { echo "$0 ERROR: ${2} FILE NOT READABLE: ${1}" ; usage && exit ;}
}

check_variable_has_content ()
{
        # check whether variable has content
        # echo variable to check = $1
        if [ "$1" != "" ]; then
                        # if variable has content return 1
                        return 1
                else
                        return 0
        fi

# end of function check_variable_has_content
}

function sanity ()
{
        check_variable_has_content ${OrgFolder} && echo "ERROR: OrgFolder is not set." && exit
        check_variable_has_content ${RestorePath} && echo "ERROR: RestorePath is not set." && exit

        [ ! -d ${OrgFolder} ] && echo "ERROR: OrgFolder ${OrgFolder} DOES NOT exist." && exit
        [ ! -d ${RestorePath} ] && echo "ERROR: RestorePath folder ${RestorePath} DOES NOT exist." && exit
}

function sanity_files_in_folder ()
{
        # If the folder is empty, we should not run a restore from it.
        if [ "$(ls -A $RestorePath)" ]; then
          return 0
        else
          return 1
        fi
}

function usage ()
{
        echo "Usage: $0"
        echo "
        This script will restore data from a backup to the eXist-db data folder.
        In the settings file: ${SettingsFileName}, place the path to the backup folder in RestorePath
        Then just run the script without any arguments.

        -y | --yes )            Assume yes for automated runs.
        -r | --RestorePath )    Option to provide a RestorePath to restore from.
                                This will override the settings file.
                                Example: --RestorePath /usr/local/exist_atp/data_backup_202403261717_firstTry
        -h | --help )           Display usage

        CAUTION: this script will DELETE your current eXist-db ./data/ and overwrite that with saved restore data.
        The script will first stop eXist-db and then handle data.
        Take care with running this on production servers. This is for testing purposes only!
        Before running this script you should have data to restore in ${RestorePath}.
        Note that the script should not delete ./data/exports (but in production setting you should backup these first).
        "
# end of function usage
}

# main logic

# Sanity check
echo ${SCRIPT}: Running this script with the following settings:
check_variable_has_content SettingsFileName
echo SettingsFileName is set to: ${SettingsFileName}
# Get common parameters from settings script
file_present ${SettingsFileName}
. ${SettingsFileName}

# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -y | --yes )            shift
                                ForceYes=1
                                ;;
        -p | --RestorePath )    shift
                                RestorePath=$1
                                ;;
        -h | --help )           usage
		                        exit 1
                                ;;
        * )                     echo "Unknown option $1"
                                usage
                                exit 1
                                ;;
    esac
    shift
  done

# Run function sanity
sanity

# TODO
# Add check to see if these is data inside of RestorePath.
# sanity_files_in_folder ${RestorePath} || echo "ERROR $RestorePath does not contain files"
# if [ sanity_files_in_folder ]; then
#     echo "ERROR: ${RestorePath} is empty (or does not exist or is a file)"
#     echo ""
#     usage
#     exit 1
# fi

usage
echo "CAUTION: the script will DELETE all data in folder: ${OrgFolder}"
echo "Then it will restore data from folder: ${RestorePath}"

# force an update if parameter is 1
if [[ "${ForceYes}" == "1" ]]; then
    echo; echo ${SCRIPT}: ForceYes was set to yes, continuing ..
else
    ask_user_abort
fi

# Check current status of eXist-db
echo systemctl status eXist-db.service:
systemctl status eXist-db.service | head -n 3
# Stop eXist-db
echo Stopping eXist-db ...
echo systemctl stop eXist-db.service
systemctl stop eXist-db.service | head -n 3
# wait till last command is executed
wait
# sleep for number of seconds
sleep 8
# Check current status of eXist-db
echo systemctl status eXist-db.service:
systemctl status eXist-db.service | head -n 3

# Restore backup to the data directory
echo Removing all data from ${OrgFolder} ...

shopt -s nullglob
for f in "${OrgFolder[@]-}"
do
  # do sanity checks on OrgFolder
  [[ $f ]] || (echo "ERROR: OrgFolder is not set." && exit)
  candidate="$(realpath -e -s "$f")" || (echo "ERROR: OrgFolder is not correctly set." && exit)
  # Check that OrgFolder is not set to / or ./* something scary.
  # CAUTION: Note that this check is not foolproof. At least it does not yet properly prevent against /
    # Is directory
  [[ -d $candidate ]] || (echo "ERROR: OrgFolder is set to ${OrgFolder} but expected a directory." && exit)
    # Not root
  [[ $candidate != "/" ]] || (echo "ERROR: OrgFolder is set to ${OrgFolder} and should not be set to /" && exit)

  # First temporarily move the exports outside of the data folder
  mkdir ${ExportTemp}
  echo mv ${OrgFolder}/export/* ${ExportTemp}/
  mv ${OrgFolder}/export/* ${ExportTemp}/

  echo rm -rf "$candidate"
  # CAUTION: we have done checks earlier, but rm is always tricky
  rm -rf "$candidate"

done

echo Restoring ${RestorePath} to ${OrgFolder} ...
mkdir ${OrgFolder}
cp -rp ${RestorePath}/* ${OrgFolder}/
# We can now place back the exports into the data folder
mkdir ${OrgFolder}/export/
echo Move files from ${ExportTemp} to ${OrgFolder}/export/
mv ${ExportTemp}/* ${OrgFolder}/export/
# Set permissions
chown -R existdb:existdb ${OrgFolder}
wait

# Start eXist-db
echo Starting eXist-db ...
echo systemctl start eXist-db.service | head -n 3
systemctl start eXist-db.service
# wait till last command is executed
wait
# sleep for number of seconds
sleep 8
# Check current status of eXist-db
echo systemctl status eXist-db.service
systemctl status eXist-db.service | head -n 3

# Report
echo Done. eXist-db started. Backup from ${RestorePath} is restored to ${OrgFolder}

# EOF
