#!/bin/bash
# perform all pacthing actions of the .env file that should be done only
# for this server and that might override git repo code, derive from .env.example

# Version
# 20221023: initial version
# 20240906: AD30-1694 HTTP Proxy installation procedure change for patching env

# .env variables and description see https://bitbucket.org/art-decor/art-decor-http-proxy/src/main/.env.example

# first copy the .env.example file into .env
FILE=.env
cp -pr .env.example $FILE

# no additional changes here needed
