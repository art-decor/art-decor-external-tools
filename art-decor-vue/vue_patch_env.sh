#!/bin/bash
# perform all pacthing actions of the .env file that should be done only
# for this server and that might override git repo code, derive from .env.example

# Version:
# 20231213: initial version
# 20240906: AD30-1693 Vue installation procedure change for patching env

# .env variables and description see https://bitbucket.org/art-decor/art-decor/src/development/.env.example

# first copy the .env.example file into .env
FILE=.env
cp -pr .env.example $FILE

# no design menu
sed -i 's/VUE_APP_FEATURE_SHOW_DESIGN_MENU_ITEMS=.*/VUE_APP_FEATURE_SHOW_DESIGN_MENU_ITEMS=false/g' $FILE

# no only terminology
sed -i 's/VUE_APP_FEATURE_TESTING_TERMINOLOGY_ONLY=.*/VUE_APP_FEATURE_TESTING_TERMINOLOGY_ONLY=false/g' $FILE

# no development features
sed -i 's/VUE_APP_FEATURE_SHOW_DEVELOPMENT_FEATURES=.*/VUE_APP_FEATURE_SHOW_DEVELOPMENT_FEATURES=false/g' $FILE

# websocket override
sed -i 's/VUE_APP_SOCKET_URL=.*/# VUE_APP_SOCKET_URL=unused/g' $FILE

