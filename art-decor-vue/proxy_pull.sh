#!/bin/bash
# all actions that should be done for pulling new ART-DECOR http proxy sources and deploy

# Version
# 20221023: initial version
# 20240906: AD30-1694 HTTP Proxy installation procedure change for patching env

# get the directory where this script is located
SCRIPT_DIR=$(dirname $(readlink -f $0))
# change directory to directory where this script is located
cd ${SCRIPT_DIR}

# set defaults
GIT_REMOTE_BRANCH=main

# get common parameters from settings script
. ./settings_proxy
# provides input for the following variables:
# * git local working copy :: GIT_LOCAL_WORKING_COPY_DIR
# * link to remote git for artdecor :: REMOTE_REPO
# * which branch to checkout :: GIT_REMOTE_BRANCH

function sanity_check ()
{
       # sanity check
       # these parameters needs to be set:

       echo $0 Running this script with the following settings:
       for var in GIT_LOCAL_WORKING_COPY_DIR SCRIPT_DIR; do
         if [ -z "${!var}" ]
           then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings'"
         else echo "    ${var}=${!var}"
         fi
       done
}

function setup_local_working_copy ()
{
        # create directory for local repo
        mkdir -p ${GIT_LOCAL_WORKING_COPY_DIR}
        # cd to directory
        cd ${GIT_LOCAL_WORKING_COPY_DIR}

        # check if directory is empty
        # if no: was already cloned, that is Ok for later runs, just continue
        # if yes: do first git clone, should just run once, when first running this script
        if [ "$(ls -A ${GIT_LOCAL_WORKING_COPY_DIR})" ]; then
                echo Did not create local working copy because folder already exists
        else
                # do initial clone
                echo Do initial clone
                git clone ${REMOTE_REPO} ${GIT_LOCAL_WORKING_COPY_DIR}
        fi

# end of function setup_local_working_copy
}

sanity_check

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -f | --force )          shift
                                FORCE=1
                                ;;
        -d | --dev )            shift
                                DEVELOP=1
                                ;;
    esac
    shift
  done

# If FORCE is 1, then skip error trap
if [[ "${FORCE}" == "1" ]]; then
    echo Forcing update, also when errors occur
  else
    echo Trapping all errors
    # catch any errors that occur
    set -e
    trap 'catch $? $LINENO' EXIT
    catch() {
    echo "catching!"
    if [ "$1" != "0" ]; then
        # error handling goes here
        echo "Error $1 occurred on $2"
    fi
    }
fi

# needed for first time deployments. Does not hurt to run again after initial setup
setup_local_working_copy

# update sources from git
cd ${GIT_LOCAL_WORKING_COPY_DIR}
# override local changes
git reset --hard origin/${GIT_REMOTE_BRANCH}
git pull origin ${GIT_REMOTE_BRANCH}
git checkout ${GIT_REMOTE_BRANCH}
# override local changes
git reset --hard origin/${GIT_REMOTE_BRANCH}
					 
git pull

# checkout certain commit HASH
# get the commit hashes from: https://bitbucket.org/art-decor/art-decor/commits/
# git checkout 312f1c4

# create .env file for this installation
${SCRIPT_DIR}/proxy_patch_env.sh

# build and deploy
echo npm install
npm install

# Option DEVELOP can be used to run vue with npm run serve (usually on your own laptop)
# Normal deployments, (also on our test/develop servers) run with npm run build (DEPLOYMENT = 0)
if [[ "${DEVELOP}" == "1" ]]; then
    echo Run for development
    echo npm run dev 
    npm rundev 
  else
    echo Run for production
    echo npm run start with special script signature
    # killall of these
    killall -e 'npm run startadhttpproxy' -q | echo no process running 
    # special start script name to allow automatic kill running server before upgrade
    nohup npm run startadhttpproxy &
    echo npm run done
fi

