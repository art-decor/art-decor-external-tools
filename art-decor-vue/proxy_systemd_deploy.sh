#!/bin/sh
# script to install a service to systemd on Linux

# parameters
# get common parameters from settings script
. ./settings_proxy
        # provides input for the following variables:
          # art-decor-http-proxy: where is our systemd script located
          # SYSTEMD_FOLDER=/etc/systemd/system/
          # art-decor-http-proxy: what is the name of the service
          # SYSTEMD_SERVICE=art-decor-http-proxy.service
	  # art-decor-http-proxy: which user runs the proxy
          # SYSTEMD_USER=nginx

function sanity_check ()
{
       # sanity check
       # these parameters needs to be set:

       echo $0 Running this script with the following settings:
       for var in SYSTEMD_FOLDER SYSTEMD_SERVICE SYSTEMD_USER; do
         if [ -z "${!var}" ]
           then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings'"
         else echo "    ${var}=${!var}"
         fi
       done
}

if [ -f ${SYSTEMD_FOLDER}/${SYSTEMD_SERVICE} ]; then
    systemctl stop ${SYSTEMD_SERVICE}
    systemctl disable ${SYSTEMD_SERVICE}
else
    echo cp -f ./${SYSTEMD_SERVICE} ${SYSTEMD_FOLDER}
    cp -f ./${SYSTEMD_SERVICE} ${SYSTEMD_FOLDER}
fi

systemctl enable ${SYSTEMD_SERVICE}
echo systemctl start ${SYSTEMD_SERVICE}
systemctl start ${SYSTEMD_SERVICE}
echo "Please check that the proxy is running: systemctl start ${SYSTEMD_SERVICE}"
echo "Upon failures: check access rights to directories and port assignment conflicts"
