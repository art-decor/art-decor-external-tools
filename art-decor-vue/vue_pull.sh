#!/bin/bash
# all actions that should be done for pulling new ART-DECOR vue sources and deploy
# usage: To run this script, with local edit (from vue_patch_env.sh): ./vue_pull.sh --local

# Version:
# 20231213: initial version
# 20240226: AD30-1403: first remove the node_modules folder + package-lock.json file
# 20240723: AD30-1630: do a git pull first, otherwise change to a release branch does not work 
# 20240906: AD30-1693 Vue installation procedure change for patching env

# get the directory where this script is located
SCRIPT_DIR=$(dirname $(readlink -f $0))
# change directory to directory where this script is located
cd ${SCRIPT_DIR}

# set defaults
GIT_REMOTE_BRANCH=development
LOCAL=0

# get common parameters from settings script
. ./settings_vue
# provides settings for the following variables:
# * git local working copy :: GIT_LOCAL_WORKING_COPY_DIR
# * link to remote git for artdecor :: REMOTE_REPO
# * which branch to checkout :: GIT_REMOTE_BRANC
# * directory where vue is served from for nginx :: VUE_PUBLIC_DIR_NGINX

function ask_user_abort () {
   echo $1
   echo Do you want to continue anyway?
   read -p "Continue (y/n)? " choice
   case "$choice" in
     y|Y ) echo Continuing;;
     n|N ) abort_installation;;
     * ) abort_installation;;
   esac

# end of function ask_user_abort
}

function abort_installation ()
{
    echo Cancelling script
    exit 0

# end of function abort_installation
}

function sanity_check ()
{
       # sanity check
       # these parameters needs to be set:

       echo $0 Running this script with the following settings:
       for var in GIT_LOCAL_WORKING_COPY_DIR SCRIPT_DIR VUE_PUBLIC_DIR_NGINX GIT_REMOTE_BRANCH; do
         if [ -z "${!var}" ]
           then echo "$0 ERROR: ${var} is not set! Either put it in the file 'settings'"
         else echo "    ${var}=${!var}"
         fi
       done

       # check if folders are actually present
       [ ! -d ${VUE_PUBLIC_DIR_NGINX} ] && ask_user_abort "WARNING: Directory VUE_PUBLIC_DIR_NGINX ${VUE_PUBLIC_DIR_NGINX} DOES NOT exist."

# end of function sanity_check
}

function setup_local_working_copy ()
{
        # create directory for local repo
        mkdir -p ${GIT_LOCAL_WORKING_COPY_DIR}
        # cd to directory
        cd ${GIT_LOCAL_WORKING_COPY_DIR}

        # check if directory is empty
        # if no: was already cloned, that is Ok for later runs, just continue
        # if yes: do first git clone, should just run once, when first running this script
        if [ "$(ls -A ${GIT_LOCAL_WORKING_COPY_DIR})" ]; then
                echo Did not create local working copy because folder already exists
        else
                # do initial clone
                echo Do initial clone
                git clone ${REMOTE_REPO} ${GIT_LOCAL_WORKING_COPY_DIR}
        fi

# end of function setup_local_working_copy
}

sanity_check

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -f | --force )          shift
                                FORCE=1
                                ;;
        -d | --dev )            shift
                                DEVELOP=1
                                ;;
        -l | --local )          shift
                                LOCAL=1
                                ;;
    esac
    shift
  done

# If FORCE is 1, then skip error trap
if [[ "${FORCE}" == "1" ]]; then
    echo Forcing update, also when errors occur
  else
    echo Trapping all errors

# catch any errors that occur
set -e
trap 'catch $? $LINENO' EXIT
catch() {
  echo "catching!"
  if [ "$1" != "0" ]; then
  # error handling goes here
  echo "Error $1 occurred on $2"
  fi
}

fi

# needed for first time deployments. Does not hurt to run again after initial setup
setup_local_working_copy

# update sources from git
cd ${GIT_LOCAL_WORKING_COPY_DIR}
# 20240723: ad30-1630: do a git pull first, otherwise change to a release branch does not work because the local working copy does not know about the remote (new) release branch
git pull
# override local changes
git reset --hard origin/${GIT_REMOTE_BRANCH}
git pull origin ${GIT_REMOTE_BRANCH}
git checkout ${GIT_REMOTE_BRANCH}
# override local changes
git reset --hard origin/${GIT_REMOTE_BRANCH}

git pull
# git checkout feature/AD30-126-urgent-news-management

# checkout certain commit HASH
# get the commit hashes from: https://bitbucket.org/art-decor/art-decor/commits/
# git checkout 312f1c4

if [[ "${LOCAL}" == "1" ]]; then
  # do local git code overrides
  echo Do local git code overrides
  ${SCRIPT_DIR}/vue_patch_env.sh
fi

# build and deploy
echo INFO: deleting directory node_modules and package-lock.json if present
rm -Rf node_modules
file=package-lock.json
[ ! -f "${file}" ] || { rm package-lock.json ;}

echo npm install
npm install

# Option DEVELOP can be used to run vue with npm run serve (usually on your own laptop)
# Normal deployments, (also on our test/develop servers) run with npm run build (DEPLOYMENT = 0)
if [[ "${DEVELOP}" == "1" ]]; then
    echo Run for development
    echo npm run serve
    npm run serve
  else
    echo Run for production
    echo npm run build
    npm run build

    echo after npm run build
    ls

    echo npm done, copy dist/ to NGINX ${VUE_PUBLIC_DIR_NGINX}
    rm -Rf ${VUE_PUBLIC_DIR_NGINX}/*
    cp -R dist/* ${VUE_PUBLIC_DIR_NGINX}
fi

# EOF
