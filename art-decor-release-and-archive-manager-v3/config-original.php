<?php

/* CONFIGS */
	// the URL to the xquery to dis-ref the project DECOR file
	$urlmra = "http://localhost:8877/systemtasks/modules/release-and-archive-manager.xquery" ;
	$urlpvr = "http://localhost:8877/art/modules/get-decor-project-versions.xquery" ;

	// data for the RetrieveProject call
	$data = array (
	// the user name of the sigbot user in Exist-DB, having the dba right
        'user' => 'adbot',
	// the secret password of that user
        'password' => '{ADBOTPASSWORD}',
	// the automatic notifier wizzard word, fixed
        'secret' => '{SECRET}'
        );
	
	$javacmd="java -jar";
	
	$bindir="bin";
	$cssdir="css";
	$javajar="$bindir/saxon9he.jar";
	$xsl="https://assets.art-decor.org/ADAR/rv/DECOR2schematron.xsl";
	$isoschematrons="https://assets.art-decor.org/ADAR/rv/iso-schematron";
	
	$compresscommand   = "/usr/bin/zip -r ";  # to compress the runtime environment with recurse option
	$uncompresscommand = "/usr/bin/unzip -o ";   # to uncompress files with overwrite option
	$untarcommand      = "/usr/bin/tar xfz "; # to untar a compressed gz tar archive 

	// set the following to 1 if pdf creation is allowed;
	// then the subsequent parameters must be set properly, too
	$allowpdf=0;
	# path to prince executable command on this server
    #$topdfcommand="/usr/local/bin/prince";
	# path to local css directory
    #$topdfcssdir="../css";
	# may use customized CSS
    #$topdfcss="$topdfcssdir/artdecor2pdf.css";
	# or default decorprint.css which resides in assets
	# $topdfcss="$topdfcssdir/override-example-artdecor2pdf.css";
	
?>
