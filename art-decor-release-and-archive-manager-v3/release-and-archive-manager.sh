#!/bin/sh
#

DIR=/opt/art-decor-release-and-archive-manager
DATE=`date +%Y%m%d-%H`
LOG=logs/log-${DATE}.txt

cd ${DIR}

php release-and-archive-manager.php $* >> ${LOG}

# cleanup log files older than 50 days
find logs/* -mtime +50 -exec rm {} \;

# cleanup tmp files and dirs older than 5 days
find _tmp_* -mtime +5 -exec rm -rf {} \;
