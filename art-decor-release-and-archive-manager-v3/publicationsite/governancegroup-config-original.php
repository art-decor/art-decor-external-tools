<?php

// parameters for governance groups
$governancegroup = "Governance Group Name";
$governanceid    = "2.16.840.1.113883.1.2.3.4.5";
// project prefixes managed by this governance group, alle prefixes must end on -
$projectprefixes = array ("xxxx-", "yyyy-");

// Set language, must be one of 'en', 'de', 'nl' (so far)
$language       = "en";

// Uncomment and change to any alternative CSS for your index.php
// default: 'css/front.css'
//$pagecssmain = 'css/front.css';

// Uncomment and change to any alternative images directory.
// Note: the default CSS will use the default images dir
// default: 'images'
$pageimagesmain = 'images';

// Uncomment and change to any alternative logo for the top right part of the index page
// default: ${pageimagesmain} . '/art-decor-logo40.png'
//$pagetoplogo = $pageimagesmain . '/art-decor-logo40.png';
$pagetoplogo = $pageimagesmain . '/art-decor-logo-small.jpg';

// Uncomment and change to any alternative favicon for this page
// default: $pageimagesmain . '/art-decor-logo40.png'
//$pagefavicon = $pageimagesmain . '/favicon.ico';

// Uncomment if this project has a live project in ART-DECOR
// default: https://art-decor.org/art-decor/decor-project--<projectprefix>
$adgghomepage    = "/art-decor/decor-governance-group--?id=$governanceid";

// Uncomment if you want to show a link to with contact information
// default: <none>
// $contact = 'http://art-decor.org/mediawiki/index.php/Contact';

// Uncomment if you want to add an additional copyright text as a footer
// default: <none>
// $additionalcopyright = '&copy; ART-DECOR Expert Group 2012-2015';

?>

