<?php

// parameters
$project        = "Project Name";
$subtitle       = "Subtitle";
$projectprefix  = "xxxx-"; // always ends on "-"

// Set language, must be one of 'en', 'de', 'nl' (so far)
$language       = "en";

// Uncomment and change to any alternative CSS for your index.php
// default: 'css/front.css'
//$pagecssmain = 'css/front.css';

// Uncomment and change to any alternative images directory.
// Note: the default CSS will use the default images dir
// default: 'images'
$pageimagesmain = 'images';

// Uncomment and change to any alternative logo for the top right part of the index page
// default: $pageimagesmain . '/art-decor-logo40.png'
//$pagetoplogo = $pageimagesmain . '/art-decor-logo40.png';

// Uncomment and change to any alternative favicon for this page
// default: $pageimagesmain . '/art-decor-logo40.png'
$pagefavicon = $pageimagesmain . '/favicon.ico';

// Uncomment infostartpage if this project has a information page somewhere,
// e.g. in the Nictiz-informatiestandaarden section
// you can also set the title by assigning text to infostarttitle
// default: <none>
//$infostarttitle = '';
//$infostartpage = '';

// Uncomment if this project has a design page in the Nictiz-informatiestandaarden wiki
// default: <none>
//$wikistartpage = '';

// Uncomment if this project has a live project in ART-DECOR
// default: https://art-decor.org/art-decor/decor-project--<projectprefix>
//$adstartpage    = "https://art-decor.org/art-decor/decor-project--" . $projectprefix;

// Uncomment if you want to enable google analytics by entering the correct account id
// default: <none>
//$googleanalyticsaccount = "UA-20138515-1";

// Uncomment if you want to show a link to with contact information
// default: <none>
// $contact = 'http://art-decor.org/mediawiki/index.php/Contact';

// Uncomment if you want to add an additional copyright text as a footer
// default: <none>
// $additionalcopyright = '&copy; ART-DECOR Expert Group 2012-2015';

?>

