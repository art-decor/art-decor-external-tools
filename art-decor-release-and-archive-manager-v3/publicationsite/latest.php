<?php
/*
	ADRAM art-decor release and archive manager
	publication site part - index and assets

	ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
    Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
    see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

    This file is part of the ART-DECOR® tools suite.

    Copyright (C) 2013-2023
    Author: Kai U. Heitmann

*/

// DONT EDIT THIS FILE, IT'S OVERWRITTEN PERIODICALLY

// set default date TZ
date_default_timezone_set("Europe/Berlin");
$date = date(DATE_RFC822);

# don|t show warnings
error_reporting(E_ALL ^ E_WARNING);

# include localized config
include "config.php";

// scan directory for html files and prepare a list of most recent and all archived versions
$directory = "";
// prefix with wildcard
$prefix = $projectprefix . "html-*" ;
$fns = glob("" . $directory . $prefix);

// sort it by name
sort($fns);

// go thru it and setup lists
$nof = count($fns);

// deep link into specific page or default to index.html
if ($_GET["action"]=='view.project') {
    $page = '/project.html';
} elseif ($_GET["action"]=='view.dataset') {
    $page = '/dataset.html';
} elseif ($_GET["action"]=='view.scenarios') {
    $page = '/scenarios.html';
} elseif ($_GET["action"]=='view.templates') {
    $page = '/rules.html';
} elseif ($_GET["action"]=='view.terminology') {
    $page = '/terminology.html';
} else {
    $page = '/index.html';
}

// link to most recent
if ($nof === 0) {
    if (file_exists($directory . "versions.php")) {
        // perinatologie hack...
        $htmlindex = "versions.php";
    } else {
        $htmlindex = "index.php";
    }
} else {
    // first guess
    $htmlindex = $fns[$nof-1] . $page ;
    // open status file from DECOR if available
    $projectversionreleasefile = $projectprefix . "project.versionsreleases.xml";
    $allprojectversionsreleases = simplexml_load_file($projectversionreleasefile, "SimpleXMLElement", LIBXML_NOWARNING);
    if ($allprojectversionsreleases['projectPrefix'] == $projectprefix) {
        foreach ($allprojectversionsreleases as $rv) {
            $thisdecorstatus  = $rv['statusCode'] ;
            // show only vivid releases (draft, pending, active)
            if ($thisdecorstatus == 'draft' or $thisdecorstatus == 'pending' or $thisdecorstatus == 'active') {
                $htmlindex = $projectprefix . "html-" . $rv['publicationdate'] . $page ;
                break;
            }
        }
    }
}

// first part of the index
// echo "xxxxxxxxxx: " . $htmlindex . "\n";exit;
echo <<<END
<META HTTP-EQUIV="Refresh" Content="0; URL=$htmlindex">
<META name="robots" content="noindex, nofollow">
<META http-equiv="expires" content="0">
END;

?>
