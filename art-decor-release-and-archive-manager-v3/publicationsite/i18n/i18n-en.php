<?php

# locale en
$_i18n_title           = "DECOR information for project: ";
$_i18n_publicationsfor = "Publications for";
$_i18n_current         = "Current";
$_i18n_noactuals       = "No current publications";
$_i18n_archive         = "Archive";
$_i18n_rcancelled      = "Cancelled";
$_i18n_download        = "Download";
$_i18n_download_closed = "Download (closed)";
$_i18n_view            = "show";
$_i18n_View            = "Show";
$_i18n_release         = "Final";
$_i18n_version         = "Intermediate";
$_i18n_nopubsavail     = "No publications available yet";
$_i18n_notavailyet     = "Not available yet";
$_i18n_notavail        = "Not&nbsp;available";
$_i18n_pubdate         = "Publication date";
$_i18n_date            = "Date";
$_i18n_pubstatus       = "Publication status. From ART-DECOR or 'Final' / 'Intermediate'. Patches will also be marked 'Final'.";
$_i18n_status          = "Status";
$_i18n_pdffiles        = "PDF-files";
$_i18n_pdf             = "PDF";
$_i18n_fhirigdir       = "FHIR-Implementation Guide";
$_i18n_fhirig          = "FHIR IG";
$_i18n_schruntime      = "Schematron runtime";
$_i18n_sch             = "Schematron";
$_i18n_xmlmat          = "XML materials";
$_i18n_xml             = "XML";
$_i18n_desc            = "Short description for the publication. Full version information in HTML";
$_i18n_comment         = "Comment";
$_i18n_staticpub       = "Static publication from DECOR";
$_i18n_html            = "HTML";
$_i18n_active          = "active";
$_i18n_pending         = "under pre-publication review";
$_i18n_draft           = "draft";
$_i18n_retired         = "retired";
$_i18n_cancelled       = "cancelled";
$_i18n_contact         = "Contact";
$_i18n_project_home    = "Landing page";
$_i18n_project_wiki    = "Wiki";
$_i18n_project_artdecor = "Development environment";
$_i18n_governancegroup = "Governance Group";
$_i18n_backtogg        = "Back to Governance Group";
$_i18n_activepubs      = "active publication(s)";
$_i18n_actualpubs      = "actual publication(s)";
$_i18n_including       = "including";
$_i18n_filteredpubs    = "partial publication(s)";
$_i18n_archcancedpubs  = "archived/cancelled publication(s)";

?>
