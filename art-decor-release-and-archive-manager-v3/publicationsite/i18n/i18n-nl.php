<?php

# locale nl
$_i18n_title           = "DECOR informatie voor project: ";
$_i18n_publicationsfor = "Publicaties voor";
$_i18n_current         = "Actueel";
$_i18n_noactuals       = "Geen actuele publicaties";
$_i18n_archive         = "Archief";
$_i18n_rcancelled      = "Geannuleerd";
$_i18n_download        = "Download";
$_i18n_download_closed = "Download (gesloten)";
$_i18n_view            = "toon";
$_i18n_View            = "Toon";
$_i18n_release         = "Definitief";
$_i18n_version         = "Tussentijds";
$_i18n_nopubsavail     = "Nog geen publicaties beschikbaar";
$_i18n_notavailyet     = "nog niet beschikbaar";
$_i18n_notavail        = "niet&nbsp;beschikbaar";
$_i18n_pubdate         = "Publicatiedatum";
$_i18n_date            = "Datum";
$_i18n_pubstatus       = "Publicatiestatus. Uit ART-DECOR of 'Definitief' / 'Tussentijds'. Patches krijgen ook de status 'Definitief'.";
$_i18n_status          = "Status";
$_i18n_pdffiles        = "PDF-bestanden";
$_i18n_pdf             = "PDF";
$_i18n_fhirigdir       = "FHIR implementatie gids";
$_i18n_fhirig          = "FHIR IG";
$_i18n_schruntime      = "Schematron runtime";
$_i18n_sch             = "Schematron";
$_i18n_xmlmat          = "XML-materialen";
$_i18n_xml             = "XML";
$_i18n_desc            = "Korte samenvatting bij de publicatie. Volledige versiegegevens in HTML";
$_i18n_comment         = "Commentaar";
$_i18n_staticpub       = "Statische publicatie uit DECOR";
$_i18n_html            = "HTML";
$_i18n_active          = "actief";
$_i18n_pending         = "onder revisie vóór de publicatie";
$_i18n_draft           = "ontwerp";
$_i18n_retired         = "obsoleet";
$_i18n_cancelled       = "geannuleerd";
$_i18n_contact         = "Contact";
$_i18n_project_home    = "Landingspagina";
$_i18n_project_wiki    = "Wiki";
$_i18n_project_artdecor = "Ontwikkelomgeving";
$_i18n_governancegroup = "Governance Groep";
$_i18n_backtogg        = "Terug naar de Governance Groep";
$_i18n_activepubs      = "actieve publicatie(s)";
$_i18n_actualpubs      = "actuele publicatie(s)";
$_i18n_including       = "inclusief";
$_i18n_filteredpubs    = "deelpublicatie(s)";
$_i18n_archcancedpubs  = "gearchiveerde/geannuleerde publicatie(s)";

?>
