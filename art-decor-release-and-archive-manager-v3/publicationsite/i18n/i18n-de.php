<?php

# locale de
$_i18n_title           = "DECOR Information für Projekt: ";
$_i18n_publicationsfor = "Publikationen für";
$_i18n_current         = "Aktuell";
$_i18n_noactuals       = "Keine aktuellen Publikationen";
$_i18n_archive         = "Archiv";
$_i18n_rcancelled      = "Annuliert";
$_i18n_download        = "Herunterladen";
$_i18n_download_closed = "Herunterladen (geschlossen)";
$_i18n_view            = "zeigen";
$_i18n_View            = "Zeigen";
$_i18n_release         = "Final";
$_i18n_version         = "Zwischenzeitlich";
$_i18n_nopubsavail     = "Bisher keine Publikationen verfügbar";
$_i18n_notavailyet     = "Noch nicht verfügbar";
$_i18n_notavail        = "Nicht&nbsp;verfügbar";
$_i18n_pubdate         = "Publikationsdatum";
$_i18n_date            = "Datum";
$_i18n_pubstatus       = "Publikationsstatus. Aus ART-DECOR oder 'Final' / 'Zwischenzeitlich'. Patches sind ebenso 'Final'.";
$_i18n_status          = "Status";
$_i18n_pdffiles        = "PDF-Files";
$_i18n_pdf             = "PDF";
$_i18n_fhirigdir       = "FHIR-Implementierungsleitfaden";
$_i18n_fhirig          = "FHIR-IG";
$_i18n_schruntime      = "Schematron Runtime";
$_i18n_sch             = "Schematron";
$_i18n_xmlmat          = "XML Materialien";
$_i18n_xml             = "XML";
$_i18n_desc            = "Kurze Beschreibung der Publikation. Ganze Versionsinformation im HTML";
$_i18n_comment         = "Kommentar";
$_i18n_staticpub       = "Statische Publikation aus DECOR";
$_i18n_html            = "HTML";
$_i18n_active          = "aktiv";
$_i18n_pending         = "unter Revision vor der Publikation";
$_i18n_draft           = "Entwurf";
$_i18n_retired         = "obsolet";
$_i18n_cancelled       = "annuliert";
$_i18n_contact         = "Kontakt";
$_i18n_project_home    = "Startseite";
$_i18n_project_wiki    = "Wiki";
$_i18n_project_artdecor = "Entwicklungsumgebung";
$_i18n_governancegroup = "Governance-Gruppe";
$_i18n_backtogg        = "Zurück zur Governance-Gruppe";
$_i18n_activepubs      = "aktive Publikation(en)";
$_i18n_actualpubs      = "aktuelle Publikation(en)";
$_i18n_including       = "einschließlich";
$_i18n_filteredpubs    = "Teilpublikation(en)";
$_i18n_archcancedpubs  = "archivierte/annullierte Publikation(en)";

?>
