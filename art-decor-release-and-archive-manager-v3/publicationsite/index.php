<?php
/*
	ADRAM art-decor release and archive manager
	publication site part - index and assets

	ART-DECOR® STANDARD COPYRIGHT AND LICENSE NOTE
	Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools GmbH
	see https://docs.art-decor.org/copyright and https://docs.art-decor.org/licenses

	This file is part of the ART-DECOR® tools suite.

	Copyright (C) 2013-2025
	Author: Kai U. Heitmann

*/

// DONT EDIT THIS FILE, IT'S OVERWRITTEN PERIODICALLY

// version
$VERSION = "2025.5";
$thisscriptversion = "ADRAM site v" . $VERSION;
// if (is_file("scriptversion.txt")) $thisscriptversion .= htmlspecialchars(file_get_contents("scriptversion.txt"));
$currentYear = date('Y');

// signal to caller that this is ADRAM
header("X-Powered-By: ART-DECOR-ADRAM");

/************************************************************
	What to do is determined in the following way:

	1. check check whether "adram.config.xml" as a call parameter is set
	   if so return the content of the file adram.config.xml (sibling file to index.php)
	
	2. check whether "prefix" as a call parameter is set
	   if so process project with that prefix using $prefix-config.php
	   if not go to 3)
    3. if no parameter "prefix" is given then process this using config.php
       3a) if config is a governance group config show the project list page
       3b) if config is a normal project process it as a normal project

************************************************************/

// set default date TZ
date_default_timezone_set("Europe/Berlin");
$date = date(DATE_RFC822);

# don|t show warnings
error_reporting(E_ALL ^ E_WARNING);

// check whether "adram.config.xml" as a call parameter is set
if (isset($_SERVER['REQUEST_URI'])) {
        if ( strpos($_SERVER['REQUEST_URI'], "adram.config.xml" ) ) {
		if ((is_file("adram.config.xml"))) {
			$adramconfig = file_get_contents("adram.config.xml");
			echo $adramconfig;
			exit;
		}	
	}
}

// check whether "prefix" as a call parameter is set
$processprefix = "";
if (isset($_GET["prefix"])) {
	if (strlen($_GET["prefix"])>0) {
    	$processprefix = htmlspecialchars(preg_replace('/[^a-z\._\d-]/i', '', $_GET["prefix"]), ENT_QUOTES, 'UTF-8');
	}
}

// vars
$aspartofgovernancegroup = 1; // indicates that the project is handled as part of a governance group

if (strlen($processprefix)>0) {
	processprojectbyprefix($processprefix, $aspartofgovernancegroup);
	//done
	exit;
}

// check default config - must be present
if (!is_file('config.php')) {
    raisenotconfiguredproperly('config.php');
	exit;
} else {
	include('config.php');
}

// die hards
if (!$language) die ("+++ERROR not set in config: language\n");

// load default language en
include "i18n/i18n-en.php";
// correct (and merge) by choice
switch ($language) {
    case 'de':
        include "i18n/i18n-de.php";
        break;
    case 'nl':
        include "i18n/i18n-nl.php";
        break;
}

// arrange other defaults

if (!isset($pagecssmain)) {
    $pagecssmain = 'css/front.css';
}

if (!isset($pageimagesmain)) {
    $pageimagesmain = 'images';
}

if (!isset($pagetoplogo)) {
    $pagetoplogo = $pageimagesmain . '/art-decor-logo40b.png';
}

if (!isset($pagefavicon)) {
    $pagefavicon = $pageimagesmain . '/favicon.ico';
}

// end defaults

// check whether this is a governance group config or a simple project config
if (isset($governancegroup)) {
	/* start process governance group */

	// presets
	$topmargintitle = "25px";
	$subtitle = "";
	$project = "";

	// die hards
	if (!$projectprefixes) die ("+++ERROR not set in config: projectprefixes\n");

	// first part of the index
	echo <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>$_i18n_title $governancegroup ($_i18n_governancegroup)</title>
<link href="$pagecssmain" rel="stylesheet" type="text/css" />
<link href="$pageimagesmain/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
<div id="page-bg">
<div class="wrapper">
<div class="shadow-left">
<div class="shadow-right">
<div class="main-page">
<div class="main-page2">
<div class="main-page3">
<div class="main-page4">
<div class="vcard"> 
<div class="vcard-header">
<h1 style="margin-top: 10px;">$_i18n_publicationsfor <span class="material-icons orange600">business</span> $governancegroup</h1>
<h4>$_i18n_governancegroup <a href="$adgghomepage"> <i>$_i18n_view</i></a></h4>
END;

// if (strlen($infolink1 . $infolink2 . $infolink3 )>0) echo "<h4>" . $infolink1 . " " . $infolink2 . " " . $infolink3 . "</h4>";

echo <<<END
<!-- Begin Header -->
<!--div class="header">
<h1 style="margin: $topmargintitle 15px">$project</h1>
<h4 style="margin: -20px 15px">$subtitle</h4>
</div-->
<!-- End Header -->
<!-- Begin Horizontal Menu -->
<!-- End Horizontal Menu -->
<!-- Begin Showcase Area -->
<!-- End Showcase Area -->
<!-- Begin Main Content Area -->
</div>
<div class="vcard-text">
END;
	doPagetoplogo($pagetoplogo);
	echo "<div class=\"thecontent\">";
	echo "<table border=\"0\" cellpadding=\"4\" style=\"height: 400px;\">";
	dogovernancegroupprojectheading($projectprefixes);
	echo "</table>";

	// output the rest of the index file
	echo <<<END
<br/>
<!-- Begin Bottom Menu -->
<!-- End Bottom Menu -->
<!-- Begin Bottom Modules -->
<!-- End Bottom Modules -->
<!-- Begin Footer -->
</div>
<div class="vcard-footer">
<div class="footer">
<div class="thefootercontent">
END;

	if (strlen($contact)>0) { 
		echo "<a style=\"float:right; padding-right: 17px;\" href=\"$contact\">$_i18n_contact</a>" ; 
	}

	echo <<<END
<center>$thisscriptversion &copy; The ART-DECOR Expert Group 2010-$currentYear $additionalcopyright</center>
</div>
</div>
<!-- End Footer -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>      
</html>
END;

	/* end process governance group */
} 
else {
	// a simple project config, process it

	processprojectbyprefix($projectprefix, 0);

}

/* ------------- */
function dothead($headline, $doPdf = false, $doRelease = false, $doHTML = false, $doSCH = false, $doXML = false, $doFHIRIG = false, $minimalHeader = false) {

global $_i18n_pubdate, $_i18n_date, $_i18n_pubstatus, $_i18n_status,
	$_i18n_staticpub, $_i18n_staticpub, $_i18n_staticpub, $_i18n_html,
	$_i18n_schruntime, $_i18n_sch, $_i18n_xmlmat, $_i18n_xml, $_i18n_desc, 
	$_i18n_comment, $_i18n_pdffile, $_i18n_pdf, $_i18n_fhirigdir, $_i18n_fhirig;

$downLoadColumnWidth = "1%";
$colspans = $minimalHeader ? "colspan=\"8\"" : "";

echo <<<END
<thead>
<tr align="left">
<th colspan="8" class="caption">$headline</th>
</tr>
<tr align="left" class="lrborder">
<th class="pr-12" width="1%" $colspans title="$_i18n_pubdate">&#160;&#160;$_i18n_date</th>
END;

  if ($doRelease & !$minimalHeader) {
    echo "<th class=\"center px-12\" width=\"$downLoadColumnWidth\" title=\"$_i18n_pubstatus\">$_i18n_status</th>";
  }

	if ($doHTML &!$minimalHeader) {
  	echo "<th class=\"center px-24\" width=\"$downLoadColumnWidth\" title=\"$_i18n_staticpub\">$_i18n_html</th>";
  }

	if ($doFHIRIG &!$minimalHeader) {
		echo "<th class=\"center px-24\" width=\"$downLoadColumnWidth\" title=\"$_i18n_fhirigdir\">$_i18n_fhirig</th>";
	}
	
	if ($doPdf &!$minimalHeader) {
		echo "<th class=\"center px-12\" width=\"$downLoadColumnWidth\" title=\"$_i18n_pdffile\">$_i18n_pdf</th>";
  }

	if ($doSCH & !$minimalHeader) {
  	echo "<th class=\"center px-12\" width=\"$downLoadColumnWidth\" title=\"$_i18n_schruntime\">$_i18n_sch</th>";
  }

	if ($doXML & !$minimalHeader) {
  	echo "<th class=\"center px-12\" width=\"$downLoadColumnWidth\" title=\"$_i18n_xmlmat\">$_i18n_xml</th>";
  }

	if (!$minimalHeader) {
		echo "<th width=\"33%\" title=\"$_i18n_comment\">$_i18n_comment</th>";
	}
	echo "</tr></thead>\n";

}

/* ------------------- */
function doPagetoplogo ($pagetoplogo) {
	echo <<<END
</div>
<div class="vcard-text">
<table style="margin: 0; border: none;">
<tr>
<td style="text-align: right; border-top: none"><img src="$pagetoplogo" style="max-height: 60px; max-width: 240px;"/></td>
</tr>
</table>
END;
}

/* -------------------------------- */
function raisenotconfiguredproperly($configfile) {
	echo <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/front.css" rel="stylesheet" type="text/css" />
<title>Not yet configured properly</title>
</head>
<body>
<div class="header">
<h1 style="margin: 15px">ADRAM: Error</h1>
<h3 style="margin: 15px">+++ERROR: not properly configured: $configfile not found.</h3>
</div>
</body>
</html>
END;
}

/* ------------------------------------- */
function dogovernancegroupprojectheading ($projectprefixes) {
	
	global $_i18n_actualpubs, $_i18n_including, $_i18n_filteredpubs,
	$_i18n_archcancedpubs, $_i18n_View, $pageimagesmain, $viewLabel;
	
	foreach ($projectprefixes as $p) {
		if (strlen($p)> 0 and substr($p, strlen($p)-1) === "-") {
		} else {
			$p .= "-";
		}
		$cf = $p . 'config.php';
		if (!is_file($cf)) {
    		raisenotconfiguredproperly($cf);
		} else {
			include($cf);
			echo "<h3>" .
				"$project ($p) ";
			switch ($language) {
				case 'en':
					echo "<img src=\"$pageimagesmain/en-US.png\">";
					break;
				case 'de':
					echo "<img src=\"$pageimagesmain/de-DE.png\">";
					break;
				case 'nl':
					echo "<img src=\"$pageimagesmain/nl-NL.png\">";
					break;
			}
			echo "<a href=\"index.php?prefix=$p\"><span class=\"material-icons orange600 pl-7\" data-md-tooltip=\"$_i18n_View\">north_east</span></a>";

			// prepare summary
			$activepubs = 0;
			$filteredpubs = 0;
			$archivedpubs = 0;
			// scan directory for html files and prepare a list of most recent and all archived versions
			$directory = "";
			// get decor publication old style
			// - prefix with wildcard pattern PROJECTPREFIX-html-* with a check of the remainder as timestamp, eg 20240818T123456
			$prefix = $projectprefix . "html-*" ;
			$tmplist = glob($directory . $prefix);
			foreach ($tmplist as $file) { 
				if (preg_match("~.*-html-[0-9]{8}T[0-9]{6}$~", $file)) 
					$p1 = substr($file, strlen($prefix)-1);
					$filtered = $projectprefix . "filter-" . $p1 . ".txt" ;
					if (file_exists($filtered)) {
						$activepubs += 1;
						$filteredpubs += 1;
					} else if ($activepubs == 0) {
						$activepubs += 1;
					}
					$archivedpubs += 1;
			}

			// check latest status of the current publication
			$relfile = $projectprefix . "project.versionsreleases.xml";
			$allprojectversionsreleases = simplexml_load_file($relfile, "SimpleXMLElement", LIBXML_NOWARNING);
			$actualdecorstatus = $allprojectversionsreleases->release['statusCode'];

			// correct archived pubs
			$archivedpubs = $archivedpubs - $activepubs;
			echo "<br/><div style=\"margin: 7px 0 7px 16px;\">";
			if ($activepubs>0) {
				echo "$activepubs $_i18n_actualpubs ";
				if (strlen($actualdecorstatus )) echo " – " . $actualdecorstatus;
				if ($filteredpubs>0) {
					echo " $_i18n_including $filteredpubs $_i18n_filteredpubs ";
				}
				if ($archivedpubs>0) {
					echo ", $archivedpubs $_i18n_archcancedpubs";
				}
			} else {
				echo "$_i18n_nopubsavail";
			}
			echo "</div></h3>";

		}
	}
}

/* ---------------------------- */
function processprojectbyprefix ($processprefix, $aspartofgovernancegroup) {

	global $thisscriptversion, $currentyear;
	global $_i18n_pubdate, $_i18n_date, $_i18n_pubstatus, $_i18n_status,
  	$_i18n_staticpub, $_i18n_staticpub, $_i18n_staticpub, $_i18n_html,
  	$_i18n_schruntime, $_i18n_sch, $_i18n_xmlmat, $_i18n_xml, $_i18n_desc, 
  	$_i18n_comment, $_i18n_pdffile, $_i18n_pdf, $_i18n_project_home, $_i18n_download_closed;
  	
  	// determine the config file
  	if ($aspartofgovernancegroup == 0) {
  		$cf = 'config.php';
  	} else if (strlen($processprefix)> 0 and substr($processprefix, strlen($processprefix)-1) === "-") {
		// processprefix is correctly formed, set config file
		$cf = $processprefix . 'config.php';
	} else if ($aspartofgovernancegroup) {
		// append the missing -, then set config file
		$cf = $processprefix . '-config.php';
	} else {
		// simple project
		$cf = 'config.php';
	}

	// presets
	$topmargintitle = "25px";
	$project = "";
	$subtitle = "";

	if (!is_file($cf)) {
    	raisenotconfiguredproperly($cf);
	} else {
		unset($pagecssmain, $pageimagesmain, $pagetoplogo, $pagefavicon,
			$infostarttitle, $infostartpage, $wikistartpage,
			$adstartpage, $googleanalyticsaccount, $contact, $additionalcopyright) ;
		include($cf);
	}
	
	// load default language en
	include "i18n/i18n-en.php";
	// correct (and merge) by choice
	switch ($language) {
		case 'de':
			include "i18n/i18n-de.php";
			break;
		case 'nl':
			include "i18n/i18n-nl.php";
			break;
	}

	// arrange other defaults

	if (!isset($pagecssmain)) {
		$pagecssmain = 'css/front.css';
	}

	if (!isset($pageimagesmain)) {
		$pageimagesmain = 'images';
	}

	if (!isset($pagetoplogo)) {
		$pagetoplogo = $pageimagesmain . '/art-decor-logo40b.png';
	}

	if (!isset($pagefavicon)) {
		$pagefavicon = $pageimagesmain . '/favicon.ico';
	}

	if (strlen($subtitle)==0) {
		$topmargintitle = "35px";
	} else {
		$topmargintitle = "25px";
	}

	if (isset($infostarttitle)) {
	} else {
		$infostarttitle = "";
	}

	if (isset($infostartpage)) {
		$infolink1 = "<small><a href=\"" . $infostartpage . "\">" . $_i18n_project_home . "</a></small>";
	} else {
		$infolink1 = '';
	}

	if (isset($wikistartpage)) {
		$infolink2 = '<small><a href="' . $wikistartpage . '">' . $_i18n_project_wiki . " " . $project . '</a></small>';
		if (strlen($infolink1)>0) {
			$infolink2 = " • " . $infolink2;
		}
	} else {
		$infolink2 = '';
	}

	if (isset($adstartpage)) {
		$infolink3 = '<small><a href="' . $adstartpage . '">' . $_i18n_project_artdecor . '</a>';
		if (strlen($projectprefix)>0) $infolink3 .= " (" . $projectprefix . ")";
		$infolink3 .= '</small>';
		if (strlen($infolink1 . $infolink2)>0) {
			$infolink3 = " • " . $infolink3;
		}
	} else {
		$infolink3 = '';
	}

	if (!isset($googleanalyticsaccount)) {
		$googleanalyticsaccount = "";
	}

	if (!isset($contact)) {
		$contact = "";
	}

	if (!isset($additionalcopyright)) {
		$additionalcopyright = "";
	}

	// end defaults

	// first part of the index
	echo <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>$_i18n_title $project ($projectprefix)</title>
<link href="$pagecssmain" rel="stylesheet" type="text/css" />
<link href="$pageimagesmain/favicon.ico" rel="shortcut icon" type="image/x-icon" />
END;
	
	// google analytics if needed
	if (strlen($googleanalyticsaccount)>0) {
	echo <<<END
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '$googleanalyticsaccount']);
_gaq.push(["_trackPageview"]);
(function() {
var ga = document.createElement("script");
ga.type = "text/javascript";
ga.async = true;
ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(ga);
})();
</script>
END;
	}
	// continue
	// toggle_on / toggle_off material-icons
	echo <<<END
<script type="text/javascript">
function togglesection(e) {
  var x = document.getElementById(e);
  if (x !== null) {
  	if (x.style.display === 'none') {
    	x.style.display = '';
  	} else {
    	x.style.display = 'none';
  	}
  }
  console.log(e);
};
</script>
END;
	echo <<<END
</head>
<body>
<div id="page-bg">
<div class="wrapper">
<div class="shadow-left">
<div class="shadow-right">
<div class="main-page">
<div class="main-page2">
<div class="main-page3">
<div class="main-page4">

<div class="vcard"> 
  <div class="vcard-header">
  $_i18n_publicationsfor $project
END;

	// subtitle if needed
	if (strlen($subtitle)>0) echo "<h4>$subtitle</h4>";

	// backlink if needed, ie $processprefix is set
	if (strlen($processprefix)>0 and $aspartofgovernancegroup > 0) {
		$backlinktogg = " | <a href=\"index.php\"><span class=\"material-icons orange600\">business</span>$_i18n_backtogg</a>";
	} else {
		$backlinktogg = "";
	}
	
	if (strlen($infolink1 . $infolink2 . $infolink3 . $backlinktogg)>0) {
		echo "<h4>" . $infolink1 . " " . $infolink2 . " " . $infolink3 ;
		if (strlen($backlinktogg)) echo $backlinktogg;
		echo "</h4>";
	}

	echo <<<END
</div>
<div class="vcard-text">
END;
	doPagetoplogo($pagetoplogo);
echo <<<END
<!-- Begin Header -->
<!-- End Header -->
<!-- Begin Horizontal Menu -->
<!-- End Horizontal Menu -->
<!-- Begin Showcase Area -->
<!-- End Showcase Area -->
<!-- Begin Main Content Area -->
<div class="thecontent">
END;

	/*
		Prepare a list of most recent and all archived versions.
		- Scan publication directory for html-* and fhir-ig-* directories.
		- If such a thing is found, add it to the list of items to be shown at all.
	*/
	$directory = "";
	$fns = array();
	// get decor publication FHIR IGs
	// - wildcard pattern PROJECTPREFIX-fhir-ig-TIMESTAMP with timestamp, eg 20240818T123456
	$fprefix = $projectprefix . "fhir-ig-*" ;
	$ftmplist = glob($directory . $fprefix);
	foreach ($ftmplist as $file) { 
		if (preg_match("~.*-fhir-ig-[0-9]{8}T[0-9]{6}$~", $file)) {
			$p1 = substr($file, strlen($file) - 15);
			$fns[$p1] = $file;
		}
	}
	// get decor publication, prefix with
	// - old style: wildcard pattern PROJECTPREFIX-html-TIMESTAMP with timestamp, eg 20240818T123456
	$prefix = $projectprefix . "html-*" ;
	$tmplist = glob($directory . $prefix);
	foreach ($tmplist as $file) { 
		if (preg_match("~.*-html-[0-9]{8}T[0-9]{6}$~", $file)) {
			$p1 = substr($file, strlen($file) - 15);
			$fns[$p1] = $file;
		}
	}
	// var_dump($fns);exit;
	/* $prefix = $projectprefix . "*.pdxxxxxxxf" ;
	// $tmplist = array(); //glob($directory . $prefix);
	foreach ($tmplist as $file) { 
		if (preg_match("~.*-[0-9]{8}T[0-9]{6}.pdf$~", $file)) 
			$fns[] = $file; 
	}
	*/
	// nof is number of matching html files or folders found
	$nof = count($fns);

	// possible project version and release file from DECOR through ADRAM
	$projectversionreleasefile = $projectprefix . "project.versionsreleases.xml";

	$doPdf     = count(glob("" . $directory . $projectprefix . "*.pdf"))+count(glob("" . $directory . $projectprefix . "*.pdf.redirect"))>0;
	$doRelease = (count(glob("" . $directory . $prefix . "/RELEASE.txt")) +
				  count(glob("" . $directory . $prefix . "/VERSION.txt"))>0)
				  or (file_exists($projectversionreleasefile));
	$doHTML    = count(glob("" . $directory . $projectprefix . "html-*"))+count(glob("" . $directory . $projectprefix . "html-*.zip"))>0;
	$doSCH     = count(glob("" . $directory . $projectprefix . "runtime-*"))+count(glob("" . $directory . $projectprefix . "runtime-*.zip"))>0;
	$doFHIRIG  = count(glob("" . $directory . $projectprefix . "fhir-ig-*"))+count(glob("" . $directory . $projectprefix . "fhir-ig-*.zip"))>0;
	$doXML     = count(glob("" . $directory . $projectprefix . "xml-*"))+count(glob("" . $directory . $projectprefix . "xml-*.zip"))>0;

	// sort it by name
	if (!empty($fns)) { sort($fns); };
	// var_dump($fns);

	// this yields:  fn1 fn2 fn3 (fn file name, 123 date), $fns[0] contains fn1

	// go thru it and setup lists
	// loop thru files, reverse order

	// open status file from DECOR if available
	$allprojectversionsreleases = simplexml_load_file($projectversionreleasefile, "SimpleXMLElement", LIBXML_NOWARNING);

	echo "<table border=\"0\" cellpadding=\"4\" style=\"height: 400px;\">";

	// if no publications are available at all, we are done
	if ($nof == 0) {

		dothead ($_i18n_current, false, false, false, false, false, false, true);

		$now = date('Y-m-d G:i');
		echo"<tr>";
		echo "<td>$now</td>";
		echo "<td>$_i18n_nopubsavail</td>";
		echo "<td colspan=\"6\"> </td>";
		echo"</tr>";

	} else {

		// output array takes a table row <tr> with all content where a prefix is added (a category)
		// which carries the category (section) of the display:
		//   category = AAAyyyymmddThhmmss    active (first and subsequent except cancelled ones)
		//   category = XCNyyyymmddThhmmss    cancelled
		//   category = FLAyyyymmddThhmmss    a filter text
		//   category = FLXyyyymmddThhmmss    a filter text, cancelled
		//   category = FLRyyyymmddThhmmss    a filter text, retired
		//     and yyyymmddThhmmss is the date of that respective version/release
		// output array is output later

		$outputarray = array();

		for($i = $nof-1; $i >= 0; $i--) {
		
			$outputrow = "";  // the table row with td elements only (trs are surrounded later)
			$filteredrow = "";
		
			// get some vars
			/*
			if (preg_match("~.*-[0-9]{8}T[0-9]{6}.pdf$~", $fns[$i])) {
				$p1 = substr($fns[$i], strlen($prefix)-5);
				$p1 = substr($p1, 0, strlen($p1)-4);
				// echo $p1;
			} else {
				$p1 = substr($fns[$i], strlen($prefix)-1);
			}
			*/
			$p1 = substr($fns[$i], strlen($fns[$i]) - 15);
			$pyyyy = substr($p1, 0, 4);
			$pmm = substr($p1, 4, 2);
			$pdd = substr($p1, 6, 2);
			$ph = substr($p1, 9, 2);
			$pm = substr($p1, 11, 2);
			// echo "o:$fns[$i]<br/>p1:$p1<br/>$pyyyy-$pmm-$pdd<br/>";
			$htmlindex = $projectprefix . "html-" . $p1 . "/index.html" ;
			$htmlzip = "pack-" . $fns[$i] . ".zip" ;
			$pdf = $projectprefix . $p1 . ".pdf" ;
			$fhirigdir = $projectprefix . "fhir-ig-" . $p1;
			$fhirig = $fhirigdir . "/index.html" ;
			$pdfredirect = $projectprefix . $p1 . ".pdf.redirect" ;
			$filtered = $projectprefix . "filter-" . $p1 . ".txt" ;
			$runtimezip = $projectprefix . "runtime-" . $p1 . ".zip" ;
			$runtimeclosedzip = $projectprefix . "runtime-closed-" . $p1 . ".zip" ;
			$xmlzip = $projectprefix . "xml-" . $p1 . ".zip" ;
			$releasefile = $fns[$i] . "/RELEASE.txt" ;
			$versionfile = $fns[$i] . "/VERSION.txt" ;
			$commentsfile = $fns[$i] . "/aaacomments" ;
		
			// echo "$fns[$i]<br/>$p1<br/>";
		
			// Language dependent stuff
			$downloadLabel       = $_i18n_download;
			$downloadclosedLabel = $_i18n_download_closed;
			$viewLabel           = $_i18n_View;
			$releaseLabel        = $_i18n_release ; // when RELEASE.txt exists
			$versionLabel        = $_i18n_version; // when VERSION.txt exists
			$noLabel             = "&nbsp;"     ; // when no RELEASE.txt or VERSION.txt exists
		
			if ($i == $nof-1) {
				// this is the first file in the list (most recent!), stuff may come
				$notAvailableLabel = $_i18n_notavailyet;
			} else {
				// this is an archived version, stuff will most likely never come if it is not here now
				$notAvailableLabel = $_i18n_notavail;
			}

			$fhirigname = "?";
			$fhirgstatus = "active";
			$fhirigid = "";
			if (file_exists($fhirig)) {
				// FHIR IG row will include time stamp and a 32 hex digit MD5 fingerprint of IG name from the manifest file
				$manifestfile = $fhirigdir . "/package.manifest.json";
				$fhirigname = $fhirig;
				$fhirgstatus = 'active';
				if (file_exists($manifestfile)) {
					// get package manifest first 
					$manifest = file_get_contents($manifestfile);
					$jmanifest = json_decode($manifest, true);
					// var_dump($jmanifest);
					$fhirigid = $jmanifest['name'];
					// check if there is an ImplementationGuide-{ig-id}.json
					$fhirigname = $fhirigid;  // the default if no title is found
					$igfile = $fhirigdir . "/ImplementationGuide-" . $fhirigid . ".json";
					if (file_exists($igfile)) {
						$ig = file_get_contents($igfile);
						$jig = json_decode($ig, true);
						if (strlen($jig['title']) > 0) $fhirigname = $jig['title'];  // override with real title
						if (strlen($jig['status']) > 0) $fhirgstatus = $jig['status'];  // override with real status
					}
				}
			}
			
			// build a row for this file object, please note that the first element <tr> with category is created and added afterwards
		
			// date
			$outputrow .=  "<td><span style=\"white-space: nowrap;\"><strong>";
			$outputrow .=  $pdd . "-" . $pmm . "-" . $pyyyy ;
			$outputrow .=  "</strong> " ;
			$outputrow .=  $ph . ":" . $pm ;
			$outputrow .=  "</span>&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;

			// Status
			if ($doRelease) {
				
				// if status from DECOR is available (exists in file project.versionsreleases.xml)
				// show that status drawn from DECOR instead of any frozen status
				$decorstatus = "";
				$decorlabel = "";
				if ($allprojectversionsreleases['projectPrefix'] == $projectprefix) {
					foreach ($allprojectversionsreleases as $rv) {
						// either release or version element
						$thisdecorpubdate = $rv['publicationdate'] ;
						$thisdecorstatus  = $rv['statusCode'] ;
						$thisdecorlabel   = $rv['versionLabel'] ;
						// echo ($p1 . "=" . $thisdecorpubdate . "#" . $thisdecorstatus . "<br/>");
						if ($p1 == $thisdecorpubdate and $thisdecorstatus) {
							$decorstatus = $thisdecorstatus;
							$decorlabel  = $thisdecorlabel;
						}
					}
				}
				if (file_exists($fhirig)) {
					$decorstatus = $fhirgstatus;
				}
			
				$outputrow .=  "<td class=\"center\">";
			
				if (strlen($decorstatus)>0) {
					switch ($decorstatus) {
						case 'active':
							$outputrow .= "<span class='dot active' data-md-tooltip='$_i18n_active'/>";
							break;
						case 'pending':
							$outputrow .= "<span class='dot pending' data-md-tooltip='$_i18n_pending'/>";
							break;	
						case 'draft':
							$outputrow .= "<span class='dot draft' data-md-tooltip='$_i18n_draft'/>";
							break;	
						case 'retired':
							$outputrow .= "<span class='dot retired' data-md-tooltip='$_i18n_retired'/>";
							break;
						case 'cancelled':
							$outputrow .= "<span class='dot cancelled' data-md-tooltip='$_i18n_cancelled'/>";
							break;
						default:
							$outputrow .= $decorstatus;
					}
				} elseif (file_exists($releasefile)) {
					$outputrow .=  $releaseLabel;
				} elseif (file_exists($versionfile)) {
					$outputrow .=  $versionLabel;
				} else {
					// default value is there's no version indicator, for now assume version, rather than publication
					$outputrow .=  $noLabel;
				}
				$outputrow .=  "</td>";
			}
		
			// create prefix so we know what status in what category
			if ($decorstatus == 'cancelled') {
				$outputrow = "XCN" . $p1 . $outputrow;
			} else if ($decorstatus == 'retired') {
				$outputrow = "AAR" . $p1 . $outputrow;
			} else {
				$outputrow = "AAA" . $p1 . $outputrow;
			}
		
			// link to HTML
			if ($doHTML) {
				$outputrow .= "<td class=\"center\">";
				if (file_exists($htmlindex)) {
					$outputrow .= "<a href=\"";
					$outputrow .= $htmlindex;
					$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$viewLabel\">read_more</span></a>";
				}
				// download HTML zip
				// echo "<td align=\"center\">";
				if (file_exists($htmlzip)) {
					$outputrow .= "<a href=\"";
					$outputrow .= $htmlzip ;
					$outputrow .= "\"><span class=\"pl-7 material-icons orange600\" data-md-tooltip=\"$downloadLabel\">download</span></a>";
				}
				$outputrow .= "</td>";
			}

			if ($doFHIRIG) {
				$outputrow .= "<td class=\"center\">";
				if (file_exists($fhirig)) {
					$outputrow .= "<a href=\"";
					$outputrow .= $fhirig;
					$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$viewLabel\">read_more</span></a>";
				}
				// download FHIR IG zip
				// echo "<td align=\"center\">";
				/*
				if (file_exists($fhirigzip)) {
					$outputrow .= "<a href=\"";
					$outputrow .= $htmlzip ;
					$outputrow .= "\"><span class=\"pl-7 material-icons orange600\" data-md-tooltip=\"$downloadLabel\">download</span></a>";
				}
				*/
				$outputrow .= "</td>";
			}

			// download PDF
			if ($doPdf) {
				$outputrow .= "<td class=\"center\">";
				if (file_exists($pdf)) {
					$outputrow .= "<a href=\"";
					$outputrow .= $pdf ;
					$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$downloadLabel\">download</span></a>";
				} else if (file_exists($pdfredirect)) {
					$outputrow .= "<a href=\"";
					$outputrow .= htmlspecialchars(file_get_contents($pdfredirect)) ;
					$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$downloadLabel\">arrow_outward</span></a>";
				} else {
					$outputrow .= "<span class=\"material-icons grey\" data-md-tooltip=\"$notAvailableLabel\">link_off</span>";
				}
				$outputrow .= "</td>";
			}
		
			// download runtime ZIP
			if ($doSCH) {
				$outputrow .= "<td class=\"center\">";
				if (file_exists($runtimezip) or file_exists($runtimeclosedzip)) {
					if (file_exists($runtimezip)){
						$outputrow .= "<a href=\"";
						$outputrow .= $runtimezip ;
						$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$downloadLabel\">download</span></a>";
					}
					if (file_exists($runtimeclosedzip)){
						$outputrow .= "<a href=\"";
						$outputrow .= $runtimeclosedzip ;
						$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$downloadclosedLabel\">arrow_circle_down</span></a>";
					}
				} else {
					$outputrow .= "<span class=\"material-icons grey\" data-md-tooltip=\"$notAvailableLabel\">link_off</span>";
				}
				$outputrow .= "</td>";
			}
		
			// download XML zip
			if ($doXML) {
				$outputrow .= "<td class=\"center\">";
				if (file_exists($xmlzip)) {
					$outputrow .= "<a href=\"";
					$outputrow .= $xmlzip ;
					$outputrow .= "\"><span class=\"material-icons orange600\" data-md-tooltip=\"$downloadLabel\">download</span></a>";
				} else {
					$outputrow .= "<span class=\"material-icons grey\" data-md-tooltip=\"$notAvailableLabel\">file_download_off</span>";
				}
				$outputrow .= "</td>";
			}
		
			// HTML comment file
			$outputrow .= "<td>";
			if (file_exists($commentsfile)) {
				$comments = file_get_contents($commentsfile);
				// Comments file may contain limited HTML markup like <br/> or <a href="">...</a>
				//$outputrow .= htmlspecialchars($comments);
				$outputrow .= $comments;
			} else {
				$outputrow .= $decorlabel > 0 ? htmlspecialchars($decorlabel) : "";
			}
			$outputrow .= "</td>";

			$outputrow .= "</tr>";

			// echo htmlspecialchars($outputrow) . "<hr/><br/>";

			if (file_exists($fhirig)) {
				// FHIR IG row includes time stamp and a 32 hex digit MD5 fingerprint of FHIR ig name (id) from the manifest file
				// or if this is not evaluated the MD5 fingerprint from the fhirigname.
				$manifestfile = $fhirigdir . "/package.manifest.json";
				if ($decorstatus == 'cancelled' or $decorstatus == 'retired') {
					$filteredrow .= "IGR";
				} else {
					$filteredrow .= "IGA";
				}
				$filteredrow .= $p1 ;
				if (strlen($fhirigid) == 0) {
					$filteredrow .= md5($fhirigname, FALSE);
				} else {
					$filteredrow .= md5($fhirigid, FALSE);
				}				
				$filteredrow .= "<td colspan=\"8\"><span class=\"material-icons filter600\" data-md-tooltip=\"FHIR Implementation Guide\">local_fire_department</span>" .
				    "FHIR Implementation Guide <strong>" . $fhirigname .
					"</strong></td>";
			}
			
			if (file_exists($filtered)) {
				// filtered row includes time stamp and a 32 hex digit MD5 fingerprint of filter file
				// + the filter file contents as table entry
				if ($decorstatus == 'cancelled') {
					$filteredrow .= "FLX" ;
				} else if ($decorstatus == 'retired') {
					$filteredrow .= "FLR" ;
				} else {
					$filteredrow .= "FLA" ;
				}
				$filteredrow .= $p1 ;
				$filteredrow .= hash_file("md5", $filtered, FALSE);
				$filteredrow .= "<td colspan=\"8\"><strong><span class=\"material-icons filter600\" data-md-tooltip=\"Filter\">filter_alt</span>" . file_get_contents($filtered) . "</strong></td>";
			}
		
			// copy rows together and create the preceding <tr> element that was omitted above, now we know what status we know what category
			// echo htmlspecialchars($outputrow) . "<hr/>";
			if (strlen($filteredrow) > 0) {
				$totalrow = $p1 . substr($filteredrow, 0, 18+32) ;
				$totalrow .= "<tr class=\"lrborder F\">" . substr($filteredrow, 18+32) . "</tr>";
				$totalrow .= "<tr class=\"lrborder\">". substr($outputrow, 18);
				// now totalrow is FL* or IG* + yyymmddThhmmss + hash value + <tr><td>..info row..</td></tr>
			} else {
				$totalrow = $p1 . substr($outputrow, 0, 18) . "<tr class=\"lrborder\">". substr($outputrow, 18);
				// here totalrow is AAA + yyymmddThhmmss<tr><td>..info row..</td></tr>
			}

			$outputarray[] = $totalrow;
			// echo htmlspecialchars($totalrow) . "<hr/><br/>";
			// echo "<table>" . substr($outputrow, 18) . "</table>";
		
		}

		// now sort in reverse order (newest to top) and output all table rows
		arsort ($outputarray, SORT_REGULAR);

		// and strip the first (duplicate) timestamp signature
		for ($si = 0; $si < count($outputarray); ++$si) {
			// echo substr($outputarray[$si], 0, 32)  . htmlspecialchars(substr($outputarray[$si], 34)) . "<hr/><br/>";
			$outputarray[$si] = substr($outputarray[$si], 15);
			// echo substr($outputarray[$si], 0, 32)  . htmlspecialchars(substr($outputarray[$si], 34)) . "<hr/><br/>";
			// echo "***" . substr($outputarray[$si], 0, 32)  . "<hr/><br/>";
		}

		// end loop to create all table rows
		// now we have
		//   AAAyyymmddThhmmss<tr><td>..info row..</td></tr>
		//     -> rows with one single ordinary info output row
		//   AARyyymmddThhmmss<tr><td>..info row..</td></tr>
		//     -> rows with one single ordinary info output row, retired item
		//   FLAyyymmddThhmmss<tr><td>..filter text..</td><tr><tr><td>..info row..</td></tr>
		//     -> rows with one row of filter text + an ordinaly info output row
		//   XCNyyymmddThhmmss<tr><tr><td>..info row..</td></tr>
		//     -> rows with one single ordinary info output row, cancelled item
		//   FLRyyymmddThhmmss<tr><td>..filter text..</td><tr><tr><td>..info row..</td></tr>
		//     -> rows with one row of filter text + an ordinaly info output row, retired item
		//   FLXyyymmddThhmmss<tr><td>..filter text..</td><tr><tr><td>..info row..</td></tr>
		//     -> rows with one row of filter text + an ordinaly info output row, cancelled item
		//   IGAyyymmddThhmmss<tr><td>..filter text..</td><tr><tr><td>..info row..</td></tr>
		//     -> rows with one row of IG name text + an ordinaly info output row
		//   IGRyyymmddThhmmss<tr><td>..filter text..</td><tr><tr><td>..info row..</td></tr>
		//     -> rows with one row of IG name text + an ordinaly info output row,cancelled/retired item

		foreach ($outputarray as $row) {
			// echo substr($row, 0, 32)  . htmlspecialchars(substr($row, 34)) . "<hr/><br/>";
		}
		// var_dump($outputarray);
		
		// get all filter hash elements
		$allfilterhashs = array();
		foreach ($outputarray as $row) {
			$ff = substr($row, 18, 32);
			//echo "(" . $row . ")-" . $ff . "<br/>";
			if (substr($row, 0, 3) === "FLA" or substr($row, 0, 3) === "FLX" or substr($row, 0, 3) === "FLR" or substr($row, 0, 3) === "IGA") {
				if (!(in_array($ff, $allfilterhashs))) {
					$allfilterhashs[] = $ff;
				}
			}
		}
		// var_dump($allfilterhashs);
		
		/*
		* now emit all rows in groups
		*/
		$realoutputrowcount = 0;  // count of emited rows
		/*
		* first round: current AAA and current filtered publications
		*/
		$aaacnt = 0;
		$alreadyshown = array();
		dothead ($_i18n_current, $doPdf, $doRelease, $doHTML, $doSCH, $doXML, $doFHIRIG);
		foreach ($outputarray as $row) {
			// echo htmlspecialchars($row) . "<hr/><br/>";
			if (substr($row, 0, 3) === "AAA") {
				echo substr($row, 18) ;
				$realoutputrowcount++;
				$aaacnt++ ;
				$aix = substr($row, 0, 18) ;
				$alreadyshown[$aix] = TRUE;
			}
		}

		$filcnt = 0;
		
		$lasthash = "";
		$seenfilterhashs = array();
		foreach ($allfilterhashs as $filter) {
			foreach ($outputarray as $row) {
				$currenthash = substr($row, 18, 32);
				if ((substr($row, 0, 3) === "FLA" or substr($row, 0, 3) === "IGA") and $currenthash === $filter) {
					$seenfilterhashs[$filter] += 1;
					if ($seenfilterhashs[$filter] >= 1) {
						if ($showheader) {
							$showheader = 0;
							dothead ($_i18n_archive, $doPdf, $doRelease, $doHTML, $doSCH, $doXML, $doFHIRIG);
						}
						if ($currenthash === $lasthash) {
							echo substr($row, strpos($row, "</tr>")) ;
						} else {
							echo substr($row, 18+32);
						}
						$realoutputrowcount++;
						$filcnt++;
						$aix = substr($row, 0, 18) ;
						$alreadyshown[$aix] = TRUE;
						$lasthash = $currenthash;
					}
				}
			}
		}
		// var_dump($seenfilterhashs);
		
		// signal if no actual items are found
		if ($aaacnt + $filcnt == 0 && $nof != 0) {
			echo "<tr><td colspan=\"8\">$_i18n_noactuals</td></tr>";
			$realoutputrowcount++;
		}

	} // end # of publications at all > 1 else
	
	// var_dump($seenfilterhashs);
	/*
	 * show archived versions
	*/
	$showheader = 1;
	if (1==1) {
		foreach ($outputarray as $row) {
			if (substr($row, 0, 3) === "AAR") {
				$aaacnt++ ;
				$aix = substr($row, 0, 18) ;
				if (!$alreadyshown[$aix]) {
					if ($showheader) {
						$showheader = 0;
						dothead ($_i18n_archive, $doPdf, $doRelease, $doHTML, $doSCH, $doXML, $doFHIRIG);
					}
					echo substr($row, 18) ;
					$realoutputrowcount++;
				}
			}
		}
		$lasthash = "";
		foreach ($allfilterhashs as $filter) {
			foreach ($outputarray as $row) {
				$currenthash = substr($row, 18, 32);
				if ((substr($row, 0, 3) === "FLR" or substr($row, 0, 3) === "IGR") and $currenthash === $filter) {
					$seenfilterhashs[$filter] += 1;
					if ($seenfilterhashs[$filter] >= 1) {
						if ($showheader) {
							$showheader = 0;
							dothead ($_i18n_archive, $doPdf, $doRelease, $doHTML, $doSCH, $doXML, $doFHIRIG);
						}
						if ($currenthash === $lasthash) {
							echo substr($row, strpos($row, "</tr>")) ;
						} else {
							echo substr($row, 18+32);
						}
						$realoutputrowcount++;
						$lasthash = $currenthash;
					}
				}
			}
		}
	}
	//var_dump($seenfilterhashs);
	
	/*
	 * last round: all X-C (cancelled) versions
	*/
	$showheader = 1;
	if (1==1) {
		foreach ($outputarray as $row) {
			if (substr($row, 0, 3) === "XCN" or substr($row, 0, 3) === "FLX") {
				if ($showheader) {
					$showheader = 0;
					dothead ($_i18n_rcancelled, $doPdf, $doRelease, $doHTML, $doSCH, $doXML, $doFHIRIG);
				}
				$snip = substr($row, 0, 3) === "FLX" ? 18+32 : 18;
				echo substr($row, $snip) ;
				$realoutputrowcount++;
			}
		}
	}

	// close table
	$schlupf = (4 - $realoutputrowcount) * 100;
	echo "<tr style=\"height: " . $schlupf . "px;\"><td colspan=\"8\"></td></tr>";
	echo "</table>";

	// output the rest of the index file
	echo <<<END
<br/>
<!-- Begin Bottom Menu -->
<!-- End Bottom Menu -->
<!-- Begin Bottom Modules -->
<!-- End Bottom Modules -->
<!-- Begin Footer -->
</div>
<div class="vcard-footer">
<div class="footer">
<div class="thefootercontent">
END;

	if (strlen($contact)>0) { 
		echo "<a style=\"float:right; padding-right: 17px;\" href=\"$contact\">$_i18n_contact</a>" ; 
	}

	$currentYear = date('Y');

	echo <<<END
<center>$thisscriptversion &copy; The ART-DECOR Expert Group 2010-$currentYear $additionalcopyright</center>
</div>
</div>
<!-- End Footer -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
END;
}

?>