<?php

/*
	ADRAM ART-DECOR release and archive manager
	server site part - manager
	
    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
    https://docs.art-decor.org/copyright/
    
    Author: Kai U. Heitmann
*/

	// name and version of this script
	$thisscriptname = "ADRAM";
	$thisscriptversion = "v3.20";

	// init
	ini_set('memory_limit', '4096M');
	date_default_timezone_set('Europe/Berlin');

/*  include CONFIGS AND DEFAULTS */
	if (is_file("config.php")) {
		include("config.php");
	} else {
		echo "+++ERROR not properly configured: config.php not found.\n";
    	exit;
	}


/* check required settings */
	if (!$urlmra) die ("+++ERROR not set in config: urlmra\n");
	if (!$urlpvr) die ("+++ERROR not set in config: urlpvr\n");
	if (!$data) die ("+++ERROR not set in config: data\n");
	if (!$javacmd) die ("+++ERROR not set in config: javacmd\n");
	if (!$javajar) die ("+++ERROR not set in config: javajar\n");
	if (!$xsl) die ("+++ERROR not set in config: xsl\n");
	if (!$isoschematrons) die ("+++ERROR not set in config: isoschematrons\n");
	if (!$compresscommand) die ("+++ERROR not set in config: compresscommand\n");
	// disabled for now if (!reachable($xsl)) die ("+++ERROR url/file not reachable, see config: xsl\n");

	
/* temporary files and dirs */
	$rand = mt_rand();
  $tmpdir = "_tmp_" . $rand;
  $tmpfile = "_tmp_pubreq-" . $rand . ".xml";
  $tmpresult = "_tmp-result.txt";
  $pdfresult = "_pdf-result.txt";
  $igresult = "_ig-result.txt";
  $publicationsite = "publicationsite";
  $assetsdir = "assets";
  $scriptversiontxt = "scriptversion.txt";
  $stylesheet = "<?xml-stylesheet type=\"text/xsl\" href=\"$xsl\"?>\n" ;
	$assetstar = "assets.tar.gz";
	$assetstarurl = "https://assets.art-decor.org/ADAR/rv/" . $assetstar;
    
	error_reporting( E_ALL | E_STRICT );

    
/* real life starts here */
/* ===================== */

	// parse command line parameters
	// --touchrefreshonly: touch adram configs at destinations and refresh version/release per hosted project only (no transformations) yes/no
	// --updatescripts: update index.php and all assets (except config.php) in target directories
	$clidotouchrefreshonly = 0;
	$clidoupdatescripts = 0;
	
	foreach ($argv as $k => $arg) { 
		if ($k==0) continue; // skip 1st argument
		switch (htmlspecialchars(strtolower($arg))) {
		case '--touchrefreshonly' :
			$clidotouchrefreshonly = 1;
			break;
		case '--updatescripts' :
			$clidoupdatescripts = 1;
			break;
		default:
			echo "+++ERROR $arg not a valid command line option.\n";
			exit;
		}
	}
	
	echo "[$thisscriptname $thisscriptversion]\n";

	// some more presets / constants
	define("LEVEL1", 0);
	define("LEVEL2", 1);
	define("LEVEL3", 2);
	define("LEVEL4", 3);
	define("INERROR", TRUE);

	logmenl("Step I: touching adram configs at destinations and refresh version/release per hosted project...");
	$localserversettings = simplexml_load_file("localserversettings.xml", "SimpleXMLElement", LIBXML_NOWARNING);
	$locations = $localserversettings -> {'location'};
	// go thru all locations defined
	
	// in case $clidoupdatescripts: first get the most recent assets from assets.art-decor.org
	$assetsdecompressresult = 0;
	if ($clidoupdatescripts) {
		$here = getcwd();
		chdir($publicationsite);
		// get from ADAR/scripts
		$ch = curl_init();
		// set URL and other options
		curl_setopt($ch, CURLOPT_URL, $assetstarurl);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// do the following actions only if all assets are available
		if ($result = curl_exec($ch)) {
        	logmenl("decompressing assets from origin...", LEVEL2);
			// save and unpack
			file_put_contents($assetstar, $result);
			exec($untarcommand . $assetstar, $ol, $assetsdecompressresult);
			unlink($assetstar);
		} else {
			logmenl("+++ERROR: scripts and assets cannot be updated due to errors during download of original assets");
		}
		if ($assetsdecompressresult) {
			logmenl("+++ERROR: scripts and assets cannot be updated due to errors during uncompress of original assets");
		}
		chdir($here);
	}
	
	$refsastarget = array ();
	foreach ($locations as $loc) {
		$locpath = $loc['localpath'] ;
		if (!(is_writable($locpath))) {
			logmenl("+++ERROR: $locpath does not exist and must be created before calling ADRAM, change local server config or create it.");
		} else {
			logmenl("Location @" . $locpath, LEVEL2);
			$now = gmdate('Y-m-d\TH:i:s', time());
			// build touch file adram.config.xml
			$content = "<adram touched=\"$now\" version=\"$thisscriptversion\"/>\n";
			file_put_contents("$locpath/adram.config.xml", $content);
			logmenl("touched adram.config.xml", LEVEL3);
			// get project version and releases for this project and store it
			foreach ($loc -> {'for'} as $for) {
				$forprefix = $for['prefix'];
				$forreference = $for['reference'];
				$content = getversionandreleases($urlpvr, $forprefix);
				file_put_contents($locpath . "/" . $forprefix . "project.versionsreleases.xml", $content);
				$refsastarget[] = "" . ($forreference);  // add to array of string
			}
			logmenl("created project version and releases refresh", LEVEL3);
			// update index.php and all assets if requested
			if ($clidoupdatescripts) {
				// check current version
				$theirscriptversion = "";
				if (is_file("$locpath/$scriptversiontxt")) $theirscriptversion = htmlspecialchars(file_get_contents("$locpath/$scriptversiontxt"));
				if ($theirscriptversion == $thisscriptversion) {
					logmenl("scripts and assets are up-to-date", LEVEL3);
				} else {
					// get from ADAR/scripts
					// copy and unpack
					rcopy ($publicationsite, $locpath);
					$content = $thisscriptversion;
					file_put_contents("$locpath/$scriptversiontxt", $content);
					logmenl("scripts and assets updated", LEVEL3);
				}
			}
		}
	}
	
	if ($clidotouchrefreshonly) {
		logmenl("Step I: finished, all done...");
		exit;
	}
	
	logme("Step II: Getting ART-DECOR publication requests as file $tmpfile...");

	// create new cURL-Handle
	$ch = curl_init();

	// set URL and other options
	curl_setopt($ch, CURLOPT_URL, $urlmra);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	// action
	if ($result = curl_exec($ch) ) {
		file_put_contents($tmpfile, $result) ;
		$curlhttp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	} else {
		$error = curl_error($ch);
        $errr = array(
			'header' => '', 'body' => '', 'curl_error' => '', 'http_code' => '', 'last_url' => '');
		$curlhttp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ( $error == "" ) {
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$errr['header'] = substr($result, 0, $header_size);
			$errr['body'] = substr( $result, $header_size );
			$errr['http_code'] = $curlhttp;
			$errr['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);            
        } else {
			$errr['curl_error'] = $error;
		}
		//trigger_error(curl_error($ch));
		logmenl("\n+++FATAL getting ART-DECOR publication requests as file $tmpfile thru $urlmra.");
		var_dump($errr);
		exit;
	}
	
	// close cURL-Handle
	curl_close($ch);

	if ($curlhttp != '200') {
		logmenl("\n+++FATAL (HTTP $curlhttp) getting ART-DECOR publication requests as file $tmpfile thru $urlmra.");
		exit;
	}

	// check compilation
	$pubreqlist = simplexml_load_file($tmpfile, "SimpleXMLElement", LIBXML_NOWARNING | LIBXML_PARSEHUGE);

	if (strpos($result, "<no-access/>") !== false) {
		logmenl("\n+++FATAL: no access, please check access rights and parameters in config.");
		exit;
	}
	$cnt = $pubreqlist["count"] ;
	if (strlen($cnt) > 0) {
		logmenl("done");
	}
	else {
		logmenl("+++ERROR getting ART-DECOR publication requests as file $tmpfile thru $urlmra.");
	}
    
	// is there a publication reuqest at all?
    $obj = $pubreqlist -> {'publication-request'} ;
    if (!$obj) {
    	logmenl("...nothing to process, done, relaxing...");
    	unlink ($tmpfile);
    	exit ;
	}

    logmenl("Step III: Transformation for publication...");
    foreach ($obj as $pubreq) {
    	$prefix = $pubreq -> {'publication'}['prefix'] ;
    	$vdate = $pubreq -> {'publication'}['versionDate'] ;
    	$vsign = $pubreq -> {'publication'}['versionSignature'] ;
    	$projectid = $pubreq -> {'publication'}['projectId'] ;
		$projectlang = $pubreq -> {'publication'}['compile-language'] ;
    	$reference = $pubreq -> {'publication'}['reference'] ;
    	$deeplinkprefixfromserver = $pubreq -> {'publication'}['deeplinkprefix'] ;
		    
		// is it in our target list?
		if (strlen($reference) == 0) {
			logmenl("+++ Reference is empty, please correct your project settings, skipping!", LEVEL2);
			setpublicationstatus ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 'failed');
			setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 
				$thisscriptname . ": Reference in project is empty, please correct your project settings, skipped...", 0, INERROR);
			continue;
		}
		if (!in_array($reference, $refsastarget)) {
			logmenl("+++ Reference '" . $reference  . "' is not defined as a target, skipping!", LEVEL2);
			// var_dump($reference); var_dump($refsastarget); exit;
			setpublicationstatus ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 'failed');
			setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 
				$thisscriptname . ": " . $reference . " is not defined as a target, skipped...", 0, INERROR);
			continue;
		}

		logmenl("start processing " . $prefix . $vsign, LEVEL2, $prefix);
		logmenl("Project ID  " . $projectid, LEVEL3, $prefix);
		logmenl("Language(s) " . $projectlang, LEVEL3, $prefix);
		logmenl("Reference   " . $reference, LEVEL3, $prefix);
    	
		// set publication status, first to new to trigger the scheduled task, the to in progress
		setpublicationstatus ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 'new');
		logmenl("set publication process status to 'in progress'", LEVEL2, $prefix);
		setpublicationstatus ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 'inprogress');
		setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": start processing...", 5);
		
		// create new empty temp dir
		mkdir($tmpdir);
		$here = getcwd();
		
		$obj2 = $pubreq -> {'compiled-file-b64'} ;
		$projectlogoszip = $pubreq -> {'logos-zip'} ;
		$decorparam = $pubreq -> {'publication'} -> {'decor-parameters'} ;
		$decorfilters = $pubreq -> {'publication'} -> {'filters'} ;
		
		if ($obj2 and $decorparam) {
			foreach ($obj2 as $compfile) {
				$fn = $compfile['name'];
				$la = $compfile['language'];
				if ($la != '*') /* don't process * with all languages (now) */ {
					if (strendswith2($fn, "decor-compiled.xml")) {
						logmenl("extracting file " . $fn . " ($la)", LEVEL2, $prefix);
						setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": extracting request...", 10);
						chdir($tmpdir);
						file_put_contents($fn, base64_decode($compfile));
						$decorparam->asXML("decor-parameters.xml");
						$decorfilters->asXML("filters.xml");

						logmenl("transforming file " . $fn, LEVEL2, $prefix);
						setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": transforming...", 20);
						
						// set up params for transformation
						// use deeplinkprefix of publication request (the server side URI) only if artdecordeeplinkprefix in decor-parameter not set
						$deeplinkindecorparameter = $decorparam -> {'artdecordeeplinkprefix'} ;
						if (strlen($deeplinkindecorparameter)>0) {
							// decor-parameter always overrides server side URI
							$params="artdecordeeplinkprefix=$deeplinkindecorparameter";
						} else {
							// default is the server side URI
							$params="artdecordeeplinkprefix=$deeplinkprefixfromserver";
						}
						
						// set adram parameter to actual script version
						$params.= " adram=" . $thisscriptversion;

						//... and do the requested transformation
						$command = $javacmd . " ../" . $javajar . " -u -xsl:" . $xsl . " -s:" . $fn .
						   " " . $params . " 2>" . $tmpresult . " > /dev/null";
						exec($command, $ol, $rv);
						if ($rv !== 0) {
							logmenl("+++ERROR: transforming file " . $fn . " failed.", LEVEL3, $prefix);
							logmenl("+++ERROR: stylesheet transform returned with status $rv, cycling to next request", LEVEL3, $prefix);
							continue;  // give up, next thing to process...
						}

						// run the implementation guide artefact creation first if requested
						if (true) { /********************************** switch for Implementation Guide processing */
							logmenl("special preparation for Implementation Guide", LEVEL2, $prefix);
							$params3 = $params . " switchCreateSchematron=false switchCreateDocPDF=false " .
							  " switchCreateDocHTML=false switchCreateTreeTableHtml=false";
							$command = $javacmd . " ../" . $javajar . " -u -xsl:" . $xsl . " -s:" . $fn .
							   " " . $params3 . " 2> " . $igresult . " > /dev/null";
							exec($command, $ol, $rv);
							if ($rv !== 0) {
								logmenl("+++ERROR: while preparing Implementation Guide.", LEVEL3, $prefix);
								logmenl("+++ERROR: transform returned with status " . $rv, LEVEL3, $prefix);
							}
							// move the IG material into a separate directory
							$igdir = $prefix . "ig-" . $vsign ;
							if (!is_dir($igdir)) mkdir($igdir);
							$docbookdir = $prefix . "docbook-" . $vsign ;
							$igmatfile = "ig-mat.xml";
							rename($docbookdir . "/" . $igmatfile, $igdir . "/" . $igmatfile);
						}

					
						// get the artefacts that shall be included in the PDF
						$pdfincludes = "";
						if ($decorparam -> {'switchCreateDocPDF1'}) {
							$pdfincludes = $decorparam -> {'switchCreateDocPDF1'} -> attributes() -> include;
						}
						if ( (strlen($pdfincludes) > 0 ) and $decorparam -> {'switchCreateTreeTableHtml1'} ) {
							// if switchCreateTreeTableHtml1 is set we must re-run the HTML/docbook conversion
							// so: run a second round because PDFs don't have tree tables
							// do it with switchCreateTreeTableHtml=false (and no SCH or HTML generation)
							logmenl("special preparation for PDF " . $pdfincludes, LEVEL2, $prefix);
							$params2 = $params . " switchCreateSchematron=false switchCreateDocHTML=false " .
							  " switchCreateDocPDF=" . $pdfincludes . " switchCreateTreeTableHtml=false";
							$command = $javacmd . " ../" . $javajar . " -u -xsl:" . $xsl . " -s:" . $fn .
							   " " . $params2 . " 2> " . $pdfresult . " > /dev/null";
							exec($command, $ol, $rv);
							if ($rv !== 0) {
								logmenl("+++ERROR: while creating PDF.", LEVEL3, $prefix);
								logmenl("+++ERROR: transform returned with status " . $rv, LEVEL3, $prefix);
							}
						}

						// get name of the runtime original
						$runt = $prefix . "runtime-" . $vsign ;
						// set name of runtime closed
						$runtclosed = $prefix . "runtime-closed-" . $vsign ;
						
						// re-run and create a runtime with switchCreateSchematronWithWarningsOnOpen1
						// if the original has no "close" option
						$wantssch = $decorparam -> {'switchCreateSchematron1'};
						$hasclosed = $decorparam -> {'switchCreateSchematronClosed1'};
						$haswarning = $decorparam -> {'switchCreateSchematronWithWarningsOnOpen1'};
						if (file_exists($runt) and $wantssch and !$hasclosed and !$haswarning) {
							logmenl("re-run for closed schematrons", LEVEL2, $prefix);
							// save original runtime
							rename($runt, "_ORIG");
							$params2 = $params . " switchCreateSchematronWithWarningsOnOpenString=true";
							$command = $javacmd . " ../" . $javajar . " -u -xsl:" . $xsl . " -s:" . $fn .
							   " " . $params2 . " 2> " . $pdfresult . " > /dev/null";
							exec($command, $ol, $rv);
							if ($rv !== 0) {
								logmenl("+++ERROR: while creating runtime with switchCreateSchematronWithWarningsOnOpen1.", LEVEL3, $prefix);
								logmenl("+++ERROR: transform returned with status " . $rv, LEVEL3, $prefix);
							}
							rename($runt, $runtclosed);
							// restore original runtime
							rename("_ORIG", $runt);
						}
						
						// create runtime zip if it exists
						if (file_exists($runt) or file_exists($runtclosed)) {
							logmenl("creating xslt variants of schematrons", LEVEL2, $prefix);
							setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": schematron xslt...", 70);
							$ldir = getcwd();
							if (file_exists($runt)) {
								chdir($runt);
								makexslt4schematron($runt, $prefix);
								chdir($ldir);
								logmenl("compressing runtime " . $runt, LEVEL3, $prefix);
								$command = "$compresscommand $runt.zip $runt";
								exec($command, $ol, $rv);
							}
							if (file_exists($runtclosed)) {
								chdir($runtclosed);
								makexslt4schematron($runtclosed, $prefix);
								chdir($ldir);
								logmenl("compressing runtime " . $runtclosed , LEVEL3, $prefix);
								$command = "$compresscommand $runtclosed.zip $runtclosed";
								exec($command, $ol, $rv);
							}
						} else {
							logmenl("no runtime generated " . $runt, LEVEL2, $prefix);
						}
					
						// inject transformation protocol with result into HTML rendition
						logme("inject transformation protocol with result: ", LEVEL2, $prefix);
						$rawlines = file_get_contents($tmpresult);
						$lines = explode("\n", $rawlines); //create array separate by new line
						// analyze
						$info = 0;
						$error = 0;
						$warn = 0;
						foreach ($lines as $line_num => $line) {
							if (substr($line, 0, 4) == 'INFO') {
								$info++;
							}
							if (substr($line, 0, 5) == 'ERROR') {
								$error++;
							}
							if (substr($line, 0, 4) == 'WARN') {
								$warn++;
							}
						}
						if ($error > 0) {
							echo "error, INFO $info - ERROR $error - WARN $warn\n";
						} else {
							echo "done, INFO $info - ERROR $error - WARN $warn\n";
						}
						//foreach ($lines as $line_num => $line) {
						//	echo "	$line\n";
						//}
						$cptf = $prefix . "html-" . $vsign . "/compiletime.html";
						if (file_exists($cptf)) {
							$cpt = file_get_contents($cptf);
							// make all ERROR lines red
							$rawlines = preg_replace("/\nERROR/", "\n<span style=\"color:red\"><b>ERROR</b></span>", $rawlines);
							$rawlines = preg_replace("/\nWARN/", "\n<span style=\"color:orange\"><b>WARN</b></span>", $rawlines);
							$rawlines = str_replace("\n", "<br/>", $rawlines);
							$cptm = str_replace("Protocol not registered.", $rawlines, $cpt);
							$cptm = str_replace("<div class=\"transformationProtocolSummary\" style=\"display:none;\"></div>", " Info(s) $info - Error(s) $error - Warning(s) $warn", $cptm);
							if ($error == 0) {
								$cptm = str_replace("<div class=\"transformationProtocolIcon\" style=\"display:none;\"></div>", "", $cptm);
							} else {
								$cptm = str_replace("info.png\"><div class=\"transformationProtocolIcon\" style=\"display:none;\"></div>", "alert.png\">", $cptm);
							}
							file_put_contents($cptf, $cptm);
						}
					
						// create html zip if html exists
						logmenl("HTML post-processing", LEVEL2, $prefix);
						setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": HTML post-processing...", 80);
						$htmls = $prefix . "html-" . $vsign ;
						$pack = "pack-" . $htmls;
						$hdir = getcwd();
						if (file_exists($htmls)) {
							$ch = curl_init();
							// set URL and other options
							curl_setopt($ch, CURLOPT_URL, $assetstarurl);
							curl_setopt($ch, CURLOPT_POST, 0);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							// do the following actions only if all assets are available
							if ($result = curl_exec($ch)) {
								logmenl("compressing HTML " . $htmls, LEVEL3, $prefix);
								// make pack directory
								mkdir($pack);
								// must have the assets now there
								chdir($pack);
								file_put_contents($assetstar, $result);
								exec($untarcommand . $assetstar, $ol, $rv);
								unlink($assetstar);
								// copy html to pack dir
								rcopy("../" . $htmls , "./" . $htmls);
								logmenl("copied html to pack folder", LEVEL3, $prefix);
								// put logos where they belong to
								if (strlen($projectlogoszip)>10) {
									chdir($htmls);
									$logozipfile = $prefix . "logos.zip";
									file_put_contents($logozipfile, base64_decode($projectlogoszip));
									$command = "$uncompresscommand $logozipfile";
									exec($command, $ol, $rv);
									unlink($logozipfile);
									// copy logo dir into html folder
									chdir($hdir);
									chdir($htmls);
									file_put_contents($logozipfile, base64_decode($projectlogoszip));
									$command = "$uncompresscommand $logozipfile";
									exec($command, $ol, $rv);
									unlink($logozipfile);
								}
								// create html zip
								chdir($hdir);
								logmenl("zipping HTML " . $pack, LEVEL4, $prefix);
								$command = "$compresscommand -m $pack.zip $pack";
								exec($command, $ol, $rv);
								logmenl("html generated, html zip generated: " . $pack, LEVEL3, $prefix);
							} else {
								logmenl("html generated, no html zip generated: " . $htmls, LEVEL2, $prefix);
							}
						} else {
							logmenl("no html generated, no html zip generated: " . $htmls, LEVEL2, $prefix);
						}
						chdir($hdir);
					
						// create the pdf if requested and allowed
						// .. ie switchCreateDocPDF1 is on and a docbook dir exist with adecorbook.html
						// .. content has already been determined by the swith in XSL conversion
						if ($decorparam -> {'switchCreateDocPDF1'}) {

							if ($allowpdf==0) {
								logmenl("skip creating pdf from extract := PDF creation not enabled but requested", LEVEL2, $prefix);
							} else {
								$docbookdir = $prefix . "docbook-" . $vsign;
								$adecorbook = $docbookdir . "/adecorbook.html";
								if (file_exists($adecorbook)) {
									$thepdf = $prefix . $vsign . ".pdf" ;
									logmenl("creating pdf from extract $adecorbook = = > $thepdf", LEVEL2, $prefix);
									setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": creating pdf...", 90);
									$command = "$topdfcommand $adecorbook" .
										" -v --script $topdfcssdir/makeruler.js --javascript " .
										" -o $thepdf " .
										" -s $topdfcss";
									// must have the assets here :-(
									$ch = curl_init();
									// set URL and other options
									curl_setopt($ch, CURLOPT_URL, $assetstarurl);
									curl_setopt($ch, CURLOPT_POST, 0);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
									if ($result = curl_exec($ch)) {
										file_put_contents($assetstar, $result);
										exec($untarcommand . $assetstar, $ol, $rv);
									}
									//echo $command . "\n";
									exec($command, $ol, $rv);
								}
							}

						} else {
							if ($allowpdf==1)
								logmenl("skip creating pdf from extract := switch switchCreateDocPDF1 not set", LEVEL2, $prefix);
						}
					
						// create filters file if necessary
						$filtersset = $decorfilters ['filter'] ;
						$filtersfile = "" ;
						if ($filtersset == 'on') {
							$filterslabel = $decorfilters ['label'] ;
							$filtersfile = $prefix . "filter-" . $vsign . ".txt" ;
							file_put_contents($filtersfile, $filterslabel);
						}

					
						// back home, one up
						chdir($here);

						logmenl("publication: copy all to location and cleanup...", LEVEL2, $prefix);
						// set publication status
						setpublicationstatus ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 'deploying');
						setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, $thisscriptname . ": final publish...", 95);
						// copy x-runtime-t.zip and x-html-t to the location found in localserversettings
						// that matches 
						// <location localpath="/var/ww/vhosts/x/y/vacc">
						//	  <for id="2.16.840.1.113883.3.1937.99.61.5" prefix="vacc-" reference="https://art-decor.org/demos/vacc/" language="en-US"/>
						// </location>
						$localserversettings = simplexml_load_file("localserversettings.xml", "SimpleXMLElement", LIBXML_NOWARNING);
						$locations = $localserversettings -> {'location'}  ;
						$didrealcopy = 0;
						// umask: as some servers might have ristriction which lead to unreadable copies of 
						// the publications we temporarily set an appropriate umask
						umask(0002);
						foreach ($locations as $loc) {
							$locpath = $loc['localpath'] ;
							foreach ($loc -> {'for'} as $for) {
								$locid = $for['id'] ;
								$locref = $for['reference'] ;
								$locprefix = $for['prefix'] ;
								$loclang = $for['language'] ;
								$locextraclosed = $for['extraclosed'] ;
								// check
								// echo ".........checking '$locprefix' $locid $locref $loclang\n";
								// echo "................. '$prefix' $projectid $reference $la\n";
								//var_dump (strcmp($prefix, $locprefix) == 0) ;
								if (strcmp($prefix, $locprefix) === 0 and 
									strcmp($projectid, $locid) === 0 and 
									strcmp($reference, $locref) === 0 and 
									strcmp($la, $loclang) === 0 ) {
										// copy html dir
										$fn = $locprefix . "html-" . $vsign ;		
										rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
										logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										// copy html pack zip
										$fn = $pack . ".zip" ;		
										rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
										logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										// copy runtime zip file
										$fn = $locprefix . "runtime-" . $vsign . ".zip" ;
										rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
										logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										if ($locextraclosed == 'true') {
											// copy runtime closed zip file
											$fn = $locprefix . "runtime-closed-" . $vsign . ".zip" ;
											rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
											logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										}
										// copy runtime dir
										$fn = $locprefix . "runtime-" . $vsign ;
										rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
										logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										// copy runtime closed dir
										if ($locextraclosed == 'true') {
											$fn = $locprefix . "runtime-closed-" . $vsign ;
											rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
											logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										}
										// copy pdf file if exist
										$fn = $locprefix . $vsign . ".pdf" ;
										if (file_exists($tmpdir . "/" . $fn)) {
											rcopy($tmpdir . "/" . $fn, $locpath . "/". $fn);
											logmenl("copied $fn to $locpath", LEVEL3, $prefix);
										}
										// copy filters file if exist
										if (strlen($filtersfile)>0) {
											rcopy($tmpdir . "/" . $filtersfile, $locpath . "/". $filtersfile);
											logmenl("copied $filtersfile to $locpath", LEVEL3, $prefix);
										}
										// yes I did copy
										$didrealcopy = 1;
								}
							}
						}
						if ($didrealcopy == 0) {
							logmenl("+++ after all that work NOTHING was copied to any destination.", LEVEL2, $prefix);
							logmenl("+++ Possible mismatches in local server settings.", LEVEL2, $prefix);
							logmenl("+++ Check for prefix " . $prefix . " id " . $projectid . " reference " . $reference . " language " . $la, LEVEL2, $prefix);
							/* not usefull as it always contains the last entry on no matches
							if (strcmp($prefix, $locprefix) != 0) echo ".........[$prefix] +++ prefix $prefix not equal $locprefix in localsettings\n";
							if (strcmp($projectid, $locid) != 0)  echo ".........[$prefix] +++ id $projectid not equal $locid in localsettings\n";
							if (strcmp($reference, $locref) != 0) echo ".........[$prefix] +++ reference $reference not equal $locref in localsettings\n";
							if (strcmp($la, $loclang) != 0)       echo ".........[$prefix] +++ language $la not equal $loclang in localsettings\n";
							*/
						}
									
						// set internal publication status to completed to signal everything went fine
						// ...and release/version to review = pre-publication review, can be moved to active...
						logmenl("set publication process status to 'completed'", LEVEL2, $prefix);
						setpublicationstatus ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, 'completed');
						setprogressmessage ($urlmra, $data, $prefix, $vdate, $vsign, $projectlang, "", 100);


					} else {
						logmenl("skipping file " . $fn , LEVEL2, $prefix);
					}
				} else {
					logmenl("skipped (* language not processed) " . $prefix . $vsign, LEVEL2, $prefix);
				}
			}
		}
				
	}
	
	logmenl("Step IV: finished, all done...");

	exit; // this is it


function strendswith2($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

// removes files and non-empty directories
function rrmdir($dir) {
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
       }
     }
     reset($objects);
     rmdir($dir);
   }
 }
 
 // copies files and non-empty directories
function rcopy($src, $dst) {
  if (is_dir($src)) {
    if (!is_dir($dst)) mkdir($dst);
    $files = scandir($src);
    foreach ($files as $file)
    	if ($file != "." && $file != "..") rcopy("$src/$file", "$dst/$file"); 
  }
  else if (file_exists($src)) copy($src, $dst);
}

// set publication status
function setpublicationstatus ($url, $data, $project, $vdate, $vsign, $projectlang, $status) {
	// create new cURL-Handle
	$ch = curl_init();

	// set URL and other options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$d = $data;
	$d['action'] = 'setpublicationstatus';
	$d['project'] = $project;
	$d['date'] = $vdate;
	$d['signature'] = $vsign;
	$d['language'] = $projectlang;
	$d['status'] = $status;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $d);

	// action
	if ($result = curl_exec($ch) ) {
    	// return result below
	} else {
		//var_dump(curl_getinfo($ch));
		//trigger_error(curl_error($ch));
		logmenl("+++ERROR processing ART-DECOR publication status change thru $url failed.");
		exit;
	}
	// close cURL-Handle
	curl_close($ch);
	
}

// set publication progress message
function setprogressmessage ($url, $data, $project, $vdate, $vsign, $projectlang, $message, $percentage, $inerror = FALSE) {
	
	// create new cURL-Handle
	$ch = curl_init();

	// set URL and other options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$d = $data;
	$d['action'] = 'setprogressmessage';
	$d['project'] = $project;
	$d['date'] = $vdate;
	$d['signature'] = $vsign;
	$d['language'] = $projectlang;
	$d['message'] = $message;
	$d['percentage'] = $percentage;
	if ($inerror == TRUE) {
		$d['busy'] = 'error';  // mark this request as "in error"
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $d);

	// action
	if ($result = curl_exec($ch) ) {
    	// return result below
		// var_dump($result);
	} else {
		//var_dump(curl_getinfo($ch));
		//trigger_error(curl_error($ch));
		logmenl("+++ERROR processing ART-DECOR progress message change thru $url failed.");
		exit;
	}
	// close cURL-Handle
	curl_close($ch);
	
}

// get project versions and releases and write it to $localfile
function getversionandreleases ($url, $project) {
	// create new cURL-Handle
	$ch = curl_init();
	
	$data = array (
	// the project prefix
        'project' => $project
        );
	// set URL and other options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	// action
	$result = curl_exec($ch);
	$curlhttp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$error = curl_error($ch);
	if ($curlhttp === 200 and $result) {
    	return $result;
	} else {
		// var_dump(curl_getinfo($ch));
		// trigger_error(curl_error($ch));
		logmenl("+++ERROR getting ART-DECOR project version and releases for project $project ($url): " . $error);
		exit; // give up!
	}

	return null;
	
}

// make xslt 4 schematrons
function makexslt4schematron ($runt, $prefix) {

	global $javacmd, $javajar, $isoschematrons;

	$path2schematroninclude     = $isoschematrons . "/iso_dsdl_include.xsl";
    $path2schematronabstract    = $isoschematrons . "/iso_abstract_expand.xsl";
    $path2schematronsvrl        = $isoschematrons . "/iso_svrl_for_xslt2.xsl";
    $path2schematrontext        = $isoschematrons . "/iso_schematron_message_xslt2.xsl";
    
    // suffix for svrl
	$schsvrlsuffix = "_svrl";
    // suffix for xslts with text error messages
    $schtextsuffix = "_text";
	
	$fns = glob("*.sch");
	$nof = count($fns);
	for($i = $nof-1; $i >= 0; $i--) {
    	// convert this schematron to xslt variant
    	$sch = $fns[$i];
    	$basename = basename($sch, ".sch");
		logmenl("creating xslt variants of schematron $sch", LEVEL3, $prefix);
    	// step 1
    	$command =  $javacmd . " ../../" . $javajar . " -u -xsl:" . $path2schematroninclude . " -s:" . $sch .
    				" allow-foreign=true generate-fired-rule=false" . " 2>" . "_0" . " > _1";
  		exec($command, $ol, $rv);
    	// step 2
    	$command =  $javacmd . " ../../" . $javajar . " -u -xsl:" . $path2schematronabstract . " -s:" . "_1" .
    				" allow-foreign=true generate-fired-rule=false" . " 2>" . "_0" . " > _2";
  		exec($command, $ol, $rv);
    	// step 3a svrl
    	$command =  $javacmd . " ../../" . $javajar . " -u -xsl:" . $path2schematronsvrl . " -s:" . "_2" .
    				" allow-foreign=true generate-fired-rule=false" . " 2>" . "_0" . " > _3";
  		exec($command, $ol, $rv);
    	// step 3b xslt
    	$command =  $javacmd . " ../../" . $javajar . " -u -xsl:" . $path2schematrontext . " -s:" . "_2" .
    				" allow-foreign=true generate-fired-rule=false" . " 2>" . "_0" . " > _4";
  		exec($command, $ol, $rv);
		// clean up
  		unlink("_0");
  		unlink("_1");
  		unlink("_2");
  		rename("_3", $basename . $schsvrlsuffix . ".xsl");
  		rename("_4", $basename . $schtextsuffix . ".xsl");
    }
};

// check whether file / url is reachable
function reachable ($url) {
	$handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($handle, CURLOPT_HEADER, true);
    curl_setopt($handle, CURLOPT_NOBODY, true);
    curl_setopt($handle, CURLOPT_USERAGENT, true);
    $headers = curl_exec($handle);
    curl_close($handle);
	$httpStatus = explode(' ', $headers)[1];
	return $httpStatus === "200";
};

/* 
 * LOG functions
 * -------------
 */
function logme ($t, $level = 0, $ID = "") {
	echo date("Y-m-d H:i:s : ");
	if (strlen($ID) > 0) echo "   " . $ID;
	for($i = $level; $i > 0; $i--) echo "...";
    echo $t;
}
function logmenl ($t, $level = 0, $ID = "") {
    logme($t, $level, $ID);
    echo "\n";
}
?>
