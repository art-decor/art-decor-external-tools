<?php
/*
 *
 *  Copyright © ART-DECOR Expert Group and ART-DECOR Open Tools
 *  https://docs.art-decor.org/copyright/
 *
 *  - - - - - - - - - - -
 *  ART-DECOR® Betteruptime System Monitor (ADBUP)
 *  Informs set of users via email on longer lasting incidents
 */

/* ----Overide default execution vars---- */
date_default_timezone_set('Europe/Berlin') ;

// PHPmailer to be able to use mail via SMTP localhost
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php'; 

/* ----PRESETS---- */
/*  name and version of this script */
$thisscriptname = "ADBUP";
$thisscriptversion = "v1.3";

// other presets
$debug = 0;
$dosendmail = false;

// preset variables, will be overridden by the config
$betteruptimemonitorurl  = "";
$betteruptimebearertoken = "";

// include config
require "config.php";

// command line arguments
//   --summary   lists all monitors and their status
//   --downs     lists all monitors with active incidents only
// if no command is specified, nothing is reported
$action = '';
if (isset($argv[1])) {
    // if command line arg is given process that
    if ($argv[1] == '--summary') {
        $action = 'summary';
    } else if ($argv[1] == '--downs') {
        $action = 'incidents';
    } else if ($argv[1] == '--statistics') {
        $action = 'xxxxxx';
    } else {
        echo "Invalid option: " . $argv[1] . "\n";
        exit;
    }
} else {
    exit;
}

$nowh = date("Y-m-d H:i:s");
echo $nowh . " " . $thisscriptname . " " . $thisscriptversion . "\n";

// prepare calls to API
$authorization = "Authorization: Bearer " . $betteruptimebearertoken; // Prepare the authorisation token
// Hush through all registered MONITORs and report last incidents
// create new cURL-Handle
$ch = curl_init();
// https://betteruptime.com/api/v2/monitors
curl_setopt($ch, CURLOPT_URL, $betteruptimemonitorurl . "monitors?per_page=250");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch); // Execute the cURL statement
curl_close($ch); // Close the cURL connection
$monitors = json_decode($result);

// Hush through all registered MONITOR GROUPs
// create new cURL-Handle
$ch = curl_init();
// https://betteruptime.com/api/v2/monitor-groups
curl_setopt($ch, CURLOPT_URL, $betteruptimemonitorurl . "monitor-groups?per_page=250");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch); // Execute the cURL statement
curl_close($ch); // Close the cURL connection
$monitorgroups = json_decode($result);

// var_dump($monitorgroups->data);

// preset lists
$summary = array();
$relevantlines = 0;  // count output lines with real information
$alerts = 0;

foreach ($monitorgroups->data as $mongrp) {
    $grpid = $mongrp->id;
    $grpname = $mongrp->attributes->name;
    $grppaused = $mongrp->attributes->paused;
    echo sprintf("Group %-55s\n", $grpname);
    $summary[] = "<tr class=\"mgroup\"><td colspan=\"4\"><strong>$grpname</strong></td></tr>\n";

    $recovered = 0;

    // Hush through all monitors of the group
    // create new cURL-Handle
    $ch = curl_init();
    // https://betteruptime.com/api/v2/monitor-groups/34876/monitors
    curl_setopt($ch, CURLOPT_URL, $betteruptimemonitorurl . "monitor-groups/" . $grpid . "/monitors");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch); // Execute the cURL statement
    curl_close($ch); // Close the cURL connection
    $monitors = json_decode($result);

    foreach ($monitors->data as $mon) {
        // get infos
        $id = $mon->id;
        $name = $mon->attributes->pronounceable_name;
        $status = $mon->attributes->status;   // can up, down, validating, paused
        echo sprintf("  Monitor %-10s - %s\n", $status, $name);

        // store sentinel if not up
        $sfn = "_monitor_" . $id;    // the monitor's sentinel file
        $sfntime = 0;
        if ($status == 'paused') {
            if ($action === 'summary') {
                $summary[] = "<tr><td><strong>$name</strong></td>" . 
                    "<td><span class=\"dot\" style=\"background-color: #eeeeee\"></span></td>" . 
                    "<td><span style='color: #aaaaaa;'>*** Paused</td>" .
                    "<td> - </td>";
                $relevantlines++;
            }
            // otherwise do nothing when monitor is paused
        } else if ($status !== 'up') {
            // if this monitor is not up and not paused...
            $now = strtotime('now');
            // ... if a sentinel file of this monitor with a timestamp exist, get this timestamp
            if (is_file($sfn)) {
                $sfntime = file_get_contents($sfn);
            }
            // ... if last notification was sent within 60 mins don't alert again for this monitor
            $adiff = ($now - $sfntime) / 60;
            if ($adiff > 60) {
                $alerts++;                      // is down since more than 60 min, count this
                file_put_contents($sfn, $now);  // and write the new notification time to the sentinel file
            } else {
                echo sprintf("    Already notified, last notification %d min ago\n", $adiff);
            }
            // echo $sfntime . "/" . $now . "/" . ($now - $sfntime) / 60 . "\n"; exit;
        } else {
            if (is_file($sfn)) {
                $alerts++;     // was down (because we found a sentinel file), but is now up, notify this
                $recovered++;
                unlink($sfn);  // delete sentinel file
            }
        }

        // do requested actions
        if ($action === 'summary') {
            $alerts++;     // this is a summary request, notify this
            if ($status == 'paused') {
                // do nothing else for the summary if status is paused
            } else {
                // get all incidents and show the last one
                $il = getIncidents($id);
                if (count($il['summary']) > 0) {
                    $summary[] = "<tr><td><strong>$name</strong></td>" . $il['summary'][0] . "</tr>\n";
                } else {
                    $summary[] = "<tr><td><strong>$name</strong></td>" . 
                        "<td><span class=\"dot\" style=\"background-color: #eeeeee\"></span></td>" . 
                        "<td><span style='color: #aaaaaa;'>*** No incidents so far</td>" .
                        "<td> - </td>";
                }
                $relevantlines++;
            }
        } else if ($action === 'incidents') {
            // if status of this monitor is not up or it is recovered, then get all incidents for that monitor
            if ( ($status !== 'up') or ($recovered > 0) ) {
                // check now between the maintenance period
                // maintenance_from maintenance_to, all time stamps, examples
                // ["maintenance_from"] => string(8) "00:57:00"
                // ["maintenance_to"]=> string(8) "01:12:00"
                $nd = strtotime(date('Y-m-d H:i:s'));    // now timestamp
                if ($mon->attributes->maintenance_from === NULL) {
                    $mf = $nd;   // no maintenance set, declare the 'from' date as now
                } else {
                    $mf = strtotime(date('Y-m-d') . " " . $mon->attributes->maintenance_from);  // today and time from maintenance
                }
                if ($mon->attributes->maintenance_to === NULL) {
                    $mt = $nd;   // no maintenance set, declare the 'to' date as now
                } else {
                    $mt = strtotime(date('Y-m-d') . " " . $mon->attributes->maintenance_to);   // today and time from maintenance
                }
                if ($mf < $nd and $nd < $mt) {
                    echo sprintf("    Monitor in maintenance periode (start %d, now %d, to %d)\n", $mf, $nd, $mt);
                } else {
                    // if NOT in maintenance interval continue with notification
                    $il = getIncidents($id);
                    if ( ($il['downs'] > 0) or ($recovered > 0) ) {
                        $summary[] = "<tr><td colspan=\"3\"><strong>$name</strong></td></tr>\n";
                        $relevantlines++;
                        foreach ($il['summary'] as $key => $val) {
                            $summary[] = "<tr><td></td>" . $val . "</tr>\n";
                            $relevantlines++;
                        }
                    }
                }
            }
        }
    }

}

// EMAIL PREP and notification (if summary array contains more entries than only the group headings)
if ($relevantlines > 0) {
    $nowh = date("Y-m-d H:i:s");
    $displayaction = $action === 'summary' ? "Cluster Summary Report on Incidents and Status" : "Cluster Incident Report +++ ";
    $headline = $displayaction . " (" . $nowh . ")";
    $out = '';  // html output file

    $out .= '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    $out .= '<title>' . $headline . '</title>';
    $out .= '<style>' . _getCSS() . '</style>';
    $out .= '</head><body>';
    $out .= '<div class="container"><div class="main-body">';
    $out .= '<h3>' . $headline . '</h3>';
    $out .= '<div class="card"><div class="card-body">';
    $out .= '<table style="margin: 0">' .
        '<tr><td colspan=\"2\"><strong>Monitor</strong></td><td><strong>Status</strong></td><td><strong>Comment</strong></td><td><strong>At</strong></td></tr>';
    foreach ($summary as $key => $val) {
        $out .= $val . "\n";
    }
    $out .= '</table>';
    $out .= '</div></div></div></div></body></html>';

    if ( ($dosendmail === true) and ($alerts > 0) ) {
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            //Server settings
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;  //Enable verbose debug output
            $mail->isSMTP();  // Set mailer to use SMTP
            $mail->Host = $SMTPHOST;  // Specify main and backup SMTP servers
            $mail->Port = 25;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
            $mail->Username = $SMTPUSER; // SMTP username
            $mail->Password = $SMTPPASS;  // SMTP password

            // Sender
            $mail->setFrom('reply-not-possible@art-decor.email', 'ART-DECOR Notifier');
            $mail->addReplyTo('reply-not-possible@art-decor.email', 'ART-DECOR Notifier');

            //Recipients
            foreach ( $recipients as $rdkey => $remail) {
                $mail->addAddress($remail);     // Add a recipient
            }

            //Content
            $mail->isHTML(true);                          // Set email format to HTML
            $mail->Subject = "[art-decor] " . $displayaction . " (" . $nowh . ")";
            $mail->Body = wordwrap($out, 68);
            $mail->AltBody = 'There is no body in plain text for non-HTML mail clients, sorry!';
            $mail->send();
            echo "Message has been sent\n";

        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    // echo $out;
}

// bye bye
exit;


// get all INCIDENTS of a monitor
function getIncidents($monitorId) {
    global $betteruptimemonitorurl, $betteruptimebearertoken;
    $summary = array();
    // create new cURL-Handle
    $ch = curl_init();
    // set URL and other options
    $authorization = "Authorization: Bearer " . $betteruptimebearertoken; // Prepare the authorisation token
    $xurl = strlen($monitorId) > 0 ? "incidents?monitor_id=$monitorId" : "incidents";
    curl_setopt($ch, CURLOPT_URL, $betteruptimemonitorurl . $xurl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // action
    $result = curl_exec($ch); // Execute the cURL statement
    curl_close($ch); // Close the cURL connection
    $incidents = json_decode($result);
    // var_dump($incidents);
    // exit;
    // list
    $downcount = 0;
    foreach ($incidents->data as $inc) {
        $id = $inc->id;
        $name = $inc->attributes->name;
        $started = strtotime($inc->attributes->started_at);
        $resolved = $inc->attributes->resolved_at === NULL ? 'NULL' : strtotime($inc->attributes->resolved_at);
        // echo $name . "\n";
        // echo $inc->attributes->resolved_at . "\n";
        // var_dump($inc->attributes->resolved_at);
        $now = strtotime('now');
        $diff = $now-$started;
        $minutes_total = $diff / 60;
        $days_total = $diff / 86400;
        if ($resolved === 'NULL') {
            // unresolved yet
            if ($days_total > 2) {
                $ohm = round($days_total) . ' days';
            } else {
                $ohm = getDTDiffNowfFormatted($inc->attributes->started_at);
            }
            $downcount++;
            // add an entry if the incident is not resolved and time since start of incident is long time ago
            $dotcolor = '#de3163';  // preset
            if ($minutes_total > 10) $dotcolor = '#f6ca16';
            if ($minutes_total > 30) $dotcolor = '#ff9d00';
            if ($minutes_total > 60) $dotcolor = '#de3163';
            $summary[] = 
                "<td><span class=\"dot\" style=\"background-color: $dotcolor\"></span></td>" . 
                "<td>+++ Incident not resolved since " . $ohm . "</td>" .
                "<td>" . date('Y-m-d H:i:s', strtotime($inc->attributes->started_at . " GMT")) . "</td>";
        } else {
            if ($days_total > 2) {
                $ohm = round($days_total) . ' days';
            } else {
                $ohm = getDTDiffNowfFormatted($inc->attributes->resolved_at);
            }
            $dotcolor = '#27ae60';  // preset
            $textcolor = $days_total > 2 ? '#696c6a' : '#27ae60';
            $summary[] = 
                "<td><span class=\"dot\" style=\"background-color: $dotcolor\"></span></td>" . 
                "<td><span style='color: $textcolor;'>*** Up since " . $ohm . "</td>" .
                 "<td>" . date('Y-m-d H:i:s', strtotime($inc->attributes->resolved_at . " GMT")) . "</td>";
        }
    }
    // prepare what goes back
    $result = array(
        'downs' => $downcount,
        'summary' => $summary
    );
    return $result;
    
}


function getDTDiffNowfFormatted ($date1) {
    $start_date = new DateTime($date1);
    $since_start = $start_date->diff(new DateTime());
    $days_total = $since_start->days;
    $ohm = "";
    if ($days_total > 2) {
        $ohm = $days_total . ' days';
    } else {
        if ($since_start->d > 0) {
            $ohm .= sprintf("%dd %dh %dm", $since_start->d, $since_start->h, $since_start->i);
        } else if ($since_start->h > 0) {
            $ohm .= sprintf("%dh %dm", $since_start->h, $since_start->i);
        } else {
            $ohm .= sprintf("%dm", $since_start->i);
        }
    }
    return $ohm;
}

function _getCSS() {
	return <<<END
* {
        font-family: Verdana, Arial, sans-serif;
        font-size: 9.0pt;
    }
    body {
        color: #1a202c;
        text-align: left;
        background-color: #e2e8f0;
    }
    .container {
        margin: 7px;
    }
    .main-body {
        padding: 10px;
    }
    h3 {
        font-size: 10.0pt;
    }
	.sdesc {
		margin: 10px;
	}
    .button {
        border: none;
        text-align: center;
        margin: 2px;
        text-decoration: none;
        display: inline-block;
        cursor: pointer;
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
    }
    .card-body {
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1rem;
    }
    .gutters-sm {
        margin-right: -8px;
        margin-left: -8px;
    }
    .gutters-sm &gt; .col,
    .gutters-sm &gt; [class *= col-] {
        padding-right: 8px;
        padding-left: 8px;
    }
    .mb-3,
    .my-3 {
        margin-bottom: 1rem !important;
    }
    .mt-3 {
        margin-top: 1rem !important;
    }
    .newlabel {
        color: #bbbbbb;
    }
    div.labelouterbox {
        background-color: #eaeaea;
        float: right;
        display: inline;
        color: black;
        font-weight: bold;
        font-size: smaller;
        margin-left: 5px;
    }
    div.labelinnerbox {
        display: inline;
        padding: 0 5px
    }
    .status-green {
        border-left: 7px solid #c9e9c9;
        border-top: 1px solid #c9e9c9;
        padding: 3px 0 0 15px;
    }
    .status-blue {
        border-left: 7px solid #D1DDFF;
        border-top: 1px solid #D1DDFF;
        padding: 3px 0 0 15px;
    }
    .status-red {
        border-left: 7px solid #fadbdb;
        border-top: 1px solid #fadbdb;
        padding: 3px 0 0 15px;
    }
    .status-orange {
        border-left: 7px solid #fffaf4;
        border-top: 1px solid #fffaf4;
        padding: 3px 0 0 15px;
    }
    .status-grey {
        border-left: 7px solid #eaeaea;
        border-top: 1px solid #eaeaea;
        padding: 3px 0 0 15px;
    }
    .tt {
        font-family: Monaco, Courier, monospace !important;
    }
	.dot {
		height: 20px;
		width: 20px;
		background-color: #bbb;
		border-radius: 50%;
		display: inline-block;
	}
	th {
		text-align: left;
	}
	td {
		text-align: left;
	}
	td.right {
		text-align: right;
	}
	td.center {
		text-align: center;
	}
	td, th {
		padding: 3px;
	}
	table, th, td {
		margin: 18px;
		border-collapse: collapse;
  		border: 1px solid #eee;
		padding: 7px;
	}
	table.cellpadding {
		border-spacing: 10px !important;
	}
    tr.mgroup {
        background-color: #eee;
    }
	.collapsible {
		padding: 18px;
		background-color: #eee;
		cursor: pointer;
		width: 100%;
		border: none;
		text-align: left;
		outline: none;
		font-size: 15px;
	}
	.active, .collapsible:hover {
		background-color: #ddd;
	}
	.collapsiblecontent {
		max-height: 0;
		overflow: hidden;
		transition: max-height 0.2s ease-out;
	}
END;
}

function _getJS() {
	return <<<END
	var coll = document.getElementsByClassName("collapsible");
	var i;

	for (i = 0; i < coll.length; i++) {
	coll[i].addEventListener("click", function() {
		this.classList.toggle("active");
		var content = this.nextElementSibling;
		if (content.style.maxHeight){
			content.style.maxHeight = null;
		} else {
			content.style.maxHeight = content.scrollHeight + "px";
		} 
	});
	}
END;
}

?>